﻿using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities;
using Newtonsoft.Json;
using System;

using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace EPP_LIPIGAS_API.FilterAttributes
{
    public class ApplicationAuthenticationFilter : Attribute, IActionFilter
    {
        public ApplicationAuthenticationFilter()
        {

        }

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken,
           Func<Task<HttpResponseMessage>> continuation)
        {

            var request = actionContext.Request.Headers.Authorization;
            var identity = ParseAuthorizationHeader(actionContext);
            if (!identity)
            {
                var resultUnauthorized = new HttpResponseMessage();
                resultUnauthorized.StatusCode = HttpStatusCode.Unauthorized;
                resultUnauthorized.Content = new StringContent(
                                                JsonConvert.SerializeObject(
                                                    new MensajeHttpResponse()
                                                    { Message = "No se pudo autenticar el aplicativo." }
                                                )
                                            );
                resultUnauthorized.Content.Headers.ContentType.MediaType = "application/json";
                var result1 = Task.Run(() => resultUnauthorized);
                result1.Wait();
                return result1;
            }

            var result = continuation();
            result.Wait();

            return result;
        }
        public bool AllowMultiple
        {
            get { return true; }
        }
        protected virtual bool ParseAuthorizationHeader(HttpActionContext actionContext)
        {
            var ctx = new EPPEntities();

            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;

            if (auth != null && auth.Scheme == "Basic") authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader)) return false;

            var decodeauthToken = System.Text.Encoding.UTF8.GetString(
            Convert.FromBase64String(authHeader));

            var credentials = decodeauthToken.Split(':');
            var clientname = credentials[0];
            var clientsecret = credentials[1];

            try
            {
                var verifyAccess = ctx.SP_SEGURIDAD_ATTRIBUTE_VALIDAR_APLICACION(clientname, clientsecret).Select(x => new
                {
                    codigo = x.codigo.GetValueOrDefault(),
                    descripcion = x.descripcion,
                    clientid = x.clientid
                }).FirstOrDefault();

                if (verifyAccess == null) return false;
                if (verifyAccess.codigo != 200) return false;
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}