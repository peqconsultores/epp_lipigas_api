﻿using EPP_LIPIGAS_API.App_Start;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Implementacion;
using EPPLipigas.DataAccess.Models;
using log4net;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EPP_LIPIGAS_API.Providers
{
    public class ApplicationLipigasOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ApplicationLipigasOAuthProvider));
        private readonly IUsuarioDO _usuarioDO;
        UsuarioDO userRepository = new UsuarioDO();

        //public ApplicationLipigasOAuthProvider(IUsuarioDO usuarioDO)
        //{
        //    _usuarioDO = usuarioDO;

        //}
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            EPPEntities ctxBD = new EPPEntities();
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.SetError("invalid_client", "Client credentials could not be retrieved through the Authorization header.");
                return Task.FromResult<object>(null);
            }

            var validAplicacion = ctxBD.SP_SEGURIDAD_ATTRIBUTE_VALIDAR_APLICACION(clientId, clientSecret).Select(x => new
            {
                codigo = x.codigo.GetValueOrDefault(),
                descripcion = x.descripcion,
                clientid = x.clientid
            }).FirstOrDefault();

            if (validAplicacion == null || validAplicacion.codigo != 200)
            {
                context.SetError("invalide_client", "Client credentials are invalid.");
                return Task.FromResult<object>(null);
            }
            else
            {


                context.OwinContext.Set<string>("ta:client", validAplicacion.clientid);
                context.Validated();
                return Task.FromResult<object>(null);

            }
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            EPPEntities ctxBD = new EPPEntities();

            try
            {
                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
                var rut = context.UserName;

                var validar_rut = userRepository.validarrut(rut);
                if (validar_rut.codigoRes == HttpStatusCode.BadRequest)
                {


                    context.SetError("invalid_grant", "El nombre de usuario y/o contraseña son incorrectos.");
                    return;


                }
                var user = await userManager.FindAsync(rut, context.Password);

                if  (user == null)
                {
                    var responseValidacion = userRepository.ValidacionCantidadIntentos(context.UserName);
                    if (responseValidacion.codigoRes == HttpStatusCode.OK)
                    {
                        context.SetError("invalid_grant", "Se inactivó el usuario por 10 min debido a que superó la cantidad de intentos fallidos.");
                    }
                    if (responseValidacion.codigoRes == HttpStatusCode.Continue)
                    {
                        context.SetError("invalid_grant", responseValidacion.mensajeRes);
                        return;
                    }
                    else

                    {
                        context.SetError("invalid_grant", "El nombre de usuario y/o contraseña son incorrectos.");
                        return;
                    }

                }
                else
                {              
                        if (user.activo == null || user.activo == false)
                        {
                            //context.SetError("invalid_user", "The user is inactive.");
                            //context.SetError("invalid_grant", "el usuario esta inactivo.");
                            context.SetError("invalid_grant", "El nombre de usuario y/o contraseña son incorrectos.");
                            return;
                        }
                        if (user.eliminado == true)
                        {
                            //context.SetError("invalid_user", "The user was deleted.");
                            //context.SetError("invalid_grant", "El usuario no existe.");
                            context.SetError("invalid_grant", "El nombre de usuario y/o contraseña son incorrectos.");
                            return;
                        }
                        if (user.activo_bloqueo == true)
                        {
                            var respuesta = userRepository.activarCuenta(context.UserName);
                            if (respuesta.codigoRes == HttpStatusCode.OK)
                            {
                               

                                ClaimsIdentity oAuthIdentity1 = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);


                                var clienteAplicativo1 = context.OwinContext.Get<string>("ta:client");

                                context.OwinContext.Set<string>("ta:idusuario", user.id_usuario.ToString());
                                context.OwinContext.Set<string>("ta:username", user.username.ToString());

                                oAuthIdentity1.AddClaim(new Claim("rut", rut));
                                oAuthIdentity1.AddClaim(new Claim("cod_aplicacion", clienteAplicativo1));


                                string Username1 = Convert.ToString(user.username);

                                ClaimsIdentity cookiesIdentity1 = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);
                                AuthenticationProperties properties1 = new AuthenticationProperties(new Dictionary<string, string>
                                {
                                    {
                                        "Username", Username1
                                    }
                                });
                                AuthenticationTicket ticket1 = new AuthenticationTicket(oAuthIdentity1, properties1);
                                context.Validated(ticket1);
                                context.Request.Context.Authentication.SignIn(cookiesIdentity1);

                            }
                            else
                            {
                                context.SetError("invalid_user",respuesta.mensajeRes);
                                return;
                            }
                               
                        }

                }


                userRepository.UpdateUsuarioExitoso(context.UserName);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);


                var clienteAplicativo = context.OwinContext.Get<string>("ta:client");

                context.OwinContext.Set<string>("ta:idusuario", user.id_usuario.ToString());
                context.OwinContext.Set<string>("ta:username", user.username.ToString());

                oAuthIdentity.AddClaim(new Claim("rut", rut));
                oAuthIdentity.AddClaim(new Claim("cod_aplicacion", clienteAplicativo));


                string Username = Convert.ToString(user.username);

                ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);
                AuthenticationProperties properties = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "Username", Username
                    }
                });
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);

            }
            catch (Exception ex)
            {
                log.Error("GrantResourceOwnerCredentials ) ->  --> (error_system) Error for login. -->. Mensaje al cliente: Error interno  " +
                                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                context.SetError("error_system", "Error for login." + ex.InnerException.Message);
                return;
            }

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }


    }
}