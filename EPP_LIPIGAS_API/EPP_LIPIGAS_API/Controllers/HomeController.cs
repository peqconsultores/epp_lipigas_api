﻿using System;
using System.Configuration;
using System.Web.Mvc;

namespace EPP_LIPIGAS_API.Controllers
{
    public class HomeController : Controller
    {

        private string _urlIframeEDigital = ConfigurationManager.AppSettings["API_EDIGITAL_IFRAME_URL"];
        private string _flagvista = ConfigurationManager.AppSettings["flag_vista"];
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.FlagVista = Convert.ToBoolean(_flagvista);
            return View();
        }
        //public ActionResult FirmaDigital(string id)
        //{
        //    ViewBag.ruta_documento = _urlIframeEDigital + id;

        //    return View();
        //}
    }
}
