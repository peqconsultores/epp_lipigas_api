﻿using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Actas;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("api/acta")]
    public class ActaController : ApiController
    {
        private string _URL_TOKEN = ConfigurationManager.AppSettings["API_TOKEN"];
        private readonly ILog log = LogManager.GetLogger(typeof(ActaController));
        private readonly IActasBO _actasBO;
        private readonly IColaboradorBO _colaboradorBO;

        public ActaController(IActasBO actasBO, IColaboradorBO ColaboradorBO)
        {

            _actasBO = actasBO;
            _colaboradorBO = ColaboradorBO;
        }

        [HttpGet]
        [Route("listarActasEPP")]
        [Authorize]
        public HttpResponseMessage ListarActasEPP()
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }
            var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
            string username = User.Identity.GetUserId();

            string guid = Guid.NewGuid().ToString();
            try
            {
                var respuesta = _actasBO.ListarActasEPP(idUsuario, guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.datos, flagMostrarBoton = respuesta.flag });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta del listado de Actas." });
                }
            }
            catch (Exception ex)
            {
                log.Error($"ActaController ({guid})-> Usuario: {username} ->ListarActasEPP. Mensaje al cliente: Error interno en el servicio de Actas " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listado de Actas" });
            }
        }

        [HttpPost]
        [Route("RegistrarActa")]
        [Authorize]
        public HttpResponseMessage SincronizarActas(SincronizarActasRequest req)
        {
            string guid = Guid.NewGuid().ToString();
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }

            var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
            int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
            string username = User.Identity.GetUserName();
            try
            {

                var respuesta = _actasBO.SincronizarActa(req, idUsuario, id_aplicacion, guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }


                else
                {

                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de  SincronizarActas." });


                }

            }
            catch (Exception ex)
            {

                log.Error($"ActaController ({guid}) -> Usuario: {username} -> RegistrarActa. Mensaje al cliente: Error interno en el servicio SincronizarActas " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio al sincronizar actas" });
            }





        }

        [HttpPost]
        [Route("AprobarEntrega")]
        [Authorize]
        public HttpResponseMessage AprobarEntregaColaborador(AprobacionEntregaRequest req)
        {
            string guid = Guid.NewGuid().ToString();
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }

            var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
            int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
            string username = User.Identity.GetUserName();

            try
            {
                var context = HttpContext.Current;
                var contextBase = new HttpContextWrapper(context);
                var routeData = new RouteData();
                routeData.Values.Add("controller", "Home");
                var controllerContext = new System.Web.Mvc.ControllerContext(contextBase, routeData, new EmptyController());
                var respuestaDAtosPdf = _colaboradorBO.ObtenerRespuestaEstadoColaboradorEntregaDatosPDF(req.id_colaborador_entrega, idUsuario, guid);
                var respuesta = _actasBO.AprobacionEntrega(req.id_colaborador_entrega, req.rut, idUsuario, id_aplicacion, guid, controllerContext);

                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        string correoRespuesta = "";
                        try
                        {
                            correoRespuesta = _actasBO.AprobarEntrega_EnvioCorreo(respuestaDAtosPdf.datos.correo_colaborador, respuesta.documento_hash, respuestaDAtosPdf.datos.correo_responsable, respuestaDAtosPdf.datos.nombre, respuestaDAtosPdf.datos.apellidos);
                        }
                        catch (Exception)
                        {
                            correoRespuesta = "";
                        }

                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, link = respuesta.link });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new { Message = respuesta.mensajeRes, link = respuesta.link });

                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de  Aprobacion Entrega." });


                }
            }
            catch (Exception ex)
            {
                log.Error($"ActaController ({guid}) -> Usuario: {username} -> RegistrarActa. Mensaje al cliente: Error interno en el servicio SincronizarActas " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio al Aprobacion Entrega " });


            }



        }

        [HttpPost]
        [Route("recepcion_edigital/documento")]
        public HttpResponseMessage Recepcion_edigital(RecepcionarEdigitalRequest req)
        {

            string guid = Guid.NewGuid().ToString();


            try
            {
                log.Info("Recepcion_edigital-> [" + guid + "]--> REQUEST: " + JsonConvert.SerializeObject(req));

                IEnumerable<string> headerValues;
                var nameFilter = string.Empty;
                if (Request.Headers.TryGetValues("x-api-key", out headerValues))
                {

                    nameFilter = headerValues.FirstOrDefault();

                    log.Info("Recepcion_edigital-> [" + guid + "]+[" + nameFilter + "]");
                }
                if (nameFilter != _URL_TOKEN)
                {
                    log.Error($"Recepcion_edigital->[" + guid + "] +[" + nameFilter + "] -> RESPONSE:(HttpStatusCode.Unauthorized) Token Inválido");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                new MensajeHttpResponse() { Message = "Token Inválido" }); ;
                }
                var respuesta = _actasBO.Recepcion_edigital(req, guid);
                if (respuesta != null)
                {
                    log.Info("Recepcion_edigital [" + guid + "]--> RESPONSE: " + JsonConvert.SerializeObject(respuesta));
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }


                else
                {
                    log.Error("ObtenerPuntosAsesora [" + guid + "]--> RESPONSE: (HttpStatusCode.InternalServerError) Error interno al obtener respuesta de  Recepcion_edigital.");

                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de  Recepcion_edigital." });


                }

            }
            catch (Exception ex)
            {

                log.Error($"ActaController ({guid}) -> Usuario: -> RegistrarActa. Mensaje al cliente: Error interno en el servicio Recepcion_edigital " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio Recepcion_edigital" });
            }





        }

    }
    class EmptyController : System.Web.Mvc.ControllerBase
    {
        protected override void ExecuteCore() { }
    }

}