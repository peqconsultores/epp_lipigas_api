﻿using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("MODULO")]
    public class ModulosController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ModulosController));
        private readonly IModuloBO _ModuloBO;
        public ModulosController(IModuloBO ModuloBO)
        {
            _ModuloBO = ModuloBO;
        }

        [Route("MODULO_ROLES_X_USUARIO")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage MODULO_ROLES_X_USUARIO()
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());

                var respuesta = _ModuloBO.ModuloRolesXUsuarios(idUsuario);

                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.data });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de ." });

                }
            }
            catch (Exception ex)
            {

                log.Error("ActaController ({guid}) ->  ListarActasEPP. Mensaje al cliente: Error interno en el servicio de Actas " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de crear actas" });
            }

        }

    }
}
