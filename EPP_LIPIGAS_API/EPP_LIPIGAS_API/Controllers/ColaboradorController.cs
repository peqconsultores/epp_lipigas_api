﻿using EPP_LIPIGAS_API.FilterAttributes;
using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Colaborador.Request;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Routing;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("api/colaborador")]
    public class ColaboradorController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ColaboradorController));
        private readonly IColaboradorBO _colaboradorBO;
        public ColaboradorController(IColaboradorBO ColaboradorBO)
        {
            _colaboradorBO = ColaboradorBO;
        }

        [HttpPost]
        [Route("registro")]
        [Authorize]
        public HttpResponseMessage RegistrarColaborador(RegistrarColaboradorRequest request)
        {

            string guid = Guid.NewGuid().ToString();
            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);


                var respuesta = _colaboradorBO.RegistrarColaborador(request, id_usuario, id_aplicacion);

                if (respuesta != null)
                {
                    return Request.CreateResponse(respuesta.codigoRes,
                        new { Message = respuesta.mensajeRes, idPersona = respuesta.id_persona });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta al crear colaborador." });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController ({guid}) ->  RegistrarColaborador. Mensaje al cliente: Error interno en el servicio de creación de colaborador. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de creación de colaborador." });
            }
        }

        [HttpPost]
        [Route("registroAPP")]
        [Authorize]
        public HttpResponseMessage RegistrarColaboradorapp(RegistrarColaboradorAppRequest request)
        {

            string guid = Guid.NewGuid().ToString();
            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);


                var respuesta = _colaboradorBO.RegistrarColaboradorAPP(request, id_usuario, id_aplicacion);

                if (respuesta != null)
                {
                    return Request.CreateResponse(respuesta.codigoRes,
                        new { Message = respuesta.mensajeRes, idPersona = respuesta.id_persona });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta al crear colaborador." });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController ({guid}) ->  RegistrarColaborador. Mensaje al cliente: Error interno en el servicio de creación de colaborador. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de creación de colaborador." });
            }
        }

        [HttpGet]
        [Route("buscarColaborador/{rut}")]
        [Authorize]
        public HttpResponseMessage buscarColaborador(string rut)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }

                var respuesta = _colaboradorBO.BuscarColaborador(rut);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.data, flagColaboradorEncontrado = respuesta.flagEncontrado });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta en el servicio de buscar Colaborador." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  buscarColaborador. Mensaje al cliente: Error interno en el servicio de buscar Colaborador " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de buscar Colaborador" });
            }
        }

        [HttpGet]
        [Route("DetalleEntregaporColaborador")]
        [Authorize]
        public HttpResponseMessage ListarDetalleDeActa(int id_colaborador_entrega)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());

                var respuesta = _colaboradorBO.ObteneDetallaeEntregaPorColaborador(id_colaborador_entrega, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)

                    {

                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, flag_boton_aprobar = respuesta.flag_boton_aprobar, flag_boton_entrega = respuesta.flag_boton_entrega, id_colaborador_entrega = respuesta.id_colaborador_entrega, colaborador = respuesta.datosCabecera, listadoEpp = respuesta.listadoEpp });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }
        [HttpPost]
        [Route("ColaboradorEntrega")]
        [Authorize]
        public HttpResponseMessage EstadoColaboradorEntrega(int id_colaborador_entrega)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }

                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);

                var respuesta = _colaboradorBO.ObtenerRespuestaEstadoColaboradorEntrega(id_colaborador_entrega, idUsuario, id_aplicacion, guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes });
                    }
                    if (respuesta.codigoRes == HttpStatusCode.BadRequest)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                            new { Message = respuesta.mensajeRes,flag_mostrar=respuesta.flag_mostrar });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes, });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }

        [HttpGet]
        [Route("listaDatosWeb/{guid_entregado}")]
        [ApplicationAuthenticationFilter]
        public HttpResponseMessage ListaDatosEntregaWeb(string guid_entregado)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Basic") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }

                var respuesta = _colaboradorBO.ObtenerDatosEntregaxGuid(guid_entregado);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.datosColaboradorporguid });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                           new { Message = respuesta.mensajeRes });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }

        [HttpPost]
        [Route("Aprobarporweb")]
        [ApplicationAuthenticationFilter]
        public HttpResponseMessage ObtenerRespuestaAprobarxWeb(AprobarxWebRequest req)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Basic") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }

                var respuesta = _colaboradorBO.ObtenerRespuestaAprobarxWeb(req.guid_entregado, req.aprobacion_transaccion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }
        [HttpPost]
        [Route("reporte-colaboradores")]
        [Authorize]
        public HttpResponseMessage Listarcolaboradoresxfiltro(ReporteColaboradoresRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _colaboradorBO.Listarcolaboradoresxfiltro(request.fechaDesde, request.fechaHasta, request.nro_actaEPP, request.estado_acta, request.rut_responsable, request.rut_colaborador, request.tipo_colaborador, request.num_pagina, request.cant_registros, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                total_registros = respuesta.total_registros

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }

        [HttpPost]
        [Route("reporte-colaboradores-excel")]
        [Authorize]
        public HttpResponseMessage ListarcolaboradoresExcelxfiltro(ReporteColaboradorExcelRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _colaboradorBO.ListarcolaboradoresExcelxfiltro(request.fechaDesde, request.fechaHasta, request.nro_actaEPP, request.estado_acta, request.rut_responsable, request.rut_colaborador, request.tipo_colaborador, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }
        [HttpPost]
        [Route("actualizarcolaborador")]
        [Authorize]
        public HttpResponseMessage ActualizarColaborador(ActualizarColaboradorRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
                var respuesta = _colaboradorBO.ActualizarColaborador(request.id_colaborador_entrega,request.genero,request.correo,request.fecha_nacimiento,id_aplicacion ,idUsuario,guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                flag_vista=respuesta.flag_vista
                             });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }

        [HttpGet]
        [Route("DescargarDocumento/{id_docuemtno}")]
        //[Authorize]
        public HttpResponseMessage ObtenerDocumentodescargar(string id_docuemtno)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                //ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                //var validToken = HelperToken.LeerToken(principal);
                //if (validToken.codigo != 1)
                //{
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                //}
                //if (!HelperToken.validClienteCodAplicativo(validToken))
                //{
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                //}
                //var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                //int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
                var respuesta = _colaboradorBO.ObtenerDocumentodescargar(id_docuemtno, guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                link_documento = respuesta.link_descarga_documento
                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }

    }
}
