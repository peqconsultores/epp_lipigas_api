﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using log4net;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("api/aplicacion")]
    public class AplicacionController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AplicacionController));
        private readonly IAplicacionBO _aplicacionBO;

        public AplicacionController(IAplicacionBO aplicacionBO)
        {
            _aplicacionBO = aplicacionBO;
        }
        [HttpGet]
        [Route("parametros")]
        //[ApplicationAuthenticationFilter]
        public HttpResponseMessage ListarParametros()
        {
            string guid = Guid.NewGuid().ToString();
            string authHeader = null;
            var auth = Request.Headers.Authorization;

            if (auth != null && auth.Scheme == "Basic") authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "Debe ingresar los datos de la aplicación." });
            };

            var decodeauthToken = System.Text.Encoding.UTF8.GetString(
            Convert.FromBase64String(authHeader));

            var credentials = decodeauthToken.Split(':');
            var clientname = credentials[0];
            var clientsecret = credentials[1];
            try
            {
                var respuesta = _aplicacionBO.ListarParametros(clientname, clientsecret, guid);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.datos });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta del listado de paises." });
                }
            }
            catch (Exception ex)
            {

                log.Error("PaisController ->  parametros. Mensaje al cliente: Error interno en el servicio de listado de paises. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listado de paises." });
            }
        }

    }
}
