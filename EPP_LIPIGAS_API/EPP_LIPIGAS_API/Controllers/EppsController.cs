﻿using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Epp.Request;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("epps")]
    public class EppsController : ApiController
    {
        private readonly IEppsBO _eppsBO;
        private readonly ILog log = LogManager.GetLogger(typeof(InstalacionController));
        public EppsController(IEppsBO eppsBO)
        {
            _eppsBO = eppsBO;
        }

        [HttpPost]
        [Route("eppsxfiltros")]
        [Authorize]
        public HttpResponseMessage ListarEppsXFiltros(ListarEppsXFiltrosRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _eppsBO.ListarEppsXFiltros(request.categoria, request.clasificacion, request.epp, request.estado, request.num_pagina, request.cant_registros, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                cant_registros = respuesta.total_registros
                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController  ({guid})->  listarUsuariosxFiltros. Mensaje al cliente: Error interno en el servicio de listar usuario por filtro. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar usuario por filtro." });
            }
        }

        [HttpPost]
        [Route("RegistrarEpps")]
        [Authorize]
        public HttpResponseMessage RegistrarEpps(RegistrarEppsRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                if (ModelState.IsValid)
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                    var validToken = HelperToken.LeerToken(principal);
                    if (validToken.codigo != 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                    }
                    if (!HelperToken.validClienteCodAplicativo(validToken))
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                    }
                    var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                    int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
                    var respuesta = _eppsBO.RegistrarEpps(request.cod_clasificacion, request.cod_categoria, id_aplicacion, idUsuario, request.epp);
                    if (respuesta != null)
                    {

                        return Request.CreateResponse(respuesta.codigoRes,
                          new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                          new MensajeHttpResponse() { Message = "Error interno al Registrar  Epp" });
                    }
                }

                else
                {
                    List<string> errores = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                          new MensajeHttpResponse() { Message = errores[0] });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController  ({guid})->  listarUsuariosxFiltros. Mensaje al cliente: Error interno en el servicio de listar usuario por filtro. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar usuario por filtro." });
            }
        }

        [HttpDelete]
        [Route("eliminar-epp/{idEquipoProteccionPersonal}")]
        [Authorize]
        public HttpResponseMessage EliminarEpp(int idEquipoProteccionPersonal)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario = Convert.ToInt32(User.Identity.GetUserId());
                var cod_aplicacion = validToken.cod_aplicacion;
                var respuesta = _eppsBO.EliminarEpp(idEquipoProteccionPersonal, id_usuario, cod_aplicacion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al eliminar epp" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"EppsController({guid})->  EliminarEpp. Mensaje al cliente: Error interno en el servicio de eliminar epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de eliminar epp." });
            }
        }

        [HttpPut]
        [Route("editar-epp/{idEquipoProteccionPersonal}")]
        [Authorize]
        public HttpResponseMessage EditarEpp(int idEquipoProteccionPersonal, EditarEppRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario = Convert.ToInt32(User.Identity.GetUserId());
                var cod_aplicacion = validToken.cod_aplicacion;
                var respuesta = _eppsBO.EditarEpp(idEquipoProteccionPersonal, request, id_usuario, cod_aplicacion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al editar epp" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"EppsController({guid})->  EliminarEpp. Mensaje al cliente: Error interno en el servicio de editar epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de editar epp." });
            }
        }

        [HttpGet]
        [Route("detalle-epp/{idEquipoProteccionPersonal}")]
        [Authorize]
        public HttpResponseMessage ObtenerDetalleEpp(int idEquipoProteccionPersonal)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario = Convert.ToInt32(User.Identity.GetUserId());
                var cod_aplicacion = validToken.cod_aplicacion;
                var respuesta = _eppsBO.ObtenerDetalleEpp(idEquipoProteccionPersonal, id_usuario, cod_aplicacion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(respuesta.codigoRes,
                      new { Message = respuesta.mensajeRes, data = respuesta.datos });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener detalle epp" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"EppsController({guid})->  ObtenerDetalleEpp. Mensaje al cliente: Error interno en el servicio de detalle epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de detalle epp." });
            }
        }

        [HttpPost]
        [Route("reporte-epps")]
        [Authorize]
        public HttpResponseMessage ListarReportesEppsxfiltro(ReporteEppsRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _eppsBO.ListarEppsxfiltro(request.fechaDesde, request.fechaHasta, request.clasificacion, request.categoria, request.nombre_producto_epp, request.rut_colaborador, request.tipo_colaborador, request.num_pagina, request.cant_registros, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                total_registros = respuesta.total_registros

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("EppsController({guid})->  ListarReportesEppsxfiltro. Mensaje al cliente: Error interno en el servicio de listar reporte de epps. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  listar reporte de epps." });
            }
        }

        [HttpPost]
        [Route("reporte-epps-excel")]
        [Authorize]
        public HttpResponseMessage ListarEppsExcelxfiltro(ReporteEppsExcelRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _eppsBO.ListarEppsExcelxfiltro(request.fechaDesde, request.fechaHasta, request.clasificacion, request.categoria, request.nombre_producto_epp, request.rut_colaborador, request.tipo_colaborador, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("EppsController({guid})->  ListarReportesEppsxfiltro. Mensaje al cliente: Error interno en el servicio de listar reporte de epps. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  listar reporte de epps." });
            }
        }
    }
}