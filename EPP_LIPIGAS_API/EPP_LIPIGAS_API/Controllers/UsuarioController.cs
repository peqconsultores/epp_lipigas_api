﻿using EPP_LIPIGAS_API.App_Start;
using EPP_LIPIGAS_API.FilterAttributes;
using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Rut;
using EPPLipigas.Entities.Usuario.Request;
using EPPLipigas.Entities.Usuario.Response;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace EPP_LIPIGAS_API.Controllers
{
    [RoutePrefix("usuario")]
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioBO _usuarioBO;
        private readonly ILog log = LogManager.GetLogger(typeof(UsuarioController));
        public UsuarioController(IUsuarioBO usuarioBO)
        {
            _usuarioBO = usuarioBO;
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        [Route("rut")]
        [ApplicationAuthenticationFilter]
        public HttpResponseMessage BuscarRut(string rut)
        {
            try
            {
                var respuesta = _usuarioBO.BusquedaRut(rut);

                if (respuesta != null)
                {
                    log.Info("UsuarioController ->  BuscarRut. RESPONSE: " + JsonConvert.SerializeObject(respuesta));
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message ="OK"});
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    log.Warn("UsuarioController ->  BuscarRut. RESPONSE: (HttpStatusCode.InternalServerError) Error interno al obtener respuesta de buscar el rut");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,

                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de buscar el rut" });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController ({guid}) ->  BuscarRut. Mensaje al cliente: Error interno en el servicio buscarRut " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio BuscarRut" });
            }
        }
        [HttpPost]
        [Route("token")]
        [ApplicationAuthenticationFilter]
        public HttpResponseMessage getToken()
        {
            try
            {
                var respuesta = _usuarioBO.gettoken();

                if (respuesta != null)
                {
                    log.Info("UsuarioController ->  BuscarRut. RESPONSE: " + JsonConvert.SerializeObject(respuesta));
                    if (respuesta.codeHTTP == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.messageHTTP, token = respuesta.token });
                    }
                    else if (respuesta.codeHTTP == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codeHTTP,
                        new MensajeHttpResponse() { Message = respuesta.messageHTTP });
                }
                else
                {
                    log.Warn("UsuarioController ->  BuscarRut. RESPONSE: (HttpStatusCode.InternalServerError) Error interno al obtener respuesta de buscar el rut");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,

                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de buscar el rut" });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController ({guid}) ->  BuscarRut. Mensaje al cliente: Error interno en el servicio buscarRut " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio BuscarRut" });
            }
        }

        [HttpPost]
        [Route("creacion")]
        [ApplicationAuthenticationFilter]
        public HttpResponseMessage CreacionUsuario(CreacionUsuarioRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                var ctx = new EPPEntities();
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Basic") authHeader = auth.Parameter;

                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar los datos de la aplicación." });
                };

                var decodeauthToken = System.Text.Encoding.UTF8.GetString(
                Convert.FromBase64String(authHeader));

                var credentials = decodeauthToken.Split(':');
                var clientname = credentials[0];
                var clientsecret = credentials[1];

                var respuesta = _usuarioBO.CreacionUsuario(request);
                trs_persona datapersona = ctx.trs_persona.Where(x => x.id_persona == respuesta.id_persona).FirstOrDefault();
                //var ultimo = ctx.trs_usuario.OrderByDescending(x => x.id_usuario).First();
                //var ultimodata = ultimo.id_usuario + 1; 
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {

                        var usuario = new trs_usuario();
                        usuario.id_rol = datapersona.id_rol;
                        usuario.id_persona = respuesta.id_persona;
                        usuario.username = request.rut;
                        usuario.password = request.contrasenia;
                        usuario.activo = true;
                        usuario.eliminado = false;
                        usuario.fecha_registro = DateTime.Now;
                        usuario.fecha_modificacion = DateTime.Now;
                        usuario.aplicacion_registro = "1";

                        //var respuestaCrearUsuario = _usuarioBO.crearUsuario(respuesta.id_persona);


                        var respCreateUser = UserManager.CreateAsync(usuario, usuario.password);
                        if (!respCreateUser.Result.Succeeded)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, new MensajeHttpResponse() { Message = "No se pudo crear el usuario." });
                        }
                        var ctx2 = new EPPEntities();
                        var usersearch = ctx2.trs_usuario.Where(x => x.id_usuario == usuario.id_usuario).FirstOrDefault();
                        usersearch.cod_usuario = usuario.id_usuario.ToString();
                        var persona = ctx2.trs_persona.Where(x => x.id_persona == usuario.id_persona).FirstOrDefault();
                        persona.estado_persona = "REGISTRO DE PERSONA";
                        var result = ctx2.SaveChanges();


                        if (result <= 0)
                        {

                            return Request.CreateResponse(HttpStatusCode.BadRequest, new MensajeHttpResponse() { Message = "No se puedo registrar el codigo del usuario" });

                        }
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new MensajeHttpResponse() { Message = "Usuario creado exitosamente." });

                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new MensajeHttpResponse() { Message = "No se encontró los datos del usuario a registrar." });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }




                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta al crear el usuario." });
                }
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioController  ({guid})->({guid})  CreacionUsuario. Mensaje al cliente: Error interno en el servicio de creación de usuario. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de creación de usuario." });
            }
        }

        [HttpPost]
        [Route("editar/{_id_persona}")]
        [Authorize]
        public HttpResponseMessage EditarUsuario(int _id_persona, EditarUsuarioRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            string authHeader = null;
            var auth = Request.Headers.Authorization;

            if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
            if (string.IsNullOrEmpty(authHeader))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "Debe ingresar el token." });
            };
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }
            var id_usuario_mod = Convert.ToInt32(User.Identity.GetUserId());
            int id_aplicacion_mod = Convert.ToInt32(validToken.cod_aplicacion);

            var respuesta = _usuarioBO.EditarUsuario(request, _id_persona, id_usuario_mod, id_aplicacion_mod);
            if (respuesta != null)
            {
                if (respuesta.codigoRes == HttpStatusCode.Created)
                {
                    return Request.CreateResponse(HttpStatusCode.Created,
                        new MensajeHttpResponse() { Message = "Usuario editado exitosamente." });

                }
                else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new MensajeHttpResponse() { Message = "No se encontró los datos del usuario a editar." });
                }
                return Request.CreateResponse(respuesta.codigoRes,
                    new MensajeHttpResponse() { Message = respuesta.mensajeRes });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta al editar el usuario." });
            }
        }

        [HttpPost]
        [Route("eliminar/{id_persona}")]
        [Authorize]
        public HttpResponseMessage EliminacionUsuario(int id_persona)
        {
            string guid = Guid.NewGuid().ToString();
            string authHeader = null;
            var auth = Request.Headers.Authorization;

            if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
            if (string.IsNullOrEmpty(authHeader))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "Debe ingresar el token." });
            };
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }
            var id_usuario_elim = Convert.ToInt32(User.Identity.GetUserId());
            int id_aplicacion_elim = Convert.ToInt32(validToken.cod_aplicacion);
            string username = User.Identity.GetUserName();

            if (id_usuario_elim == id_persona)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new MensajeHttpResponse() { Message = "No es posible eliminar el usuario en sesión." });
            }

            var respuesta = _usuarioBO.EliminarUsuario(id_aplicacion_elim, id_usuario_elim, id_persona);
            if (respuesta != null)
            {
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        new MensajeHttpResponse() { Message = "Usuario eliminado exitosamente." });

                }
                else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new MensajeHttpResponse() { Message = "No se encontró los datos del usuario a eliminar." });
                }
                return Request.CreateResponse(respuesta.codigoRes,
                    new MensajeHttpResponse() { Message = respuesta.mensajeRes });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new MensajeHttpResponse() { Message = "Error interno al obtener respuesta al eliminar el usuario." });
            }
        }

        [HttpPost]
        [Route("contrasenia/reestablecer/{id_persona}")]
        [Authorize]
        public HttpResponseMessage ReestablecerContrasenia(int id_persona)
        {
            string guid = Guid.NewGuid().ToString();


            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var id_usuario_sesion = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion_sesion = Convert.ToInt32(validToken.cod_aplicacion);

                var ctx2 = new EPPEntities();
                var usuario2 = ctx2.trs_usuario.Where(x => x.id_persona == id_persona).FirstOrDefault();

                if (usuario2 != null)
                {
                    var usuario = UserManager.FindById(usuario2.id_usuario.ToString());
                    var passwordToken = UserManager.GeneratePasswordResetToken(usuario.Id);
                    IdentityResult resultChangePassword = UserManager.ResetPassword(usuario.Id, passwordToken, "123456");
                    if (resultChangePassword.Succeeded)
                    {
                        usuario.usuario_modificacion = usuario.cod_usuario;
                        usuario.fecha_modificacion = DateTime.Now;
                        var registerModified = UserManager.UpdateAsync(usuario);

                        return Request.CreateResponse(HttpStatusCode.OK,
                           new MensajeHttpResponse() { Message = "Contraseña actualizada" });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                new MensajeHttpResponse() { Message = "Error interno al actualizar la contraseña." });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            new MensajeHttpResponse() { Message = "Error interno al actualizar la contraseña." });
                }
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioController  ({guid})-> -> ReestablecerContrasenia. Mensaje al cliente: Error interno en el servicio de reestablecer contraseña del usuario. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de reestablecer contraseña del usuario." });
            }
        }

        [HttpPost]
        [Route("usuariosxfiltros")]
        [Authorize]
        public HttpResponseMessage listarUsuariosxFiltros(ListarUsuariosXFiltrosRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.ListarUsuariosXFiltros(request.nombre, request.rut, request.num_pagina, request.cant_registros, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                cant_registros = respuesta.total_registros
                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController  ({guid})->  listarUsuariosxFiltros. Mensaje al cliente: Error interno en el servicio de listar usuario por filtro. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar usuario por filtro." });
            }
        }

        [HttpGet]
        [Route("ListarInstalacion")]
        [Authorize]

        public HttpResponseMessage listarInstalacion()
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                var respuesta = _usuarioBO.listarInstalacion();
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.listaInstalacion });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       new MensajeHttpResponse() { Message = "Error interno al obtener respuesta del listado de Instalacion." });
                }

            }

            catch (Exception ex)
            {

                log.Error("UsuarioController ({guid}) ->  listarInstalacion. Mensaje al cliente: Error interno en el servicio de usuario " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar Instalacion" });


            }


        }


        [HttpGet]
        [Route("ListarEPP")]

        public HttpResponseMessage listarEpp()
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                var respuesta = _usuarioBO.listarEpp();
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.listaEpp });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de listar Epp." });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController  ({guid})->  listarEpp. Mensaje al cliente: Error interno en el servicio de usuario " +
                  "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarEpp." });

            }

        }

        [HttpGet]
        [Route("listas")]
        [Authorize]
        public HttpResponseMessage Obternetdatalistas()
        {
            string guid = Guid.NewGuid().ToString();
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el token." });
            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
            }

            var idUsuario = Convert.ToInt32(User.Identity.GetUserId());

            try
            {
                var respuesta = _usuarioBO.Obternetdatalistas(idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                listaepp = respuesta.listaEpp,
                                listaInstalacion = respuesta.listaInstalacion,
                                listausuariosxinstalalcion = respuesta.listaUsuariosPorInstalacion,
                                listaClasificacionEPP = respuesta.listaDataClasificacionEpp,
                                listasCategoriaEpp = respuesta.listaDataCategoriaEpp,
                                listaCargo = respuesta.listaCargo,
                                listaArea = respuesta.listaArea,
                                listaEmpresa = respuesta.listaEmpresa
                            });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController  ({guid})->  Obternetdatalistas. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarEpp." });
            }

        }


        [HttpGet]
        [Route("listaDatosUsuario")]
        [Authorize]
        public HttpResponseMessage ObtenerListaDatosdeUsuario()
        {
            string guid = Guid.NewGuid().ToString();
            try
            {

                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.ListarDatosUsuario(idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, DatosUsuario = respuesta.listaDatosUsuarios });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }





        }
        [HttpGet]
        [Route("usuariosxtipodecolaboradores")]

        public HttpResponseMessage ListarUsuarioxTipodecolaboradores()
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                var respuesta = _usuarioBO.ListarUsuariosxTipodecolaborador();
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.ListadeTiposColaborador });

                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de usuario por tipo de colaboradores" });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController ({guid})->  ListarUsuarioxTipodecolaboradores. Mensaje al cliente: Error interno en el servicio de usuario " +
                  "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta de usuario por tipod de colaborador." });

            }

        }
        [HttpPost]
        [Route("Cambiarcontrasenia")]
        [Authorize]
        public async Task<IHttpActionResult> CambiarContraseña(CambiarContraseñaRequest model)
        {

            string guid = Guid.NewGuid().ToString();
            string authHeader = null;
            var auth = Request.Headers.Authorization;

            if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
            if (string.IsNullOrEmpty(authHeader))
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Debe ingresar el token."));
            };
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);

            if (validToken.codigo != 1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "No se pudo validar el token."));

            }
            if (!HelperToken.validClienteCodAplicativo(validToken))
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "No se pudo validar el aplicativo."));
            }
            int idUsuario = Convert.ToInt32(User.Identity.GetUserId());
            string username = User.Identity.GetUserName();
            int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);
            try
            {
              

                if (string.IsNullOrWhiteSpace(model.NuevaContrasenia))
                {
                    throw new Exception("Password should not be empty");
                }

                var hasNumber = new Regex(@"[0-9]+");
                var hasUpperChar = new Regex(@"[A-Z]+");
                var hasMiniMaxChars = new Regex(@".{8,15}");
                var hasLowerChar = new Regex(@"[a-z]+");
                var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

                if (!hasLowerChar.IsMatch(model.NuevaContrasenia))
                {
                    return BadRequest(message: "La contraseña debe contener al menos una letra minúscula.");
                
                }
                else if (!hasUpperChar.IsMatch(model.NuevaContrasenia))
                {
                    return BadRequest(message: "La contraseña debe contener al menos una letra mayúscula");
                  
                }
                else if (!hasMiniMaxChars.IsMatch(model.NuevaContrasenia))
                {
                    return BadRequest(message: "La contraseña no debe tener menos de 8 ni más de 15 caracteres.");
              
                }
                else if (!hasNumber.IsMatch(model.NuevaContrasenia))
                {
                    return BadRequest(message: "La contraseña debe contener al menos un valor numérico.");
                   
                }

                else if (!hasSymbols.IsMatch(model.NuevaContrasenia))
                {
                    return BadRequest(message: "La contraseña debe contener al menos un carácter de caso especial.");

                }

                else
                {
                    var NewPassword = String.Concat(model.NuevaContrasenia.Where(c => !Char.IsWhiteSpace(c)));

                    if (!ModelState.IsValid)
                    {
                        return BadRequest(message: "La contraseña no debe tener menos de 8 ni más de 15 caracteres.");
                    }

                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.ActualContrasenia,
                        NewPassword);

                    var respuesta = _usuarioBO.ObtenerRespuesta(idUsuario, id_aplicacion, guid);

                    if (!result.Succeeded)
                    {
                        return BadRequest(message: "No se puede cambiar la contraseña.");
                    }

                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.OK, "Se actualizó la contraseña correctamente."));

                    
                }

                


            }
            catch (Exception ex)
            {

                log.Error($"UsuarioController  ({guid})-> ({username})-> ChangePassword. Mensaje al cliente: Error interno al obtener detalle del usuario" +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error interno al obtener detalle del usuario."));

            }


        }


        [HttpGet]
        [Route("detalle-usuario/{_id_persona}")]
        [Authorize]
        public HttpResponseMessage ObtenerDetalleUsuario(int _id_persona)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;

                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.ObtenerDetalleUsuario(_id_persona, validToken.cod_aplicacion);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, data = respuesta.data });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener el detalle del usuario" });
                }

            }
            catch (Exception ex)
            {

                log.Error($"UsuarioController  ({guid})-> id_usuario-> ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener detalle del usuario" +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener detalle del usuario." });
            }





        }
        [HttpGet]
        [Route("DetalleActa")]
        [Authorize]
        public HttpResponseMessage ListarDetalleDeActa(int id_entrega)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.ListadoDetalleActaEntrega(id_entrega, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK || respuesta.codigoRes == HttpStatusCode.NoContent)

                    {
                        if (respuesta.datosColaborador == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK,
                            new { Message = respuesta.mensajeRes, Recibe = respuesta.datosColaborador, listaEPP = respuesta.datosColaborador, Creador = respuesta.datosResponsable.cabecera, listaColaboradores = respuesta.datosResponsable.listadoColaboradores });

                        }
                        else

                            return Request.CreateResponse(HttpStatusCode.OK,
                                new { Message = respuesta.mensajeRes, Recibe = respuesta.datosColaborador.cabecera, listaEPP = respuesta.datosColaborador.listadoEpp, Creador = respuesta.datosResponsable, listaColaboradores = respuesta.datosResponsable });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error("UsuarioController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }

        [HttpPost]
        [Route("cargausuarios/procesocarga")]
        [Authorize]
        public async Task<IHttpActionResult> CrearProcesoCargaUsuarios()
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var validToken = HelperToken.LeerToken(principal);
            if (validToken.codigo != 1)
            {
                return Unauthorized();
            }

            if (!Request.Content.IsMimeMultipartContent())
            {
                return Ok(new CreacionProcesoCargaUsuariosResponse() { codigo = -1, descripcion = "No se puede cargar el archivo." });
            }
            HttpContent file = null;
            var streamProvider = new MultipartMemoryStreamProvider();
            var task = Request.Content.ReadAsMultipartAsync(streamProvider);
            if (streamProvider.Contents.Count > 0)
            {
                file = streamProvider.Contents[0];
            }

            string nameFileExcel = file.Headers.ContentDisposition.FileName;
            if (!String.IsNullOrWhiteSpace(nameFileExcel))
            {
                if (nameFileExcel.StartsWith("\"", StringComparison.Ordinal) && nameFileExcel.EndsWith("\"", StringComparison.Ordinal)
                        && nameFileExcel.Length > 1)
                {
                    nameFileExcel = nameFileExcel.Substring(1, nameFileExcel.Length - 2);
                }
            }

            var UUIDusuario = User.Identity.GetUserId();
            var listadoDatos = await _usuarioBO.CrearProcesoCargaUsuarios_Background_LeerArchivoGetData(file);



            if (listadoDatos == null || listadoDatos.Count == 0)
            {
                return Ok(new CreacionProcesoCargaUsuariosResponse() { codigo = -1, descripcion = "El archivo cargado no contiene datos" });
            }

            var response = _usuarioBO.CrearProcesoCargaUsuarios(UUIDusuario, file, nameFileExcel);
            if (response.codigo == 1)
            {
                log.Warn($"UsuarioController  ({response.idProcesoCargaUsuarios})->  CrearProcesoCargaUsuarios. Inicio jecucion segundo plano carga usuarios.");
                Task.Run(() =>
                {
                    IniciarProcesoCargaUsuarios(UUIDusuario, listadoDatos, response.idProcesoCargaUsuarios, response.codProcesoCargaUsuarios);
                });
            }
            return Ok(response);
        }

        private async Task<bool> IniciarProcesoCargaUsuarios(string UUIDusuario, List<RegistroProcesoCargaUsuariosRequest> listadoDatos, int idProcesoCargaUsuarios,
            string codProcesoCargaUsuarios)
        {
            log.Warn($"UsuarioController  ({idProcesoCargaUsuarios})->  CrearProcesoCargaUsuarios. Ejecutando carga usuarios.");
            await _usuarioBO.CrearProcesoCargaUsuarios_Background(UUIDusuario, listadoDatos,
                    idProcesoCargaUsuarios, codProcesoCargaUsuarios);
            log.Warn($"UsuarioController  ({idProcesoCargaUsuarios})->  CrearProcesoCargaUsuarios. Inicio actualización finalización carga usuario.");
            //CrearProcesoCargaUsuariosResponse resInitProc = await resTask;

            var resFinalizarCarga = _usuarioBO.ActualizarProcesoCargaUsuariosFinalizado(idProcesoCargaUsuarios, UUIDusuario);
            return true;
        }

        [HttpGet]
        [Route("listas-web")]

        public HttpResponseMessage ObtenerListarWeb()
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                var respuesta = _usuarioBO.ListarTablasMaestrasWEB();
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,

                                listaClasificacionEPP = respuesta.listaDataClasificacionEppWeb,
                                listasCategoriaEpp = respuesta.listaDataCategoriaEppWeb,
                                listaEstadoActa = respuesta.listaDataEstadoEntregaWeb,
                                listaTiposdeColaboradores = respuesta.listaDataTipoColaboradorWeb,
                                ListaInstalacion = respuesta.listaDataInstalacionesWeb,
                                ListaEmpresa = respuesta.listaDataEmpresaWeb,
                                ListaRolesUsuario = respuesta.listaDataRolUsuarioWeb,
                                ListaAreas = respuesta.listaDataAreasWeb,
                                ListaCargo = respuesta.listaDataCargoWeb

                            });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController  ({guid})->  Obternetdatalistas. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarEpp." });
            }

        }

        [HttpPost]
        [Route("Listar-Carga")]
        [Authorize]
        public HttpResponseMessage ListarCargaProcesoUsuario(ProcesoCargalistadoRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.listarProcesoCargo(request.num_pagina, request.cant_registros, request.criterio_busqueda, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                total_registros = respuesta.total_registros

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }


        [HttpGet]
        [Route("detallecargaUsuario/{id_proceso_Carga}")]
        [Authorize]
        public HttpResponseMessage ListarDetalleCargaUsuario(int id_proceso_Carga)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.DetalleProcesoCarga(id_proceso_Carga, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,


                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }

        [HttpGet]
        [Route("listaserror")]

        public HttpResponseMessage listaserror()
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                var respuesta = _usuarioBO.ObtenerListaTipoError();
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                listaTipoerrores = respuesta.ListaErroesCarga

                            });
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });

                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener los tipo errores de carga" });
                }

            }
            catch (Exception ex)
            {

                log.Error("UsuarioController  ({guid})->  Obternetdatalistas. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener los tipo errores de carga" });
            }

        }
        [HttpPost]
        [Route("Listar-Carga-errores")]
        [Authorize]
        public HttpResponseMessage listarProcesoCargoErrores(ProcesoCargaErroresRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.listarProcesoCargoErrores(request.idProcesoCarga, request.idsCargaErrorEstados, request.criterioBusqueda, request.num_pagina, request.cant_registro, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                total_registros = respuesta.total_registros

                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }
        [HttpPost]
        [Route("Listar-Carga-errores-excel")]
        [Authorize]
        public HttpResponseMessage listarProcesoCargoErroresExcel(ProcesoCargaErroresExcelRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                var respuesta = _usuarioBO.listarProcesoCargoErroresExcel(request.idProcesoCarga, request.idsCargaErrorEstados, request.criterioBusqueda, idUsuario);
                if (respuesta != null)
                {
                    if (respuesta.codigoRes == HttpStatusCode.OK)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,


                            });
                    }
                    else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    return Request.CreateResponse(respuesta.codigoRes,
                        new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                }
            }
            catch (Exception ex)
            {
                log.Error("ColaboradorController  ({guid})->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio de reporte de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de  de reporte de colaboradores." });
            }
        }

    }

}
