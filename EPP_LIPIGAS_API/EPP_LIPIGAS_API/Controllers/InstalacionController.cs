﻿using EPP_LIPIGAS_API.Ulti;
using EPPLipigas.Business.Contrato;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Instalacion.Request;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace EPP_LIPIGAS_API.Controllers
{

    [RoutePrefix("instalacion")]
    public class InstalacionController : ApiController
    {
        private readonly IInstalacionBO _instalacionBO;
        private readonly ILog log = LogManager.GetLogger(typeof(InstalacionController));
        public InstalacionController(IInstalacionBO instalacionBO)
        {
            _instalacionBO = instalacionBO;
        }

        [HttpPost]
        [Route("crear-instalacion")]
        [Authorize]
        public HttpResponseMessage CrearIntalacion(CrearInstalacionRequest req)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                if (ModelState.IsValid)
                {
                    string authHeader = null;
                    var auth = Request.Headers.Authorization;
                    if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                    if (string.IsNullOrEmpty(authHeader))
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                    };
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                    var validToken = HelperToken.LeerToken(principal);
                    if (validToken.codigo != 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                    }
                    if (!HelperToken.validClienteCodAplicativo(validToken))
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                    }
                    var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                    int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);

                    var respuesta = _instalacionBO.CrearInstalacion(id_aplicacion, idUsuario, req.nombre_instalacion, req.cod_tipo_instalacion);
                    if (respuesta != null)
                    {

                        return Request.CreateResponse(respuesta.codigoRes,
                          new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                          new MensajeHttpResponse() { Message = "Error interno al crear instalación" });
                    }
                }
                else
                {
                    List<string> errores = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                          new MensajeHttpResponse() { Message = errores[0] });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  crearIntalacion. Mensaje al cliente: Error interno en el servicio de crear instalación." +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de crear instalación." });
            }
        }

        [HttpDelete]
        [Route("eliminar-instalacion/{id_instalacion}")]
        [Authorize]
        public HttpResponseMessage EliminarInstalacion(int id_instalacion)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);

                var respuesta = _instalacionBO.EliminarInstalacion(id_aplicacion, idUsuario, id_instalacion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(HttpStatusCode.OK,
                      new { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }

        [HttpPut]
        [Route("editar-instalacion/{id_instalacion}")]
        public HttpResponseMessage EditarInstalacion(EditarInstalacionRequest req, int id_instalacion)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                string authHeader = null;
                var auth = Request.Headers.Authorization;
                if (auth != null && auth.Scheme == "Bearer") authHeader = auth.Parameter;
                if (string.IsNullOrEmpty(authHeader))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "Debe ingresar el token." });
                };
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var validToken = HelperToken.LeerToken(principal);
                if (validToken.codigo != 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                }
                if (!HelperToken.validClienteCodAplicativo(validToken))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                }
                var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                int id_aplicacion = Convert.ToInt32(validToken.cod_aplicacion);

                var respuesta = _instalacionBO.EditarInstalacion(id_aplicacion, idUsuario, id_instalacion, req.cod_tipo_instalacion, req.nombre_instalacion, req.activo);
                if (respuesta != null)
                {

                    return Request.CreateResponse(respuesta.codigoRes,
                      new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener las listas" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener respuesta listarDatosUusario." });
            }

        }

        [HttpGet]
        [Route("detalle-instalacion/{id_instalacion}")]
        public HttpResponseMessage DetalleInstallacion(int id_instalacion)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                var respuesta = _instalacionBO.DetalleInstalacion(id_instalacion);
                if (respuesta != null)
                {

                    return Request.CreateResponse(HttpStatusCode.OK,
                    new { Message = respuesta.mensajeRes, data = respuesta.datos });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno al obtener el detalle de la instalación" });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  ObtenerListaDatosdeUsuario. Mensaje al cliente: Error interno en el servicio de detalle de instalaciión" +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de detalle de instalaciión." });
            }
        }

        [HttpPost]
        [Route("lista-instalaciones-x-filtros")]
        [Authorize]
        public HttpResponseMessage ListarInstalacionXFiltros(ListarInstalacionXFiltroRequest request)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                if (ModelState.IsValid)
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                    var validToken = HelperToken.LeerToken(principal);
                    if (validToken.codigo != 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                    }
                    if (!HelperToken.validClienteCodAplicativo(validToken))
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                    }
                    var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                    var respuesta = _instalacionBO.ListarInstalacionXFiltros(request.instalacion, request.cod_tipo_instalacion, request.estado, request.num_pagina, request.cant_registros, idUsuario);
                    if (respuesta != null)
                    {
                        if (respuesta.codigoRes == HttpStatusCode.OK)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.data,
                                cant_registros = respuesta.total_registros
                            });
                        }
                        else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                        {
                            return Request.CreateResponse(HttpStatusCode.NoContent);
                        }
                        return Request.CreateResponse(respuesta.codigoRes,
                            new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                    }
                }
                else
                {
                    List<string> errores = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                          new MensajeHttpResponse() { Message = errores[0] });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  ListarInstalacionXFiltros. Mensaje al cliente: Error interno en el servicio de listar instalaciones por filtros. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar instalaciones por filtros." });
            }
        }

        [HttpGet]
        [Route("parametros-instalacion")]
        [Authorize]
        public HttpResponseMessage ListarParametrosInstalacion()
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                if (ModelState.IsValid)
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                    var validToken = HelperToken.LeerToken(principal);
                    if (validToken.codigo != 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el token." });
                    }
                    if (!HelperToken.validClienteCodAplicativo(validToken))
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            new MensajeHttpResponse() { Message = "No se pudo validar el aplicativo." });
                    }
                    var idUsuario = Convert.ToInt32(User.Identity.GetUserId());
                    var respuesta = _instalacionBO.ListarParametrosInstalacion(idUsuario, validToken.cod_aplicacion);
                    if (respuesta != null)
                    {
                        if (respuesta.codigoRes == HttpStatusCode.OK)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                Message = respuesta.mensajeRes,
                                data = respuesta.datos
                            });
                        }
                        else if (respuesta.codigoRes == HttpStatusCode.NoContent)
                        {
                            return Request.CreateResponse(HttpStatusCode.NoContent);
                        }
                        return Request.CreateResponse(respuesta.codigoRes,
                            new MensajeHttpResponse() { Message = respuesta.mensajeRes });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            new MensajeHttpResponse() { Message = "Error interno al obtener respuesta." });
                    }
                }
                else
                {
                    List<string> errores = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                          new MensajeHttpResponse() { Message = errores[0] });
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionController  ({guid})->  ListarParametrosInstalacion. Mensaje al cliente: Error interno en el servicio de listar parametros instalación. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                      new MensajeHttpResponse() { Message = "Error interno en el servicio de listar parametros instalación." });
            }
        }
    }
}