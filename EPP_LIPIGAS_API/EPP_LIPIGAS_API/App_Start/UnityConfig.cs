﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.Business.Implementacion;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Implementacion;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace EPP_LIPIGAS_API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            #region Business

            container.RegisterType<IUsuarioBO, UsuarioBO>();
            container.RegisterType<IActasBO, ActasBO>();
            container.RegisterType<IColaboradorBO, ColaboradorBO>();
            container.RegisterType<IAplicacionBO, AplicacionBO>();
            container.RegisterType<IModuloBO, ModuloBO>();
            container.RegisterType<IInstalacionBO, InstalacionBO>();
            container.RegisterType<IEppsBO, EppsBO>();


            #endregion

            #region DataAccess

            container.RegisterType<IUsuarioDO, UsuarioDO>();
            container.RegisterType<IActasDO, ActasDO>();
            container.RegisterType<IColaboradorDO, ColaboradorDO>();
            container.RegisterType<IAplicacionDO, AplicacionDO>();
            container.RegisterType<ModuloDO, ModuoDO>();
            container.RegisterType<IInstalacionDO, InstalacionDO>();
            container.RegisterType<IEppsDO, EppsDO>();
            container.RegisterType<IApiEDigitalDO, ApiEDigitalDO>();
            #endregion

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}