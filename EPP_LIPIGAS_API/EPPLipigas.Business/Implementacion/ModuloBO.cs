﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Modulo;
using System.Net;

namespace EPPLipigas.Business.Implementacion
{
    public class ModuloBO : IModuloBO
    {
        private readonly ModuloDO _ModuloDO;
        public ModuloBO(ModuloDO ModuloDO)
        {
            _ModuloDO = ModuloDO;
        }

        public ModuloResponse ModuloRolesXUsuarios(int idUsuario)
        {

            var response = _ModuloDO.ModuloRolesXUsuarios(idUsuario);

            if (response != null || response.Count > 0)
            {
                return new ModuloResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos extraidos correctamente",
                    data = response
                };
            }
            else
            {

                return new ModuloResponse
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "No se obtuvo obetener modulos"
                };
            }


        }
    }
}
