﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Aplicacion;

namespace EPPLipigas.Business.Implementacion
{
    public class AplicacionBO : IAplicacionBO
    {
        private readonly IAplicacionDO _aplicacionDO;
        public AplicacionBO(IAplicacionDO aplicacionDO)
        {
            _aplicacionDO = aplicacionDO;
        }

        public ListarParametrosResponse ListarParametros(string clientname, string clientsecret, string guid)
        {
            return _aplicacionDO.ListarParametros(clientname, clientsecret, guid);
        }
    }
}
