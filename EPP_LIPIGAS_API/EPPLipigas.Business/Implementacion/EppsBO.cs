﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Epp.Request;
using EPPLipigas.Entities.Epp.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Net;

namespace EPPLipigas.Business.Implementacion
{
    public class EppsBO : IEppsBO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(IEppsBO));
        private readonly IEppsDO _eppsDO;
        public EppsBO(IEppsDO eppsDO)
        {
            _eppsDO = eppsDO;

        }
        public ListarEppsXFiltroResponse ListarEppsXFiltros(string categoria, string clasificacion, string epp, string estado, int num_pagina, int cant_registros, int idUsuario)
        {

            var datosUsuariosXFiltros = _eppsDO.ListarEppsXFiltros(categoria, clasificacion, epp, estado, num_pagina, cant_registros, idUsuario);
            if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
            {
                return new ListarEppsXFiltroResponse()
                {
                    codigoRes = datosUsuariosXFiltros.codigoRes,
                    mensajeRes = datosUsuariosXFiltros.mensajeRes
                };
            }
            if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
            {
                return new ListarEppsXFiltroResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "No se obtuvieron el detalle de los epps por filtros."
                };
            }
            return new ListarEppsXFiltroResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos obtenidos correctamente.",
                total_registros = datosUsuariosXFiltros.total_registros,
                data = datosUsuariosXFiltros.data
            };
        }
        public RegistrarEppsResponse RegistrarEpps(string cod_clasificacion, string cod_categoria, int aplicacion, int usuario, string epp)
        {

            string guid = Guid.NewGuid().ToString();
            var response = new RegistrarEppsResponse();

            try
            {
                var respuesta = _eppsDO.RegistrarEpps(cod_clasificacion, cod_categoria, aplicacion, usuario, epp);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {

                    return new RegistrarEppsResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,

                    };

                }
                else
                {

                    return new RegistrarEppsResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,

                    };


                }

            }
            catch (Exception ex)
            {
                log.Error($"EppsBO ({guid})-> Usuario: ({usuario}) ->RegistrarEpps. Mensaje al cliente: Error interno al registrar epps" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new RegistrarEppsResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al registrar epps "
                };


            }
        }
        public EliminarEppResponse EliminarEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new RegistrarEppsResponse();

            try
            {
                var respuesta = _eppsDO.EliminarEpp(idEquipoProteccionPersonal, id_usuario, cod_aplicacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new EliminarEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
                else
                {

                    return new EliminarEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }

            }
            catch (Exception ex)
            {
                log.Error($"EppsBO ({guid})-> Usuario: ({id_usuario}) ->EliminarEpp. Mensaje al cliente: Error interno al eliminar epp" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new EliminarEppResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al eliminar epp"
                };
            }
        }
        public EditarEppResponse EditarEpp(int idEquipoProteccionPersonal, EditarEppRequest request, int id_usuario, string cod_aplicacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new RegistrarEppsResponse();

            try
            {
                var respuesta = _eppsDO.EditarEpp(idEquipoProteccionPersonal, request, id_usuario, cod_aplicacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new EditarEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
                else
                {

                    return new EditarEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }

            }
            catch (Exception ex)
            {
                log.Error($"EppsBO ({guid})-> Usuario: ({id_usuario}) ->EditarEpp. Mensaje al cliente: Error interno al editar epp" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new EditarEppResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al editar epp"
                };
            }
        }
        public ObtenerDetalleEppResponse ObtenerDetalleEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new RegistrarEppsResponse();

            try
            {
                var respuesta = _eppsDO.ObtenerDetalleEpp(idEquipoProteccionPersonal, id_usuario, cod_aplicacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new ObtenerDetalleEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,
                        datos = respuesta.datos
                    };
                }
                else
                {

                    return new ObtenerDetalleEppResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }

            }
            catch (Exception ex)
            {
                log.Error($"EppsBO ({guid})-> Usuario: ({id_usuario}) ->ObtenerDetalleEpp. Mensaje al cliente: Error interno al obtener detalle epp" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ObtenerDetalleEppResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener detalle epp"
                };
            }
        }

        public ReporteEppsResponse ListarEppsxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario)
        {


            try
            {
                var datofechadesde = String.IsNullOrEmpty(fechaDesde);
                var datofechaHasta = String.IsNullOrEmpty(fechaHasta);

                if (datofechadesde == false && datofechaHasta == false)

                {
                    var fechadesde1 = Convert.ToDateTime(fechaDesde);
                    var fechaHasta1 = Convert.ToDateTime(fechaHasta);

                    if (fechadesde1 > fechaHasta1)
                    {
                        return new ReporteEppsResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "La fecha final debe ser mayor que la fecha inicial"
                        };


                    }

                }

                var datosUsuariosXFiltros = _eppsDO.ListarEppsxfiltro(fechaDesde, fechaHasta, clasificacion, categoria, nombre_producto_epp, rut_colaborador, tipo_colaborador, num_pagina, cant_registro, idUsuario);
                if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
                {
                    return new ReporteEppsResponse()
                    {
                        codigoRes = datosUsuariosXFiltros.codigoRes,
                        mensajeRes = datosUsuariosXFiltros.mensajeRes
                    };
                }
                if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
                {
                    return new ReporteEppsResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron el detalle de los reportes de epp"
                    };
                }
                return new ReporteEppsResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    total_registros = datosUsuariosXFiltros.total_registros,
                    data = datosUsuariosXFiltros.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"EppsBO ()-> Usuario: () ->Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio listar los reportes de epss." +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ReporteEppsResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar los reportes de epss."
                };

            }


        }
        public ReportEppExcelResponse ListarEppsExcelxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int idUsuario)
        {

            try
            {

                var datofechadesde = String.IsNullOrEmpty(fechaDesde);
                var datofechaHasta = String.IsNullOrEmpty(fechaHasta);

                if (datofechadesde == false && datofechaHasta == false)

                {
                    var fechadesde1 = Convert.ToDateTime(fechaDesde);
                    var fechaHasta1 = Convert.ToDateTime(fechaHasta);

                    if (fechadesde1 > fechaHasta1)
                    {
                        return new ReportEppExcelResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "La fecha final debe ser mayor que la fecha inicial"
                        };


                    }

                }
                var datosUsuariosXFiltros = _eppsDO.ListarEppsExcelxfiltro(fechaDesde, fechaHasta, clasificacion, categoria, nombre_producto_epp, rut_colaborador, tipo_colaborador, idUsuario);
                if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
                {
                    return new ReportEppExcelResponse()
                    {
                        codigoRes = datosUsuariosXFiltros.codigoRes,
                        mensajeRes = datosUsuariosXFiltros.mensajeRes
                    };
                }
                if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
                {
                    return new ReportEppExcelResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron el detalle de los reportes de epp"
                    };
                }
                return new ReportEppExcelResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    data = datosUsuariosXFiltros.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"EppsBO ()-> Usuario: () ->Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio listar los reportes de epss." +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ReportEppExcelResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar los reportes de epss."
                };

            }



        }
    }
}
