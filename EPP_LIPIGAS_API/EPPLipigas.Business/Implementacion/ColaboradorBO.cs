﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Actas;
using EPPLipigas.Entities.Colaborador.Request;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.Constantes;
using EPPLipigas.Entities.EDigital.Response;
using log4net;
using Newtonsoft.Json;
using PdfFileWriter;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace EPPLipigas.Business.Implementacion
{
    public class ColaboradorBO : IColaboradorBO
    {
        private string _UBICACION_IMAGNE_PDF = ConfigurationManager.AppSettings["UBICACION_ARCHIVO_PDF"];
        private string _TEMPLATE_ID = ConfigurationManager.AppSettings["DOCUMENTO_TEMPLATE_ID"];
        private string _DOCUMENTO_META_ORGANIZACION = ConfigurationManager.AppSettings["DOCUMENTO_META_ORGANIZACION"];
        private string _MENSAJE_API_EDIGITAL = ConfigurationManager.AppSettings["MENSAJE_API_EDIGITAL"];
        private string _MENSAJE_API_EDIGITAL1 = ConfigurationManager.AppSettings["MENSAJE_API_EDIGITAL1"];
       private string _apiIframe = ConfigurationManager.AppSettings["API_EDIGITAL_IFRAME_URL"];
        private readonly IColaboradorDO _colaboradorDO;
        private readonly IApiEDigitalDO _ApiEDigitalDO;
        private readonly IActasDO _actasDO;
        private readonly ILog log = LogManager.GetLogger(typeof(IColaboradorBO));
        private double alturatotalEnPagina = 0.0;
        private double alturatotalContenido = 8.99;
        private PdfDocument Document;
        private PdfContents BaseContents;
        private PdfContents Contents;
        private PdfPage Page;
        public ColaboradorBO(IColaboradorDO ColaboradorDO, IApiEDigitalDO apiEDigital, IActasDO actasDO)
        {
            _colaboradorDO = ColaboradorDO;
            _ApiEDigitalDO = apiEDigital;
            _actasDO = actasDO;

        }
        public BuscarColaboradorResponse BuscarColaborador(string rut)
        {
            return _colaboradorDO.BuscarColaborador(rut);
        }
        public RegistrarColaboradorResponse RegistrarColaborador(RegistrarColaboradorRequest request, int id_usuario, int id_aplicacion)
        {

            var validData = RegistrarColaborador_ValidData(request);
            if (validData.codigoRes != HttpStatusCode.OK)
            {
                return validData;
            }

            if (String.IsNullOrEmpty(request.correo))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El correo es obligatorio"
                };
            }

            if (String.IsNullOrEmpty(request.celular))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El celular es obligatorio"
                };
            }
            else
            {
                if (!request.celular.All(char.IsDigit))
                {
                    return new RegistrarColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Solo  ingresar números",
                    };
                }
                else if (request.celular.Length != 9)
                {

                    return new RegistrarColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Se debe ingresar 9 dígitos",
                    };

                }
            }

            if (String.IsNullOrEmpty(request.genero))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El género es obligatorio"
                };
            }
            if (!(request.genero == "F" || request.genero == "M"))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El género ingresado no es correcto"
                };
            }




            if ((!new EmailAddressAttribute().IsValid(request.correo)))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Correo invalido",

                };
            }

            if ((!new EmailAddressAttribute().IsValid(request.correo)))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Correo invalido",

                };
            }

            if (char.Parse(request.genero) == 'F')
            {
                request.genero = "Femenino";

            }
            else
            {
                request.genero = "Masculino";

            }

            var validarExistenciaUsuario = _colaboradorDO.BuscarColaborador(request.rut);

            if (validarExistenciaUsuario.codigoRes != HttpStatusCode.OK)
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = validarExistenciaUsuario.codigoRes,
                    mensajeRes = validarExistenciaUsuario.mensajeRes
                };
            }
            else
            {
                if (!validarExistenciaUsuario.flagEncontrado)
                {

                    var registroPersona = _colaboradorDO.RegistrarColaborador(request, id_usuario, id_aplicacion);
                    if (registroPersona.codigoRes != HttpStatusCode.Created)
                    {
                        return new RegistrarColaboradorResponse()
                        {
                            codigoRes = registroPersona.codigoRes,
                            mensajeRes = registroPersona.mensajeRes
                        };
                    }
                    return new RegistrarColaboradorResponse()
                    {
                        codigoRes = registroPersona.codigoRes,
                        mensajeRes = registroPersona.mensajeRes,
                        id_persona = registroPersona.id_persona
                    };
                }
                else
                {
                    return new RegistrarColaboradorResponse()
                    {
                        codigoRes = validarExistenciaUsuario.codigoRes,
                        mensajeRes = validarExistenciaUsuario.mensajeRes
                    };
                }
            }
        }
        public RegistrarColaboradorAppResponse RegistrarColaboradorAPP(RegistrarColaboradorAppRequest request, int id_usuario, int id_aplicacion)
        {
            var validData = RegistrarColaborador_ValidData_app(request);
            if (validData.codigoRes != HttpStatusCode.OK)
            {
                return validData;
            }

            var validarExistenciaUsuario = _colaboradorDO.BuscarColaborador(request.rut);

            if (validarExistenciaUsuario.codigoRes != HttpStatusCode.OK)
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = validarExistenciaUsuario.codigoRes,
                    mensajeRes = validarExistenciaUsuario.mensajeRes
                };
            }
            else
            {
                if (!validarExistenciaUsuario.flagEncontrado)
                {
                    if (request.correo == null)
                    {
                        return new RegistrarColaboradorAppResponse()
                        {
                            codigoRes = HttpStatusCode.NoContent,
                            mensajeRes = "El correo es obligatorio"
                        };
                    }
                    if (String.IsNullOrEmpty(request.celular))
                    {
                        return new RegistrarColaboradorAppResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "El celular es obligatorio"
                        };
                    }
                    else
                    {
                        if (!request.celular.All(char.IsDigit))
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = "Solo  ingresar números",
                            };
                        }
                        else if (request.celular.Length != 9)
                        {

                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = "Se debe ingresar 9 dígitos",
                            };

                        }
                    }
                    if (new EmailAddressAttribute().IsValid(request.correo))
                    {
                        var registroPersona = _colaboradorDO.RegistrarColaboradorAPP(request, id_usuario, id_aplicacion);
                        if (registroPersona.codigoRes != HttpStatusCode.Created)
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = registroPersona.codigoRes,
                                mensajeRes = registroPersona.mensajeRes
                            };
                        }
                        return new RegistrarColaboradorAppResponse()
                        {
                            codigoRes = registroPersona.codigoRes,
                            mensajeRes = registroPersona.mensajeRes,
                            id_persona = registroPersona.id_persona
                        };

                    }
                    return new RegistrarColaboradorAppResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Correo invalido",

                    };

                }
                else
                {
                    return new RegistrarColaboradorAppResponse()
                    {
                        codigoRes = validarExistenciaUsuario.codigoRes,
                        mensajeRes = validarExistenciaUsuario.mensajeRes
                    };
                }
            }





        }
        private RegistrarColaboradorAppResponse RegistrarColaborador_ValidData_app(RegistrarColaboradorAppRequest request)
        {
            if (request == null)
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Debe ingresar los datos solicitados."
                };
            }
            if (String.IsNullOrEmpty(request.rut))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El rut es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_tipo_colaborador))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El tipo de colaborador es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_rol))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La rol es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_instalacion))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La instalación es obligatoria."
                };
            }
            if (String.IsNullOrEmpty(request.nombres))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Los nombres son obligatorios."
                };
            }
            if (String.IsNullOrEmpty(request.apellido_materno) || String.IsNullOrEmpty(request.apellido_paterno))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Los apellidos son obligatorios."
                };
            }
            if (String.IsNullOrEmpty(request.correo))
            {
                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El correo es obligatorio."
                };
            }
            return new RegistrarColaboradorAppResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos validados correctamente."
            };
        }
        private RegistrarColaboradorResponse RegistrarColaborador_ValidData(RegistrarColaboradorRequest request)
        {
            if (request == null)
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Debe ingresar los datos solicitados."
                };
            }
            if (String.IsNullOrEmpty(request.rut))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El rut es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_tipo_colaborador))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El tipo de colaborador es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_rol))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La rol es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_instalacion))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La instalación es obligatoria."
                };
            }
            if (String.IsNullOrEmpty(request.nombres))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Los nombres son obligatorios."
                };
            }
            if (String.IsNullOrEmpty(request.apellido_materno) || String.IsNullOrEmpty(request.apellido_paterno))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Los apellidos son obligatorios."
                };
            }
            if (String.IsNullOrEmpty(request.correo))
            {
                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El correo es obligatorio."
                };
            }
            return new RegistrarColaboradorResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos validados correctamente."
            };
        }
        public DetalleEntregaResponse ObteneDetallaeEntregaPorColaborador(int id_colaborador_entrega, int idusuario)
        {
            var response = new DetalleEntregaResponse();

            try
            {
                var ListadoDetalleColaborador = _colaboradorDO.ObtenerDetalleActaPorColaborador(id_colaborador_entrega);
                var listaEppColaborador = _colaboradorDO.ListarEPPActaEntrega(id_colaborador_entrega).Select(x => new Epps()
                {
                    descripcion = x.descripcion,
                    cantidad = x.cantidad,
                    clasificacion = x.clasificacion,

                }).ToList();

                CabeceraCol cacaberacoldatos = new CabeceraCol();
                cacaberacoldatos.nombre = ListadoDetalleColaborador.colaborador;
                cacaberacoldatos.empresa = ListadoDetalleColaborador.nombre_empresa;
                cacaberacoldatos.codigo = ListadoDetalleColaborador.cod_entrega;
                cacaberacoldatos.estado = ListadoDetalleColaborador.nombre_estado;
                cacaberacoldatos.responsable = ListadoDetalleColaborador.responsable;


                response.datosCabecera = cacaberacoldatos;
                response.listadoEpp = listaEppColaborador;
                response.id_colaborador_entrega = ListadoDetalleColaborador.id_colaborador_entrega;
                response.flag_boton_aprobar = ListadoDetalleColaborador.flag_boton_aprobar;
                response.flag_boton_entrega = ListadoDetalleColaborador.flag_boton_entrega;

                response.codigoRes = HttpStatusCode.OK;
                response.mensajeRes = "Se obtuvieron los detalles del acta de entrega por colaborador correctamente.";

            }
            catch (Exception e)
            {
                log.Error($"UsuarioDO ->({idusuario})->  ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener el detalle del usuario";
            }

            return response;

        }
        public DatosEntregaPDF_EppsResponse ObtenerRespuestaEstadoColaboradorEntregaDatosPDF(int id_Colaborador_entrega, int id_usuario, string guid)
        {
            try
            {
                var datosEntrega = _actasDO.DatosEntregaPDF(id_Colaborador_entrega, guid);
                var datosEpps = _actasDO.DatosEppPDF(id_Colaborador_entrega, guid);

                if (datosEntrega.codigoRes == HttpStatusCode.OK && datosEpps.codigoRes == HttpStatusCode.OK)
                {
                    var responsePDF_Epps = new DatosEntregaPDF_EppsResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos.",
                        datos = new DatosEntregaPDF_EppsResponse_Detalle
                        {
                            rut = datosEntrega.datos.rut,
                            codigo_entrega = datosEntrega.datos.codigo_entrega,
                            responsable = datosEntrega.datos.responsable,
                            fecha_entrega = datosEntrega.datos.fecha_entrega,
                            estado = datosEntrega.datos.estado,
                            nombre = datosEntrega.datos.nombre,
                            apellidos = datosEntrega.datos.apellidos,
                            area_empresa = datosEntrega.datos.area_empresa,
                            entidad = datosEntrega.datos.entidad,
                            correo_colaborador = datosEntrega.datos.correo_colaborador,
                            correo_responsable = datosEntrega.datos.correo_responsable,
                            tipo_colaborador=datosEntrega.datos.tipo_colaborador,
                            empresa=datosEntrega.datos.empresa,
                            rut_responsable=datosEntrega.datos.rut_responsable,

                        }
                    };
                    responsePDF_Epps.datos.epps = datosEpps.datos.Select(x => new DatosEntregaPDF_EppsResponse_Epps()
                    {
                        epp = x.epp,
                        cantidad = x.cantidad,
                        clasificaicon = x.clasificaicon,
                        categoria=x.categoria
                    }).ToList();
                    return responsePDF_Epps;
                }
                else
                {
                    return new DatosEntregaPDF_EppsResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se encontraron datos de EPPs para el colaborador."
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid})-> Usuario: ({id_usuario}) -> ObtenerRespuestaEstadoColaboradorEntregaDatosPDF. Mensaje al cliente: Error interno al obtener los datos del pdf. " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new DatosEntregaPDF_EppsResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener los datos del pdf.",
                };
            }
        }
        public EntregaEppColaboradorResponse ObtenerRespuestaEstadoColaboradorEntrega(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid)
        {
            try
            {
                var respuestaDatosPdf = ObtenerRespuestaEstadoColaboradorEntregaDatosPDF(id_Colaborador_entrega, id_usuario, guid);
                if (respuestaDatosPdf.codigoRes != HttpStatusCode.OK)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = respuestaDatosPdf.codigoRes,
                        mensajeRes = respuestaDatosPdf.mensajeRes
                    };
                }

                var respuestaPDF = ObtenerRespuestaColaboradorEntregaDocumentoPDF(respuestaDatosPdf.datos, id_usuario, guid);
                if (respuestaPDF.codigoRes != HttpStatusCode.OK)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = respuestaPDF.codigoRes,
                        mensajeRes = respuestaPDF.mensajeRes
                    };
                }

                var respuesDatosColaborador = _colaboradorDO.ObtenerDatosColaboradorPorId(id_Colaborador_entrega, id_usuario, guid);
                if (respuesDatosColaborador.data.genero =="Femenino")
                {
                    respuesDatosColaborador.data.genero = "female";
                }else if(respuesDatosColaborador.data.genero=="Masculino")
                {
                    respuesDatosColaborador.data.genero = "male";

                }
                else
                {
                    respuesDatosColaborador.data.genero = null;
                }
                //respuesDatosColaborador.data.genero = (respuesDatosColaborador.data.genero == "Femenino") ? "female" : "male";

                var respuestaValidarDatosColaborador = ValidarDatosColaborador(respuesDatosColaborador.data.genero,respuesDatosColaborador.data.correo,respuesDatosColaborador.data.fecha_nacimiento);  
                if(respuestaValidarDatosColaborador.codigoRes == HttpStatusCode.BadRequest )
                {

                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = respuestaValidarDatosColaborador.codigoRes,
                        mensajeRes = respuestaValidarDatosColaborador.mensajeRes,
                        flag_mostrar=respuestaValidarDatosColaborador.flag_mostrar
                    };

                }


                if (respuesDatosColaborador.codigoRes != HttpStatusCode.OK)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = respuesDatosColaborador.codigoRes,
                        mensajeRes = respuesDatosColaborador.mensajeRes
                    };
                }

                var datosEntrega = _colaboradorDO.DatosEntregaPDF(id_Colaborador_entrega, guid);

                if (datosEntrega.codigoRes != HttpStatusCode.OK)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = datosEntrega.codigoRes,
                        mensajeRes = datosEntrega.mensajeRes
                    };
                }

                var tokenEDigital = _ApiEDigitalDO.GetToken();

                if (tokenEDigital.codeHTTP != HttpStatusCode.OK)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = tokenEDigital.codeHTTP,
                        mensajeRes = tokenEDigital.messageHTTP
                    };
                }

                var validarUsuario = _ApiEDigitalDO.GetValidUsuario(respuesDatosColaborador.data.rut, respuesDatosColaborador.data.tipo_documento, tokenEDigital.token.access_token, guid);

                if (validarUsuario.codeHTTP == HttpStatusCode.BadRequest && 
                    (validarUsuario.data_badquest_otros.message.ToLower() == _MENSAJE_API_EDIGITAL1.ToLower()|| validarUsuario.data_badquest_otros.errors.ToLower() == _MENSAJE_API_EDIGITAL1.ToLower()))
                {
                    var crearUsuario = _ApiEDigitalDO.CrearUsuario(respuesDatosColaborador.data.nombres,
                            respuesDatosColaborador.data.apellidos, respuesDatosColaborador.data.rut, respuesDatosColaborador.data.tipo_documento,
                            respuesDatosColaborador.data.correo, respuesDatosColaborador.data.genero, respuesDatosColaborador.data.fecha_nacimiento,
                            respuesDatosColaborador.data.celular, respuesDatosColaborador.data.tipo_usuario, tokenEDigital.token.access_token);

                    if (crearUsuario.codeHTTP == HttpStatusCode.OK)
                    {
                        return ObtenerCreacionDocumentoFirmaDigital(datosEntrega, id_Colaborador_entrega, tokenEDigital, id_usuario, aplicacion, respuestaPDF.pdfBase64, respuesDatosColaborador.data.id_entrega, guid);
                    }
                    else
                    {
                        return new EntregaEppColaboradorResponse()
                        {
                            codigoRes = crearUsuario.codeHTTP,
                            mensajeRes = crearUsuario.messageHTTP
                        };
                    }
                }
                else if (validarUsuario.codeHTTP == HttpStatusCode.OK || (validarUsuario.codeHTTP==HttpStatusCode.BadRequest && 
                    (validarUsuario.data_badquest_otros.message.ToLower()== _MENSAJE_API_EDIGITAL.ToLower() || validarUsuario.data_badquest_otros.errors.ToLower() == _MENSAJE_API_EDIGITAL.ToLower())))
                {
                    return ObtenerCreacionDocumentoFirmaDigital(datosEntrega, id_Colaborador_entrega, tokenEDigital, id_usuario, aplicacion, respuestaPDF.pdfBase64, respuesDatosColaborador.data.id_entrega, guid);
                }
                else
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = validarUsuario.codeHTTP,
                        mensajeRes = validarUsuario.messageHTTP
                    };
                }
            }

            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid})-> Usuario: ({id_usuario}) -> ObtenerRespuestaEstadoColaboradorEntrega. Mensaje al cliente: Error interno al notificar al colaborador. " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new EntregaEppColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al notificar al colaborador.",

                };
            }
        }
        private EntregaEppColaboradorResponse ObtenerCreacionDocumentoFirmaDigital(DatosEntregaPEFResponse datosEntrega, int id_Colaborador_entrega,
            GetTokenResponse tokenEDigital, int id_usuario, int aplicacion, string pdf_base64, int id_entrega, string guid)
        {
            string descripcion_documento = $"Documento para {datosEntrega.datos.rut}. Responsable: {datosEntrega.datos.rut_responsable}";
            var settings_documento = new { is_public = true };
            var metadata_documento = new { id_acta = id_entrega, id_colaborador_entrega = id_Colaborador_entrega };

            var Respuesta = _ApiEDigitalDO.CrearDocumento(datosEntrega.datos.rut, descripcion_documento,_TEMPLATE_ID, datosEntrega.datos.rut_responsable,
                metadata_documento, null, pdf_base64, null, settings_documento, tokenEDigital.token.access_token, guid);


            if (Respuesta.codeHTTP == HttpStatusCode.OK)
            {
                var GuardarDatos = _colaboradorDO.GuardarDatosDocumento(Respuesta.data.document_hash, id_usuario, aplicacion, id_Colaborador_entrega, guid);

                if (GuardarDatos.codigo == 200)
                {

                    var respuesEstado = _colaboradorDO.ObtenerRespuestaEstadoColaboradorEntrega(id_Colaborador_entrega, id_usuario, aplicacion, guid);

                    if (respuesEstado.codigoRes != HttpStatusCode.OK)
                    {
                        return new EntregaEppColaboradorResponse()
                        {
                            codigoRes = respuesEstado.codigoRes,
                            mensajeRes = respuesEstado.mensajeRes,
                        };
                    }

                    var respuestadatosguid = _colaboradorDO.ObtenerGuiddeAprobacion(id_Colaborador_entrega, id_usuario, aplicacion, guid);

                    if (respuestadatosguid.codigoRes == HttpStatusCode.OK)
                    {
                        ObtenerRespuestaEstadoColaboradorEntrega_Notificacion_EnvioCorreo(respuestadatosguid.correo,
                               _apiIframe + Respuesta.data.document_hash, guid);
                    }

                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = respuesEstado.codigoRes,
                        mensajeRes = respuesEstado.mensajeRes
                    };
                }
                else
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = (HttpStatusCode)GuardarDatos.codigo,
                        mensajeRes = GuardarDatos.descripcion
                    };
                }
            }
            else
            {
                return new EntregaEppColaboradorResponse()
                {
                    codigoRes = Respuesta.codeHTTP,
                    mensajeRes = Respuesta.messageHTTP
                };
            }
        }
        private string ObtenerRespuestaEstadoColaboradorEntrega_Notificacion_EnvioCorreo(string correoColaborador,
            string link, string guid)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(correoColaborador);
                mail.Subject = "Aprobación Entrega EPP";
                mail.Body = $"<p>A continuación le bridamos el link para su aprobación:</p><br/><p><a href=\"{link}\">{link}</a></p>";
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.Normal;
         


                SmtpClient client = new SmtpClient();
                client.Send(mail);

                return "Envío correo exitoso.";
            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid}) -> ObtenerRespuestaEstadoColaboradorEntrega_Notificacion_EnvioCorreo. Mensaje al cliente: No se pudo enviar el correo de notificación. " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));

                return "No se pudo enviar el correo de notificación.";
            }
        }

        private EntregaEppColaboradorResponse ValidarDatosColaborador( string genero,string correo,string fecha_nacimiento )
        {
            var response = new EntregaEppColaboradorResponse();
           
            try
            {
                if(String.IsNullOrEmpty(genero))
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Completar los datos faltantes.",
                        flag_mostrar = true,


                    };

                }
                if (String.IsNullOrEmpty(correo))
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Completar los datos faltantes.",
                          flag_mostrar = true,
                    };

                }
                if (String.IsNullOrEmpty(fecha_nacimiento) || fecha_nacimiento== "1900-01-01")
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "Completar los datos faltantes.",
                        flag_mostrar = true,
                    };

                }

            }
            catch (Exception ex)
            {
                log.Error($"ActaController ()-> Usuario: () ->ValidarDatosColaborador. Mensaje al cliente: Error interno en el servicio de ObtenerDatosEntregaxGuid " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                return new EntregaEppColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno el validar datos del colaborador."

                };
            }

            return response;


        }
        public DatosEntregaResponseporguid ObtenerDatosEntregaxGuid(string guid_entregado)
        {

            var response = new DatosEntregaResponseporguid();
            try
            {
                var DatosColobaradorEntregaxguid = _colaboradorDO.ObtenerDatosEntregaxGuid(guid_entregado);
                var DatosEPPEntrega = _colaboradorDO.ObtenerDatosEEPEntregaxGuid(guid_entregado).Select(x => new EppPorGuid()
                {
                    descripcion = x.descripcion,
                    cantidad = x.cantidad,
                    clasificacion = x.clasificacion
                }).ToList();

                if (DatosColobaradorEntregaxguid != null && DatosEPPEntrega != null)
                {

                    if (DatosColobaradorEntregaxguid.codigo == 200)

                    {

                        Cabeceraporguid cabeceracolporguid = new Cabeceraporguid();
                        cabeceracolporguid.guid_aprobacion = DatosColobaradorEntregaxguid.guid_aprobacion;
                        cabeceracolporguid.responsable = DatosColobaradorEntregaxguid.responsable;
                        cabeceracolporguid.colaborador = DatosColobaradorEntregaxguid.colaborador;
                        cabeceracolporguid.empresa = DatosColobaradorEntregaxguid.empresa;
                        cabeceracolporguid.rut = DatosColobaradorEntregaxguid.rut;
                        cabeceracolporguid.id_colaborador_entrega = DatosColobaradorEntregaxguid.id_colaborador_entrega;
                        cabeceracolporguid.estado_colaborador_entrega = DatosColobaradorEntregaxguid.estado_colaborador_entrega;

                        Colaboradorporguid colaboradorporguid = new Colaboradorporguid();
                        colaboradorporguid.cabeceraporguid = cabeceracolporguid;
                        response.datosColaboradorporguid = colaboradorporguid;
                        response.datosColaboradorporguid.listadoEppporguid = DatosEPPEntrega;

                        response.codigoRes = HttpStatusCode.OK;
                        response.mensajeRes = "Se obtuvieron los detalles de entrega del colaborador.";
                    }
                    else
                    {
                        Cabeceraporguid cabeceracolporguid = new Cabeceraporguid();
                        response.codigoRes = HttpStatusCode.NoContent;
                        response.mensajeRes = DatosColobaradorEntregaxguid.mensaje;
                    }
                }
                else
                {
                    return new DatosEntregaResponseporguid()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = DatosColobaradorEntregaxguid.mensaje

                    };

                }

            }
            catch (Exception ex)
            {
                log.Error($"ActaController ()-> Usuario: () ->ObtenerDatosEntregaxGuid. Mensaje al cliente: Error interno en el servicio de ObtenerDatosEntregaxGuid " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));



            }
            return response;


        }
        public AprobarxWebResponse ObtenerRespuestaAprobarxWeb(string guid_entregado, string guid_aprobacion_transaccion)
        {


            var response = new DatosEntregaResponseporguid();
            try
            {
                var obtenerdatosdocumento = _colaboradorDO.ObtenerDatosDocumentosWeb(guid_entregado);

                var tokenEDigital = _ApiEDigitalDO.GetToken();

                if (tokenEDigital.codeHTTP != HttpStatusCode.OK)
                {
                    return new AprobarxWebResponse()
                    {
                        codigoRes = tokenEDigital.codeHTTP,
                        mensajeRes = tokenEDigital.messageHTTP
                    };
                }



                var datosDocumentoEDigital = _ApiEDigitalDO.ObtenerDocumento(obtenerdatosdocumento.datos.documento, tokenEDigital.token.access_token);
                if (datosDocumentoEDigital.codeHTTP == HttpStatusCode.OK)
                {
                    string rut_colaborador = obtenerdatosdocumento.datos.rut_colaborador.Replace("-", "").ToUpper();
                    if (datosDocumentoEDigital.data.document.actions.Any(x => x.identity_document == rut_colaborador && x.action == "signed"))
                    {
                        var respuesta = _colaboradorDO.ObtenerRespuestaAprobarxWeb(guid_entregado, guid_aprobacion_transaccion);
                        if (respuesta.codigoRes == HttpStatusCode.OK)
                        {
                            return new AprobarxWebResponse()
                            {
                                codigoRes = respuesta.codigoRes,
                                mensajeRes = respuesta.mensajeRes,

                            };


                        }
                        else
                        {
                            return new AprobarxWebResponse()
                            {
                                codigoRes = respuesta.codigoRes,
                                mensajeRes = respuesta.mensajeRes,

                            };

                        }


                    }
                    else
                    {
                        return new AprobarxWebResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "Aún no se ha firmado.",

                        };

                    }


                }
                else
                {
                    return new AprobarxWebResponse()
                    {
                        codigoRes = datosDocumentoEDigital.codeHTTP,
                        mensajeRes = datosDocumentoEDigital.messageHTTP
                    };

                }


            }
            catch (Exception ex)
            {


                log.Error($"ColaboradorBO ()-> Usuario: () ->ObtenerRespuestaAprobarxWeb. Mensaje al cliente: Error interno en el servicio al obtener respuesta al aprobar web" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new AprobarxWebResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta de aprobacion "
                };

            }






        }
        public ReporteColaboradoresResponse Listarcolaboradoresxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario)
        {
            try
            {
                var datofechadesde = String.IsNullOrEmpty(fechaDesde);
                var datofechaHasta = String.IsNullOrEmpty(fechaHasta);

                if (datofechadesde == false && datofechaHasta == false)

                {
                    var fechadesde1 = Convert.ToDateTime(fechaDesde);
                    var fechaHasta1 = Convert.ToDateTime(fechaHasta);

                    if (fechadesde1 > fechaHasta1)
                    {
                        return new ReporteColaboradoresResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "La fecha final debe ser mayor que la fecha inicial"
                        };


                    }

                }



                var datosUsuariosXFiltros = _colaboradorDO.Listarcolaboradoresxfiltro(fechaDesde, fechaHasta, nro_actaEPP, estado_acta, rut_responsable, rut_colaborador, tipo_colaborador, num_pagina, cant_registro, idUsuario);
                if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
                {
                    return new ReporteColaboradoresResponse()
                    {
                        codigoRes = datosUsuariosXFiltros.codigoRes,
                        mensajeRes = datosUsuariosXFiltros.mensajeRes
                    };
                }
                if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
                {
                    return new ReporteColaboradoresResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron el detalle de los epps por filtros."
                    };
                }
                return new ReporteColaboradoresResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    total_registros = datosUsuariosXFiltros.total_registros,
                    data = datosUsuariosXFiltros.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"ColaboradorBO ()-> Usuario: () ->Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio listar colaboradorres por filtro" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ReporteColaboradoresResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar colaboradorres por filtro"
                };

            }




        }
        public ReporteColaboradorExcelResponse ListarcolaboradoresExcelxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int idUsuario)
        {
            try
            {
                var datofechadesde = String.IsNullOrEmpty(fechaDesde);
                var datofechaHasta = String.IsNullOrEmpty(fechaHasta);

                if (datofechadesde == false && datofechaHasta == false)

                {
                    var fechadesde1 = Convert.ToDateTime(fechaDesde);
                    var fechaHasta1 = Convert.ToDateTime(fechaHasta);

                    if (fechadesde1 > fechaHasta1)
                    {
                        return new ReporteColaboradorExcelResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "La fecha final debe ser mayor que la fecha inicial"
                        };
                    }
                }

                var datosUsuariosXFiltros = _colaboradorDO.ListarcolaboradoresExcelxfiltro(fechaDesde, fechaHasta, nro_actaEPP, estado_acta, rut_responsable, rut_colaborador, tipo_colaborador, idUsuario);
                if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
                {
                    return new ReporteColaboradorExcelResponse()
                    {
                        codigoRes = datosUsuariosXFiltros.codigoRes,
                        mensajeRes = datosUsuariosXFiltros.mensajeRes
                    };
                }
                if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
                {
                    return new ReporteColaboradorExcelResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron el detalle de los epps por filtros."
                    };
                }
                return new ReporteColaboradorExcelResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    data = datosUsuariosXFiltros.data
                };

            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ()-> Usuario: () ->Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno en el servicio listar colaboradorres por filtro" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ReporteColaboradorExcelResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar colaboradorres por filtro"
                };
            }
        }
        public ObtenerRespuestaColaboradorEntregaDocumentoPDFResponse ObtenerRespuestaColaboradorEntregaDocumentoPDF(DatosEntregaPDF_EppsResponse_Detalle datosPDF, int id_usuario, string guid)
        {
            try
            {
                var archivoBase64 = String.Empty;
                var RowTopPosition = 9.55;
                using (var mergedPdfStream = new MemoryStream())
                {
                    Document = new PdfDocument(8.27, 11.69, UnitOfMeasure.Inch, mergedPdfStream);
                    Document.Debug = false;
                    PdfFont NormalFont = PdfFont.CreatePdfFont(Document, "Arial", FontStyle.Regular, true);
                    PdfFont TitleFont = PdfFont.CreatePdfFont(Document, "Arial", FontStyle.Bold, true);                
                    PdfRectangle Margin = new PdfRectangle(0.05, 0.07);
                    PdfImage imagen = new PdfImage(Document, _UBICACION_IMAGNE_PDF);
                    Page = new PdfPage(Document);
                  

                    CreateBaseContents(TitleFont, NormalFont, datosPDF,imagen);
                    RowTopPosition = CreateTableColaborador(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);
                    RowTopPosition = CreateTableCabeceraEPP(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);
                    RowTopPosition = CreateTableContenidoEPP(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);
                    RowTopPosition = CreateCabeceraDeclaracion(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);

                    Document.CreateFile();

                    var archivoByte = mergedPdfStream.ToArray();
                    archivoBase64 = Convert.ToBase64String(archivoByte);
                }                

                return new ObtenerRespuestaColaboradorEntregaDocumentoPDFResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Documento PDF generado",
                    pdfBase64 = archivoBase64
                };
            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid})-> Usuario: ({id_usuario}) ->ObtenerRespuestaColaboradorEntregaDocumentoPDF. Mensaje al cliente: Error interno al generar el documento PDF" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ObtenerRespuestaColaboradorEntregaDocumentoPDFResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al generar el documento PDF"
                };
            }
        }
        public void CreateBaseContents(PdfFont TitleFont, PdfFont NormalFont, DatosEntregaPDF_EppsResponse_Detalle datosPDF,PdfImage imagen) 
        {
            BaseContents = new PdfContents(Document);
            BaseContents.SaveGraphicsState();



            BaseContents.DrawImage(imagen, 0.42,10.35, 2);
            BaseContents.DrawText(TitleFont, 22, 4.135, 10.6, TextJustify.Center, "EPPS-PERSONAL INTERNO");        
            BaseContents.DrawText(NormalFont, 8, 6.35, 10.9, TextJustify.Left, "Entidad : " + datosPDF.entidad);
            BaseContents.DrawText(NormalFont, 8, 6.35, 10.7, TextJustify.Left, "Código : " + datosPDF.codigo_entrega);
            BaseContents.DrawText(NormalFont, 8, 3.4, 10.4, TextJustify.Left, "Fecha de entrega : " + datosPDF.fecha_entrega); 

            BaseContents.DrawText(NormalFont, 8, 0.35, 10.3, TextJustify.Left, "────────────────────────────────────────────────────────────────────────────────────────────────");
            BaseContents.DrawText(NormalFont, 8, 0.35, 10.28, TextJustify.Left, "────────────────────────────────────────────────────────────────────────────────────────────────");


            BaseContents.DrawText(NormalFont, 8, 4.135, 10.15, TextJustify.Center, "Entrega de EPP");

            BaseContents.DrawText(TitleFont, 8, 0.35, 10, TextJustify.Left, "────────────────────────────────────────────────────────────────────────────────────────────────");

            BaseContents.DrawText(TitleFont, 8, 0.5, 9.8, TextJustify.Left, "Responsable : ");
            BaseContents.DrawText(TitleFont, 8, 0.5, 9.65, TextJustify.Left, "Fecha : ");

            BaseContents.DrawText(NormalFont, 8, 1.3, 9.8, TextJustify.Left, "Responsable que hace la entrega (" + datosPDF.responsable.ToUpper() + ")");
            BaseContents.DrawText(NormalFont, 8, 0.9, 9.65, TextJustify.Left, datosPDF.fecha_entrega);
    
            BaseContents.RestoreGraphicsState();
        }
        public double CreateTableColaborador(PdfFont TitleFont, PdfFont NormalFont, DatosEntregaPDF_EppsResponse_Detalle datosPDF, PdfRectangle Margin, double RowTopPosition)
        {
            Contents = AddPageToDocument(true);

            Contents.SaveGraphicsState();

            PdfTable TableColaborador = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableColaborador.TableArea = new PdfRectangle(0.4, 1, 7.87, RowTopPosition);
            TableColaborador.SetColumnWidth(4.5);

            TableColaborador.CustomDrawCellEvent += BookListDrawCellEvent;
            TableColaborador.DefaultCellStyle.Margin = Margin;

            TableColaborador.Cell[0].Style = TableColaborador.CellStyle;
            TableColaborador.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaborador.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaborador.Cell[0].Style.MultiLineText = true;

            TextBox BoxColaboradorTitulo = TableColaborador.Cell[0].CreateTextBox();
            BoxColaboradorTitulo.AddText(TitleFont, 8.0, Color.Black, "COLABORADOR");

            alturatotalEnPagina = alturatotalEnPagina + BoxColaboradorTitulo.BoxHeight;

            TableColaborador.DrawRow();
            TableColaborador.Close();

            PdfTable TableColaboradorSubtitulos = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableColaboradorSubtitulos.TableArea = new PdfRectangle(0.4, 1, 7.87, TableColaborador.RowTopPosition);
            TableColaboradorSubtitulos.SetColumnWidth(1.3, 1.3, 0.7,1,1, 1.05);
            TableColaboradorSubtitulos.CustomDrawCellEvent += BookListDrawCellEvent;
            TableColaboradorSubtitulos.HeaderOnEachPage = true;
            TableColaboradorSubtitulos.DefaultCellStyle.Margin = Margin;

            TableColaboradorSubtitulos.Cell[0].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[0].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[0].Style.MultiLineText = true;


            TableColaboradorSubtitulos.Cell[1].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[1].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[1].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[1].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[1].Style.MultiLineText = true;

            TableColaboradorSubtitulos.Cell[2].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[2].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[2].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[2].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[2].Style.MultiLineText = true;

            TableColaboradorSubtitulos.Cell[3].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[3].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[3].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[3].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[3].Style.MultiLineText = true;

            TableColaboradorSubtitulos.Cell[4].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[4].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[4].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[4].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[4].Style.MultiLineText = true;

            TableColaboradorSubtitulos.Cell[5].Style = TableColaboradorSubtitulos.CellStyle;
            TableColaboradorSubtitulos.Cell[5].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorSubtitulos.Cell[5].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableColaboradorSubtitulos.Cell[5].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableColaboradorSubtitulos.Cell[5].Style.MultiLineText = true;

            TextBox BoxNombres = TableColaboradorSubtitulos.Cell[0].CreateTextBox();
            BoxNombres.AddText(TitleFont, 8.0, Color.White, "NOMBRES");

            alturatotalEnPagina = alturatotalEnPagina + BoxNombres.BoxHeight;

            TextBox BoxApellidos = TableColaboradorSubtitulos.Cell[1].CreateTextBox();
            BoxApellidos.AddText(TitleFont, 8.0, Color.White, "APELLIDOS");

            alturatotalEnPagina = alturatotalEnPagina + BoxApellidos.BoxHeight;

            TextBox BoxRut = TableColaboradorSubtitulos.Cell[2].CreateTextBox();
            BoxRut.AddText(TitleFont, 8.0, Color.White, "RUT");

            alturatotalEnPagina = alturatotalEnPagina + BoxRut.BoxHeight;

            TextBox BoxTipoColaborador = TableColaboradorSubtitulos.Cell[3].CreateTextBox();
            BoxTipoColaborador.AddText(TitleFont, 8.0, Color.White, "Tipo de colaborador");

            alturatotalEnPagina = alturatotalEnPagina + BoxTipoColaborador.BoxHeight;

            TextBox BoxEmpresa = TableColaboradorSubtitulos.Cell[4].CreateTextBox();
            BoxEmpresa.AddText(TitleFont, 8.0, Color.White, "Empresa");

            alturatotalEnPagina = alturatotalEnPagina + BoxTipoColaborador.BoxHeight;

            TextBox BoxAreasEmpresas = TableColaboradorSubtitulos.Cell[5].CreateTextBox();
            BoxAreasEmpresas.AddText(TitleFont, 8.0, Color.White, "AREA");

            alturatotalEnPagina = alturatotalEnPagina + BoxAreasEmpresas.BoxHeight;

            TableColaboradorSubtitulos.DrawRow();
            TableColaboradorSubtitulos.Close();

            PdfTable TableColaboradorContenido = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableColaboradorContenido.TableArea = new PdfRectangle(0.4, 1, 7.87, TableColaboradorSubtitulos.RowTopPosition);
            TableColaboradorContenido.SetColumnWidth(1.3, 1.3, 0.7, 1, 1, 1.05);
            TableColaboradorContenido.CustomDrawCellEvent += BookListDrawCellEvent;
            TableColaboradorContenido.HeaderOnEachPage = true;
            TableColaboradorContenido.DefaultCellStyle.Margin = Margin;

            TableColaboradorContenido.Cell[0].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[0].Style.MultiLineText = true;

            TableColaboradorContenido.Cell[1].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[1].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[1].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[1].Style.MultiLineText = true;

            TableColaboradorContenido.Cell[2].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[2].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[2].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[2].Style.MultiLineText = true;

            TableColaboradorContenido.Cell[3].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[3].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[3].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[3].Style.MultiLineText = true;

            TableColaboradorContenido.Cell[4].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[4].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[4].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[4].Style.MultiLineText = true;

            TableColaboradorContenido.Cell[5].Style = TableColaboradorContenido.CellStyle;
            TableColaboradorContenido.Cell[5].Style.Alignment = ContentAlignment.MiddleLeft;
            TableColaboradorContenido.Cell[5].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableColaboradorContenido.Cell[5].Style.MultiLineText = true;

            TextBox BoxNombresContenido = TableColaboradorContenido.Cell[0].CreateTextBox();
            BoxNombresContenido.AddText(NormalFont, 8.0, Color.Black, datosPDF.nombre.ToUpper());

            alturatotalEnPagina = alturatotalEnPagina + BoxNombresContenido.BoxHeight;

            TextBox BoxApellidosContenido = TableColaboradorContenido.Cell[1].CreateTextBox();
            BoxApellidosContenido.AddText(NormalFont, 8.0, Color.Black, datosPDF.apellidos.ToUpper());

            alturatotalEnPagina = alturatotalEnPagina + BoxApellidosContenido.BoxHeight;

            TextBox BoxRutContenido = TableColaboradorContenido.Cell[2].CreateTextBox();
            BoxRutContenido.AddText(NormalFont, 8.0, Color.Black, datosPDF.rut);

            alturatotalEnPagina = alturatotalEnPagina + BoxRutContenido.BoxHeight;

            TextBox BoxTipodeColaborador = TableColaboradorContenido.Cell[3].CreateTextBox();
            BoxTipodeColaborador.AddText(NormalFont, 8.0, Color.Black, datosPDF.tipo_colaborador);

            alturatotalEnPagina = alturatotalEnPagina + BoxTipodeColaborador.BoxHeight;

            TextBox BoxEmpresas = TableColaboradorContenido.Cell[4].CreateTextBox();
            BoxEmpresas.AddText(NormalFont, 8.0, Color.Black, datosPDF.empresa);

            alturatotalEnPagina = alturatotalEnPagina + BoxEmpresas.BoxHeight;

            TextBox BoxAreasEmpresasContenido = TableColaboradorContenido.Cell[5].CreateTextBox();
            BoxAreasEmpresasContenido.AddText(NormalFont, 8.0, Color.Black, datosPDF.area_empresa);

            alturatotalEnPagina = alturatotalEnPagina + BoxAreasEmpresasContenido.BoxHeight;

            TableColaboradorContenido.DrawRow();
            TableColaboradorContenido.Close();

            RowTopPosition = TableColaboradorContenido.RowBottomPosition;

            Contents.RestoreGraphicsState();
            return RowTopPosition;
        }
        public double CreateTableCabeceraEPP(PdfFont TitleFont, PdfFont NormalFont, DatosEntregaPDF_EppsResponse_Detalle datosPDF, PdfRectangle Margin, double RowTopPosition) 
        {
            PdfTable TableEPP = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableEPP.TableArea = new PdfRectangle(0.4, 1, 7.87, RowTopPosition - 0.1);
            TableEPP.SetColumnWidth(4.5);

            TableEPP.CustomDrawCellEvent += BookListDrawCellEvent;
            TableEPP.DefaultCellStyle.Margin = Margin;

            TableEPP.Cell[0].Style = TableEPP.CellStyle;
            TableEPP.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableEPP.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableEPP.Cell[0].Style.MultiLineText = true;

            TextBox BoxEPPTitulo = TableEPP.Cell[0].CreateTextBox();
            BoxEPPTitulo.AddText(TitleFont, 8.0, Color.Black, "EPP");

            alturatotalEnPagina = alturatotalEnPagina + BoxEPPTitulo.BoxHeight;

            TableEPP.DrawRow();
            TableEPP.Close();

            PdfTable TableEPPSubtitulos = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableEPPSubtitulos.TableArea = new PdfRectangle(0.4, 1, 7.87, TableEPP.RowTopPosition);
            TableEPPSubtitulos.SetColumnWidth(0.5, 1.4,1.4, 1.4);
            TableEPPSubtitulos.CustomDrawCellEvent += BookListDrawCellEvent;
            TableEPPSubtitulos.HeaderOnEachPage = true;
            TableEPPSubtitulos.DefaultCellStyle.Margin = Margin;

            TableEPPSubtitulos.Cell[0].Style = TableEPPSubtitulos.CellStyle;
            TableEPPSubtitulos.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableEPPSubtitulos.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableEPPSubtitulos.Cell[0].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableEPPSubtitulos.Cell[0].Style.MultiLineText = true;

            TableEPPSubtitulos.Cell[1].Style = TableEPPSubtitulos.CellStyle;
            TableEPPSubtitulos.Cell[1].Style.Alignment = ContentAlignment.MiddleLeft;
            TableEPPSubtitulos.Cell[1].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableEPPSubtitulos.Cell[1].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableEPPSubtitulos.Cell[1].Style.MultiLineText = true;

            TableEPPSubtitulos.Cell[2].Style = TableEPPSubtitulos.CellStyle;
            TableEPPSubtitulos.Cell[2].Style.Alignment = ContentAlignment.MiddleLeft;
            TableEPPSubtitulos.Cell[2].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableEPPSubtitulos.Cell[2].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableEPPSubtitulos.Cell[2].Style.MultiLineText = true;

            TableEPPSubtitulos.Cell[3].Style = TableEPPSubtitulos.CellStyle;
            TableEPPSubtitulos.Cell[3].Style.Alignment = ContentAlignment.MiddleLeft;
            TableEPPSubtitulos.Cell[3].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableEPPSubtitulos.Cell[3].Style.BackgroundColor = Color.FromArgb(18, 91, 204);
            TableEPPSubtitulos.Cell[3].Style.MultiLineText = true;

            TextBox Boxcantidad = TableEPPSubtitulos.Cell[0].CreateTextBox();
            Boxcantidad.AddText(TitleFont, 8.0, Color.White, "CANTIDAD");

            alturatotalEnPagina = alturatotalEnPagina + Boxcantidad.BoxHeight;

            TextBox BoxEPP = TableEPPSubtitulos.Cell[1].CreateTextBox();
            BoxEPP.AddText(TitleFont, 8.0, Color.White, "NOMBRE EPP");

            alturatotalEnPagina = alturatotalEnPagina + BoxEPP.BoxHeight;

            TextBox BoxClasificacion = TableEPPSubtitulos.Cell[2].CreateTextBox();
            BoxClasificacion.AddText(TitleFont, 8.0, Color.White, "CLASIFICACIÓN");

            alturatotalEnPagina = alturatotalEnPagina + BoxClasificacion.BoxHeight;

            TextBox BoxcATEGORIA = TableEPPSubtitulos.Cell[3].CreateTextBox();
            BoxcATEGORIA.AddText(TitleFont, 8.0, Color.White, "CATEGORÍA");

            alturatotalEnPagina = alturatotalEnPagina + BoxcATEGORIA.BoxHeight;

            TableEPPSubtitulos.DrawRow();
            TableEPPSubtitulos.Close();

            RowTopPosition = TableEPPSubtitulos.RowBottomPosition;
            return RowTopPosition;
        }
        public double CreateTableContenidoEPP(PdfFont TitleFont, PdfFont NormalFont, DatosEntregaPDF_EppsResponse_Detalle datosPDF, PdfRectangle Margin, double RowTopPosition) 
        {
            var cont = 1;
            var alturaInicial = 9.55;
            foreach (var itemEpp in datosPDF.epps)
            {
                PdfTable TableEPPContenido = new PdfTable(Page, Contents, NormalFont, 9.0);
                TableEPPContenido.TableArea = new PdfRectangle(0.4, 1, 7.87, RowTopPosition);
                TableEPPContenido.SetColumnWidth(0.5, 1.4,1.4, 1.4);
                TableEPPContenido.CustomDrawCellEvent += BookListDrawCellEvent;
                TableEPPContenido.HeaderOnEachPage = true;
                TableEPPContenido.DefaultCellStyle.Margin = Margin;

                TableEPPContenido.Cell[0].Style = TableEPPContenido.CellStyle;
                TableEPPContenido.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
                TableEPPContenido.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Left;
                TableEPPContenido.Cell[0].Style.MultiLineText = true;

                TableEPPContenido.Cell[1].Style = TableEPPContenido.CellStyle;
                TableEPPContenido.Cell[1].Style.Alignment = ContentAlignment.MiddleLeft;
                TableEPPContenido.Cell[1].Style.TextBoxTextJustify = TextBoxJustify.Left;
                TableEPPContenido.Cell[1].Style.MultiLineText = true;

                TableEPPContenido.Cell[2].Style = TableEPPContenido.CellStyle;
                TableEPPContenido.Cell[2].Style.Alignment = ContentAlignment.MiddleLeft;
                TableEPPContenido.Cell[2].Style.TextBoxTextJustify = TextBoxJustify.Left;
                TableEPPContenido.Cell[2].Style.MultiLineText = true;


                TableEPPContenido.Cell[3].Style = TableEPPContenido.CellStyle;
                TableEPPContenido.Cell[3].Style.Alignment = ContentAlignment.MiddleLeft;
                TableEPPContenido.Cell[3].Style.TextBoxTextJustify = TextBoxJustify.Left;
                TableEPPContenido.Cell[3].Style.MultiLineText = true;


                TextBox BoxElementos = TableEPPContenido.Cell[0].CreateTextBox();
                BoxElementos.AddText(NormalFont, 8.0, Color.Black, itemEpp.cantidad);

                alturatotalEnPagina = alturatotalEnPagina + BoxElementos.BoxHeight;

                TextBox BoxEPP = TableEPPContenido.Cell[1].CreateTextBox();
                BoxEPP.AddText(NormalFont, 8.0, Color.Black, itemEpp.epp.ToUpper());

                alturatotalEnPagina = alturatotalEnPagina + BoxEPP.BoxHeight;

                TextBox BoxClasificacion = TableEPPContenido.Cell[2].CreateTextBox();
                BoxClasificacion.AddText(NormalFont, 8.0, Color.Black, itemEpp.clasificaicon);

                alturatotalEnPagina = alturatotalEnPagina + BoxClasificacion.BoxHeight;

                TextBox BoxCategoria = TableEPPContenido.Cell[3].CreateTextBox();
                BoxCategoria.AddText(NormalFont, 8.0, Color.Black, itemEpp.categoria);

                alturatotalEnPagina = alturatotalEnPagina + BoxCategoria.BoxHeight;

                TableEPPContenido.DrawRow();
                TableEPPContenido.Close();

                RowTopPosition = TableEPPContenido.RowBottomPosition;

                if (RowTopPosition < 1.4)
                {
                    if (datosPDF.epps.Count != cont)
                    {
                        alturatotalEnPagina = alturaInicial;
                        RowTopPosition = alturaInicial;
                        Contents = AddPageToDocument();
                        //RowTopPosition = 0.9;
                        //RowTopPosition = CreateCabeceraDeclaracion(TitleFont, NormalFont, datosPDF, Margin, 9.55);
                    }
                }

                cont++;
                TableEPPContenido = null;
            }

            var flgCabeceraDeclaracion = false;

            //if (RowTopPosition - 0.8 < 1.4)
            //{
            //    if (datosPDF.epps.Count != cont)
            //    {
            //        alturatotalEnPagina = alturaInicial;
            //        RowTopPosition = alturaInicial;
            //        Contents = AddPageToDocument();
            //        //RowTopPosition = 0.9;
            //        //RowTopPosition = CreateCabeceraDeclaracion(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);
            //        flgCabeceraDeclaracion = true;
            //    }
            //}

            if (!flgCabeceraDeclaracion) 
            {
                //RowTopPosition = CreateCabeceraDeclaracion(TitleFont, NormalFont, datosPDF, Margin, RowTopPosition);
            }

            return RowTopPosition;
        }
        public double CreateCabeceraDeclaracion(PdfFont TitleFont, PdfFont NormalFont, DatosEntregaPDF_EppsResponse_Detalle datosPDF, PdfRectangle Margin, double RowTopPosition) 
        {
            PdfTable TableDeclaracion = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableDeclaracion.TableArea = new PdfRectangle(0.4, 1, 7.87, RowTopPosition - 0.1);
            //TableDeclaracion.TableArea = new PdfRectangle(0.4, 1, 7.87, alturatotalEnPagina - 0.1);
            TableDeclaracion.SetColumnWidth(4.5);

            TableDeclaracion.CustomDrawCellEvent += BookListDrawCellEvent;
            TableDeclaracion.DefaultCellStyle.Margin = Margin;

            TableDeclaracion.Cell[0].Style = TableDeclaracion.CellStyle;
            TableDeclaracion.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableDeclaracion.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Center;
            TableDeclaracion.Cell[0].Style.MultiLineText = true;

            TextBox BoxDeclaracionTitulo = TableDeclaracion.Cell[0].CreateTextBox();
            BoxDeclaracionTitulo.AddText(TitleFont, 8.0, Color.Black, "DECLARACIÓN");

            alturatotalEnPagina = alturatotalEnPagina + BoxDeclaracionTitulo.BoxHeight;

            TableDeclaracion.DrawRow();
            TableDeclaracion.Close();

            PdfTable TableDeclaracionContenido = new PdfTable(Page, Contents, NormalFont, 9.0);
            TableDeclaracionContenido.TableArea = new PdfRectangle(0.4, 1, 7.87, TableDeclaracion.RowTopPosition);
            //TableDeclaracionContenido.TableArea = new PdfRectangle(0.4, 1, 7.87, alturatotalEnPagina);
            TableDeclaracionContenido.SetColumnWidth(4.2);
            TableDeclaracionContenido.CustomDrawCellEvent += BookListDrawCellEvent;
            TableDeclaracionContenido.HeaderOnEachPage = true;
            TableDeclaracionContenido.DefaultCellStyle.Margin = Margin;

            TableDeclaracionContenido.Cell[0].Style = TableDeclaracionContenido.CellStyle;
            TableDeclaracionContenido.Cell[0].Style.Alignment = ContentAlignment.MiddleLeft;
            TableDeclaracionContenido.Cell[0].Style.TextBoxTextJustify = TextBoxJustify.Left;
            TableDeclaracionContenido.Cell[0].Style.MultiLineText = true;


            TextBox BoxDeclaracion = TableDeclaracionContenido.Cell[0].CreateTextBox();
            BoxDeclaracion.AddText(NormalFont, 8.0, Color.Black, "Por la presente declaro haber recibido los EPP indicados en este documento en óptimas condiciones de funcionamiento. Asimismo entiendo los cuidados y mantenimientos que debo realizar a los mismos.");

            alturatotalEnPagina = alturatotalEnPagina + BoxDeclaracion.BoxHeight;

            TableDeclaracionContenido.DrawRow();
            TableDeclaracionContenido.Close();

            RowTopPosition = TableDeclaracionContenido.RowBottomPosition;
            if (RowTopPosition < 2)
            {
                Contents = AddPageToDocument();
            }

            return RowTopPosition;
        }
        private bool BookListDrawCellEvent(PdfTable Table, PdfTableCell Cell)
        {
            Table.Contents.SaveGraphicsState();
            if ((string)Cell.Value == "Paperback")
                Table.Contents.SetColorNonStroking(Color.LightCyan);
            else
                Table.Contents.SetColorNonStroking(Color.LightPink);
            double PosX = Cell.ClientLeft;
            double PosY = 0.5 - (Cell.ClientBottom + Cell.ClientTop) - Cell.Style.FontLineSpacing;
            double Width = Cell.ClientRight - Cell.ClientLeft;
            double Height = 2.0 * Cell.Style.FontLineSpacing;
            Table.Contents.DrawRoundedRectangle(PosX, PosY, Width, Height, 0.25, PaintOp.Fill);
            Table.Contents.RestoreGraphicsState();
            return false;
        }
        public PdfContents AddPageToDocument(bool init = false) 
        {
            if (!init)
            {
                Page = new PdfPage(Document);
            }
            Page.AddContents(BaseContents);
            PdfContents Contents = new PdfContents(Page);
            return Contents;
        }
       public ActualizarColaboradorResponse ActualizarColaborador(int id_colaborador_entrega, string genero, string correo, string fecha_nacimiento, int id_aplicacion, int id_usuario, string guid)
        {
            var response = new ActualizarColaboradorResponse();
            try
            {
                var respuesta = _colaboradorDO.ActualizarColaborador(id_colaborador_entrega, genero, correo, fecha_nacimiento, id_aplicacion, id_usuario,guid);
                if(respuesta.codigoRes==HttpStatusCode.OK)
                {
                    response.codigoRes = respuesta.codigoRes;
                    response.mensajeRes = respuesta.mensajeRes;
                    response.flag_vista = false;

                }
                else
                {
                    response.codigoRes = respuesta.codigoRes;
                    response.mensajeRes = respuesta.mensajeRes;
                }

            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid})-> Usuario: ({id_usuario}) ->ActualizarColaborador. Mensaje al cliente: Error interno " +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                response.codigoRes = HttpStatusCode.InternalServerError;
                    response.mensajeRes = "Error Interno al  obtener respuesta de ActualizarColaborador ";

            }
            return response;

        }

        public ObtenerDocumentoDescargarResponse ObtenerDocumentodescargar(string id_documento,string guid )
        {
            var response = new ObtenerDocumentoDescargarResponse();
            try
            {
                var tokenEDigital = _ApiEDigitalDO.GetToken();

                if (tokenEDigital.codeHTTP != HttpStatusCode.OK)
                {
                    return new ObtenerDocumentoDescargarResponse()
                    {
                        codigoRes = tokenEDigital.codeHTTP,
                        mensajeRes = tokenEDigital.messageHTTP
                    };
                }

                var respuesta = _ApiEDigitalDO.DescargarDocumento(id_documento, tokenEDigital.token.access_token);
                if (respuesta.codeHTTP != HttpStatusCode.OK)
                {
                    return new ObtenerDocumentoDescargarResponse()
                    {
                        codigoRes = tokenEDigital.codeHTTP,
                        mensajeRes = tokenEDigital.messageHTTP
                    };
                }
                else
                {
                    response.link_descarga_documento = respuesta.data.document.output;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Se obtuvo el link del documento ";


                }

            }
            catch (Exception ex)
            {
                log.Error($"ColaboradorBO ({guid}) ->ObtenerDocumentodescargar. Mensaje al cliente: Error interno " +
                               "Detalle error: " + JsonConvert.SerializeObject(ex));

                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error Interno al  obtener respuesta de ObtenerDocumentodescargar.";
            }

            return response;
        }
        
    }
}

