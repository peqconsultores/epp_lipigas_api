﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Actas;
using log4net;
using Newtonsoft.Json;
using PdfFileWriter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace EPPLipigas.Business.Implementacion
{
    public class ActasBO : IActasBO
    {

        private string _url_digital = ConfigurationManager.AppSettings["API_EDIGITAL_IFRAME_URL"];
        private string _url_DOCUMENTO= ConfigurationManager.AppSettings["API_EDIGITAL_DOCUMENT_URL"];
        private readonly IActasDO _actasDO;
        private readonly IApiEDigitalDO _ApiEDigitalDO;
        private readonly ILog log = LogManager.GetLogger(typeof(ActasBO));
        public ActasBO(IActasDO ActasDO, IApiEDigitalDO apiEDigital)
        {
            _actasDO = ActasDO;
            _ApiEDigitalDO = apiEDigital;
        }
        public ListarActasEPPResponse ListarActasEPP(int idUsuario, string guid)
        {

            ListarActasEPPResponse response = new ListarActasEPPResponse();
            try
            {

                var RespuestaFlag = _actasDO.Validar_rol(idUsuario, guid);
                if (RespuestaFlag != null)
                {
                    response.codigoRes = HttpStatusCode.NotFound;
                    response.mensajeRes = "La respuestaFlag es nulo";

                }

                var datosBusqueda = _actasDO.ListarActasEPP(idUsuario, guid)
                    .Select(x => new ListarActasEPPDatosResponse()
                    {
                        id_entrega = x.id_entrega,
                        cod_entrega = x.cod_entrega,
                        fecha_entrega = String.Format("{0:dd-MM-yyyy}", x.fecha_entrega),
                        id_estado_entrega = x.id_estado_entrega,
                        nombre_estado_acta = x.estado_acta,
                        nombre_estado_entrega = x.estado_entrega
                    }).ToList();

                response.codigoRes = HttpStatusCode.OK;
                response.mensajeRes = "Lista de actas obtenidas correctamente";
                response.datos = datosBusqueda;
                response.flag = RespuestaFlag.flag;

            }
            catch (Exception ex)
            {

                log.Error($"ActasBO  ({guid})->  Usuario: {idUsuario} -> ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener detalle del usuario" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));
            }



            return response;
        }
        public SincronizarActaResponse SincronizarActa(SincronizarActasRequest req, int idUsuario, int id_aplicacion, string guid)
        {
            DataTable Datoscolaboradores = GuardarDatos_colaborador_emtrega(req.datoscolaboradores, guid);
            DataTable DatosEPP = GuardarDatos_colaborador_entrega_epp(req.datoscolaboradores, guid);

            try
            {
                var response = _actasDO.SincronizarActa(req, idUsuario, id_aplicacion, Datoscolaboradores, DatosEPP, guid);

                if (response.codigoRes == HttpStatusCode.OK)
                {
                    return new SincronizarActaResponse()
                    {
                        codigoRes = response.codigoRes,
                        mensajeRes = response.mensajeRes
                    };
                }

                return new SincronizarActaResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "No se obtuvo la sincronizacion del acta"
                };

            }
            catch (Exception ex)
            {

                log.Error($"ActasBO  ({guid})->  Usuario: {idUsuario} -> SincronizarActa. Mensaje al cliente: Error interno al SincronizarActa" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new SincronizarActaResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "error interno "
                };
            }


        }
        private DataTable GuardarDatos_colaborador_emtrega(List<listacolaboradores> req, string guid)
        {

            DataTable dataColaboradores = new DataTable() { Locale = CultureInfo.InvariantCulture };
            dataColaboradores.Columns.Add("id_persona", typeof(int));

            try
            {
                int indexDataTable = 0;

                foreach (var item in req.Select(x => new { id_persona = x.id_persona }).Distinct())
                {
                    dataColaboradores.Rows.Add();
                    dataColaboradores.Rows[indexDataTable]["id_persona"] = item.id_persona;

                    indexDataTable++;

                }
            }
            catch (Exception ex)
            {
                log.Error($"ActasBO  ({guid}) -> GuardarDatos_colaborador_emtrega. Mensaje al cliente: Error interno al Gudardar Datos Colaborador" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

            }


            return dataColaboradores;

        }
        private DataTable GuardarDatos_colaborador_entrega_epp(List<listacolaboradores> req, string guid)
        {
            DataTable datosColaboradores_epp = new DataTable() { Locale = CultureInfo.InvariantCulture };

            try
            {
                datosColaboradores_epp.Columns.Add("id_equipo_proteccion_personal", typeof(int));
                datosColaboradores_epp.Columns.Add("cantidad", typeof(string));
                datosColaboradores_epp.Columns.Add("id_persona", typeof(int));
                int indexDataTable = 0;
                foreach (var item in req.Select(x => new { id_persona = x.id_persona }).Distinct())
                {
                    var datosEppPersona = req.Where(x => x.id_persona == item.id_persona).ToList();
                    foreach (var item3 in datosEppPersona)
                    {
                        foreach (var item4 in item3.datosEpp)
                        {
                            datosColaboradores_epp.Rows.Add();
                            datosColaboradores_epp.Rows[indexDataTable]["id_equipo_proteccion_personal"] = item4.id_epp;
                            datosColaboradores_epp.Rows[indexDataTable]["cantidad"] = item4.cantidad;
                            datosColaboradores_epp.Rows[indexDataTable]["id_persona"] = item.id_persona;

                            indexDataTable++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Error($"ActasBO  ({guid}) -> GuardarDatos_colaborador_entrega_epp. Mensaje al cliente: Error interno al Gudardar Datos Colaborador con epp" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

            }


            return datosColaboradores_epp;
        }
        public AprobacionEntregaResponse AprobacionEntrega(int id_colaborador_entrega, string rut, int id_usuario, int aplicacion, string guid, System.Web.Mvc.ControllerContext controllerContex)
        {
            try
            {


                /*
                 1. obtener los datos del documento de firma digital por el id colaborador entrega (id documento que em entrega e-digital)
                 2. obtener los datos del documento de e-digital (atributos si ya se firmaron)
                 3. actualizar el acta a aprobado si ya se firmo
                 
                 */
                var datoseppPdf = _actasDO.DatosEppPDF(id_colaborador_entrega, guid);
                var datosDocumentoFirma = _actasDO.ObtenerDatosDocumentos(id_colaborador_entrega);

                if (datosDocumentoFirma.codigoRes != HttpStatusCode.OK)
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = datosDocumentoFirma.codigoRes,
                        mensajeRes = datosDocumentoFirma.mensajeRes
                    };
                }

                var tokenEDigital = _ApiEDigitalDO.GetToken();

                if (tokenEDigital.codeHTTP != HttpStatusCode.OK)
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = tokenEDigital.codeHTTP,
                        mensajeRes = tokenEDigital.messageHTTP
                    };
                }
                var datosDocumentoEDigital = _ApiEDigitalDO.ObtenerDocumento(datosDocumentoFirma.datos.documento, tokenEDigital.token.access_token);

                if (datosDocumentoEDigital.codeHTTP == HttpStatusCode.OK)
                {
                    var datosEntrega = _actasDO.DatosEntregaPDF(id_colaborador_entrega, guid);


                    string rut_colaborador = datosDocumentoFirma.datos.rut_colaborador.Replace("-", "").ToUpper();
                    if (datosDocumentoEDigital.data.document.actions.Any(x => x.identity_document == rut_colaborador && x.action == "signed"))
                    {
                        var RespuestaAporbacionEntrega = _actasDO.AprobacionEntrega(id_colaborador_entrega, rut, id_usuario, aplicacion, guid);


                        if (RespuestaAporbacionEntrega.codigoRes == HttpStatusCode.OK && tokenEDigital.codeHTTP == HttpStatusCode.OK)
                        {

                            return new AprobacionEntregaResponse()
                            {



                                documento_hash = datosDocumentoFirma.datos.documento,
                                responsable = datosEntrega.datos.responsable,
                                estado = datosEntrega.datos.estado,
                                fecha_entrega = datosEntrega.datos.fecha_entrega,
                                nombre = datosEntrega.datos.nombre,
                                apellidos = datosEntrega.datos.apellidos,
                                rut = datosEntrega.datos.rut,
                                area_empresa = datosEntrega.datos.area_empresa,
                                codigoRes = RespuestaAporbacionEntrega.codigoRes,
                                mensajeRes = RespuestaAporbacionEntrega.mensajeRes
                            };

                        }
                        else
                        {
                            return new AprobacionEntregaResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = RespuestaAporbacionEntrega.mensajeRes
                            };


                        }
                    }
                    else
                    {
                        return new AprobacionEntregaResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "Aún no se ha firmado.",
                            link = _url_digital + datosDocumentoFirma.datos.documento

                        };

                    }


                }
                else
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = datosDocumentoEDigital.codeHTTP,
                        mensajeRes = datosDocumentoEDigital.messageHTTP
                    };
                }




            }
            catch (Exception ex)
            {
                log.Error($"ActasBO ({guid})-> Usuario: ({id_usuario}) ->AprobacionEntrega. Mensaje al cliente: Error interno en el servicio de AprobacionEntrega " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                return new AprobacionEntregaResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error en el servidor",
                };
            }



        }
        public string AprobarEntrega_EnvioCorreo(string correoColaborador, string documento_hash, string correoResponsable, string nombre, string apellido)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(correoColaborador);
                mail.Subject = "Entrega EPP Colaborador " + nombre + " " + apellido;
                string link = _url_DOCUMENTO + documento_hash + "/blank";
                mail.Body = $"Se envía el acta de entrega del usuario: <br><br> <a href=\"{link}\" target=\"_blank\">Link del documento</a>";
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.Normal;
                mail.CC.Add(correoResponsable);
       


                SmtpClient client = new SmtpClient();
                client.Send(mail);

                return "Envío correo exitoso.";
            }
            catch (Exception)
            {
                //log
                return "No se pudo enviar el correo de notificación.";
            }
        }
        public RecepcionarEdigitalResponse Recepcion_edigital(RecepcionarEdigitalRequest req, string guid)
        {
            try
            {
                if (req == null)
                {
                    log.Info("Recepcion_edigital  --> RESPONSE:( HttpStatusCode.NoContent) Los campos estan vacios :" + JsonConvert.SerializeObject(req));
                    return new RecepcionarEdigitalResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "Los campos estan vacios "
                    };

                }

                else
                {
                    if (req.document_state == "signed")
                    {
                        var respuesta_documento = "Se firmó el documento";
                        int codigo = 200;
                        var Respuesta = _actasDO.Recepcion_edigital(respuesta_documento, req.document_hash, codigo);
                        log.Info("Recepcion_edigital  --> RESPONSE:( HttpStatusCode.OK) Los datos se guardaron exitosamente :" + respuesta_documento + JsonConvert.SerializeObject(req));

                    }

                    else
                    {
                        var respuesta_documento = "No se firmó el documento ";
                        int codigo = 400;
                        var Respuesta = _actasDO.Recepcion_edigital(respuesta_documento, req.document_hash, codigo);

                        log.Info("Recepcion_edigital  --> RESPONSE:( HttpStatusCode.BadRequest) :" + respuesta_documento + JsonConvert.SerializeObject(req));
                    }

                    return new RecepcionarEdigitalResponse()
                    {

                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se registró Exitosamente"
                    };

                }

            }
            catch (Exception ex)
            {

                log.Error($"ActasBO  (({guid}))->  Usuario:  -> Recepcion_edigital. Mensaje al cliente: Error interno al obtener detalle del usuario" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));
                return new RecepcionarEdigitalResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error en el servicio Recepcion_edigital "
                };
            }




        }
    }

}



