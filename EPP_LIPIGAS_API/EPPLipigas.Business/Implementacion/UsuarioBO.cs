﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.EDigital.Response;
using EPPLipigas.Entities.Epp;
using EPPLipigas.Entities.Instalacion;
using EPPLipigas.Entities.Rut;
using EPPLipigas.Entities.Usuario.Request;
using EPPLipigas.Entities.Usuario.Response;
using log4net;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EPPLipigas.Business.Implementacion
{
    public class UsuarioBO : IUsuarioBO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(IUsuarioBO));
        private readonly IUsuarioDO _usuarioDO;
        private readonly IApiEDigitalDO _ApiEDigitalDO;

        public UsuarioBO(IUsuarioDO usuarioDO, IApiEDigitalDO apiEDigital)
        {
            _usuarioDO = usuarioDO;
            _ApiEDigitalDO = apiEDigital;
        }

        public BusquedaRutResponse BusquedaRut(string rut)
        {
            try
            {

                return _usuarioDO.BusquedaRut(rut);

            }
            catch (Exception)
            {
                //log.Error("UsuarioBO ->  BusquedaRut. Mensaje al cliente: Error interno al listar los paises. " +
                //   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new BusquedaRutResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener el registro."
                };
            }
        }
        public GetTokenResponse gettoken()
        {
            try
            {
                var dato = _ApiEDigitalDO.GetToken();

                return dato;

            }
            catch (Exception)
            {
                //log.Error("UsuarioBO ->  BusquedaRut. Mensaje al cliente: Error interno al listar los paises. " +
                //   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new GetTokenResponse()
                {
                    codeHTTP = HttpStatusCode.InternalServerError,
                    messageHTTP = "Error interno al obtener el registro."
                };
            }
        }



        public CreacionUsuarioResponse CreacionUsuario(CreacionUsuarioRequest req)
        {

            string guid = Guid.NewGuid().ToString();

            var validData = CreacionUsuario_ValidData(req);
            //log.Info("UsuarioBO --> CreacionUsuario. | validData --> " + JsonConvert.SerializeObject(validData));

            if (validData.codigoRes != HttpStatusCode.OK)
            {
                return validData;
            }
            var validarExistenciaUsuario = _usuarioDO.ValidarExistenciaUsuario(req.rut);



            if (validarExistenciaUsuario.codigoRes != HttpStatusCode.OK)
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = validarExistenciaUsuario.codigoRes,
                    mensajeRes = validarExistenciaUsuario.mensajeRes
                };
            }
            return new CreacionUsuarioResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Exitoso.",
                id_persona = validarExistenciaUsuario.id_persona,
            };
        }

        //public RegistrarUsuarioResponse crearUsuario(int id_persona)
        //{


        //    var respuesta = _usuarioDO.crearUsuario(id_persona);

        //    if(respuesta.codigoRes==HttpStatusCode.OK)
        //    {

        //        return new RegistrarUsuarioResponse()
        //        {
        //            codigoRes = HttpStatusCode.OK,
        //            mensajeRes = respuesta.mensajeRes,

        //        };

        //    }
        //    else
        //    {
        //        return new RegistrarUsuarioResponse()
        //        {
        //            codigoRes = HttpStatusCode.InternalServerError,
        //            mensajeRes = respuesta.mensajeRes,

        //        };

        //    }

        //}
        public EditarUsuarioResponse EditarUsuario(EditarUsuarioRequest request, int _id_persona, int id_usuario_mod, int id_aplicacion_mod)
        {
            var valEditUsu = EditarUsuario_ValidData(request);
            if (valEditUsu.codigoRes != HttpStatusCode.OK)
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = valEditUsu.codigoRes,
                    mensajeRes = valEditUsu.mensajeRes
                };
            }
            if (char.Parse(request.genero) == 'F')
            {
                request.genero = "Femenino";

            }
            else
            {
                request.genero = "Masculino";

            }

            var editarUsuario = _usuarioDO.EditarUsuario(request, id_usuario_mod, id_aplicacion_mod, _id_persona);


            if (editarUsuario.codigoRes != HttpStatusCode.Created)
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = editarUsuario.codigoRes,
                    mensajeRes = editarUsuario.mensajeRes
                };
            }
            return new EditarUsuarioResponse()
            {
                codigoRes = editarUsuario.codigoRes,
                mensajeRes = editarUsuario.mensajeRes
            };
        }
        private EditarUsuarioResponse EditarUsuario_ValidData(EditarUsuarioRequest request)
        {
            if (request == null)
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Debe ingresar los datos solicitados."
                };
            }
            if (String.IsNullOrEmpty(request.nombres))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El nombres es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.apellido_paterno))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El apellido paterno es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.apellido_materno))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El apellido materno es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_instalacion))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La instalación es obligatoria."
                };
            }
            if (String.IsNullOrEmpty(request.cod_rol))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El rol es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.cod_tipo_colaborador))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El tiipo de colaborador es obligatorio."
                };
            }
            //if (String.IsNullOrEmpty(request.nombre_empresa))
            //{
            //    return new EditarUsuarioResponse()
            //    {
            //        codigoRes = HttpStatusCode.BadRequest,
            //        mensajeRes = "El nombre de empresa es obligatorio."
            //    };
            //}
            //if (String.IsNullOrEmpty(request.area))
            //{
            //    return new EditarUsuarioResponse()
            //    {
            //        codigoRes = HttpStatusCode.BadRequest,
            //        mensajeRes = "El area es obligatoria."
            //    };
            //}
            if (String.IsNullOrEmpty(request.correo))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El correo es obligatorio."
                };
            }
            if (String.IsNullOrEmpty(request.celular))
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El celular es obligatorio."
                };
            }
            else
            {
                if (!request.celular.All(char.IsDigit))
                {
                    return new EditarUsuarioResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "El teléfono solo puede contener números."
                    };
                }
            }
            //if (String.IsNullOrEmpty(request.cod_cargo))
            //{
            //    return new EditarUsuarioResponse()
            //    {
            //        codigoRes = HttpStatusCode.BadRequest,
            //        mensajeRes = "El cargo es obligatorio."
            //    };
            //}
            return new EditarUsuarioResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos validados correctamente."
            };
        }

        private static string ValidarNumeroTelefonico(string numeroTelefono)
        {
            string numero = numeroTelefono.Replace("+", "").Trim();
            if (!numero.StartsWith("51") && !numero.StartsWith("5"))
                numero = $"51{numero}";
            return numero;
        }
        public EliminarUsuarioResponse EliminarUsuario(int id_aplicacion_elim, int id_usuario_elim, int id_persona)
        {
            var eliminarUsuario = _usuarioDO.EliminarUsuario(id_aplicacion_elim, id_usuario_elim, id_persona);
            if (eliminarUsuario.codigoRes != HttpStatusCode.OK)
            {
                return new EliminarUsuarioResponse()
                {
                    codigoRes = eliminarUsuario.codigoRes,
                    mensajeRes = eliminarUsuario.mensajeRes
                };
            }
            return new EliminarUsuarioResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "No se obtuvo respuesta al eliminar el usuario."
            };
        }

        private CreacionUsuarioResponse CreacionUsuario_ValidData(CreacionUsuarioRequest req)
        {

            if (req == null)
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Debe ingresar los datos solicitados."
                };
            }
            if (string.IsNullOrWhiteSpace(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña no debe estar vacía."
                };
          
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña debe contener al menos una letra minúscula."
                };
               

            }
            else if (!hasUpperChar.IsMatch(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña debe contener al menos una letra mayúscula."
                };
             

            }
            else if (!hasMiniMaxChars.IsMatch(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña no debe tener menos de 8 ni más de 15 caracteres."
                };
               

            }
            else if (!hasNumber.IsMatch(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña debe contener al menos un valor numérico."
                };
               

            }
            else if (!hasSymbols.IsMatch(req.contrasenia))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "La contraseña debe contener al menos un carácter de caso especial."
                };

          

            }

            if (String.IsNullOrEmpty(req.rut))
            {
                return new CreacionUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "El rut es obligatoria."
                };
            }

         

            return new CreacionUsuarioResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos validados correctamente."
            };
        }

        public ListaInstalacionResponse listarInstalacion()
        {
            ListaInstalacionResponse response = _usuarioDO.ListarInstalacion();
            return response;
        }

        public ListarInstalacionxusuario listarUsuarioxInstalacion(int idUsuario)
        {

            var response = new ListarInstalacionxusuario();


            UsuariosPorInstalacionResponse resInst = _usuarioDO.listarUsuarioxInstalacion(idUsuario);

            if (resInst.codigoRes != HttpStatusCode.OK)
            {
                return new ListarInstalacionxusuario()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "Error en la petición"
                };

            }
            else
            {
                response.listaInstalaciones = new List<DataInstalacionUsu>();

                var listaInstalaciones = resInst.listaUsuariosPorInstalacion.Select(x => new
                {
                    id_instalacion = x.id_instalacion,
                    nombre_instalacion = x.nombre_instalacion
                }).Distinct().ToList();

                foreach (var itemInstalacion in listaInstalaciones)
                {
                    var instalacion = new DataInstalacionUsu();

                    instalacion.id_instalacion = itemInstalacion.id_instalacion;
                    instalacion.nombre_instalacion = itemInstalacion.nombre_instalacion;
                    instalacion.listaUsuario = resInst.listaUsuariosPorInstalacion.Where(x => x.id_instalacion == itemInstalacion.id_instalacion).Select(x => new DatosUsuario()
                    {
                        id_persona = x.id_persona,
                        rut = x.rut,
                        nombres = x.nombres,
                        apellido_paterno = x.apellido_paterno,
                        apellido_materno = x.apellido_materno
                    }).ToList();

                    response.listaInstalaciones.Add(instalacion);
                }

                if (response.listaInstalaciones.Count <= 0)

                {
                    return new ListarInstalacionxusuario()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "La lista esta vacia"
                    };

                    //mensaje error con return
                }
                else
                {
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "lista de usuario x instalacion";

                    return response;
                }

                //return new ListarInstalacionxusuario()
                //{

                //    codigoRes = HttpStatusCode.OK,
                //    mensajeRes = "la lista  esta con datos"
                //};

            }
        }

        public ListarUsuariosXFiltrosResponse ListarUsuariosXFiltros(string nombres, string rut, int num_pagina, int cant_registros, int idUsuario)
        {

            var datosUsuariosXFiltros = _usuarioDO.ListarUsuariosXFiltros(nombres, rut, num_pagina, cant_registros, idUsuario);
            if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
            {
                return new ListarUsuariosXFiltrosResponse()
                {
                    codigoRes = datosUsuariosXFiltros.codigoRes,
                    mensajeRes = datosUsuariosXFiltros.mensajeRes
                };
            }
            if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
            {
                return new ListarUsuariosXFiltrosResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "No se obtuvieron el detalle de los usuarios por filtros."
                };
            }
            return new ListarUsuariosXFiltrosResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos obtenidos correctamente.",
                total_registros = datosUsuariosXFiltros.total_registros,
                data = datosUsuariosXFiltros.data
            };
        }

        public ListarEppResponse listarEpp()
        {
            ListarEppResponse response = _usuarioDO.listarEpp();
            return response;
        }
        public ListarClasificacionEppResponse ListarClasificaiconEpp()
        {
            ListarClasificacionEppResponse reponse = _usuarioDO.ListarClasificaiconEpp();
            return reponse;
        }
        public ListarCategoriaEppResponse ListarCategoriaEpp()
        {
            ListarCategoriaEppResponse reponse = _usuarioDO.ListarCategoriaEpp();
            return reponse;
        }
        public ListarCargoResponse listarCargos()
        {
            ListarCargoResponse response = _usuarioDO.listarCargos();
            return response;
        }
        public ListarAreaResponse listarArea()
        {
            ListarAreaResponse response = _usuarioDO.ListarArea();
            return response;
        }
        public ListarEmpresaResponse listarEmpresa()
        {
            ListarEmpresaResponse response = _usuarioDO.ListarEmpresa();
            return response;
        }


        public ListasResponse Obternetdatalistas(int idUsuario)
        {

            ListasResponse response = new ListasResponse();

            try
            {
                ListarEppResponse responseEpp = listarEpp();

                ListaInstalacionResponse responseIns = listarInstalacion();

                ListarInstalacionxusuario response3 = listarUsuarioxInstalacion(idUsuario);

                ListarClasificacionEppResponse responsClasi = ListarClasificaiconEpp();

                ListarCategoriaEppResponse responseCatego = ListarCategoriaEpp(); ;

                ListarCargoResponse responseCargo = listarCargos();


                ListarAreaResponse responseArea = _usuarioDO.ListarArea(); ;

                ListarEmpresaResponse responseEmpresa = _usuarioDO.ListarEmpresa();

                if (responseEpp.codigoRes == HttpStatusCode.OK)
                {
                    response.listaEpp = responseEpp.listaEpp;
                }
                else
                {
                    response.listaEpp = new List<DataEpp>();
                }
                if (responseIns.codigoRes == HttpStatusCode.OK)
                {
                    response.listaInstalacion = responseIns.listaInstalacion;
                }
                else
                {
                    response.listaInstalacion = new List<DataInstalacion>();
                }
                if (response3.codigoRes == HttpStatusCode.OK)
                {
                    response.listaUsuariosPorInstalacion = response3.listaInstalaciones;
                }
                else
                {
                    response.listaUsuariosPorInstalacion = new List<DataInstalacionUsu>();
                }
                if (responsClasi.codigoRes == HttpStatusCode.OK)
                {
                    response.listaDataClasificacionEpp = responsClasi.listaDataClasificacionEpp;
                }
                else
                {
                    response.listaDataClasificacionEpp = new List<DataClasificacionEpp>();
                }
                if (responseCatego.codigoRes == HttpStatusCode.OK)
                {
                    response.listaDataCategoriaEpp = responseCatego.listaDataCategoriaEpp;
                }
                else
                {
                    response.listaDataCategoriaEpp = new List<DataCategoriaEpp>();
                }
                if (responseCargo.codigoRes == HttpStatusCode.OK)
                {
                    response.listaCargo = responseCargo.listaCargo;
                }
                else
                {
                    responseCargo.listaCargo = new List<DataCargo>();
                }
                if (responseArea.codigoRes == HttpStatusCode.OK)
                {
                    response.listaArea = responseArea.listaArea;
                }
                else
                {
                    responseArea.listaArea = new List<DataArea>();
                }
                if (responseEmpresa.codigoRes == HttpStatusCode.OK)
                {
                    response.listaEmpresa = responseEmpresa.listaEmpresa;
                }
                else
                {
                    responseEmpresa.listaEmpresa = new List<DataEmpresa>();
                }


                response.codigoRes = HttpStatusCode.OK;
                response.mensajeRes = "Se obtuvieron las listas correctamente";
            }
            catch (Exception)
            {
                response.codigoRes = HttpStatusCode.BadRequest;
                response.mensajeRes = "Error interno al obtener el detalle de las listas";
            }

            return response;

        }
        public ListarDatosUsuariosResponse ListarDatosUsuario(int idUsuario)
        {

            ListarDatosUsuariosResponse reponse = _usuarioDO.ListarDatosUsuario(idUsuario);
            return reponse;

        }

        public ListarTipodecolaboradoresxUsuario ListarUsuariosxTipodecolaborador()
        {
            ListarTipodecolaboradoresxUsuario reponse = _usuarioDO.ListarUsuariosxTipodecolaborador();
            return reponse;
        }

        public ObtenerDetalleUsuarioResponse ObtenerDetalleUsuario(int id_persona, string cod_aplicacion)
        {
            ObtenerDetalleUsuarioResponse reponse = _usuarioDO.ObtenerDetalleUsuario(id_persona, cod_aplicacion);
            if (reponse.data.genero == "Masculino")
            {
                reponse.data.genero = "M";

            }
            else if (reponse.data.genero == "Femenino")
            {
                reponse.data.genero = "F";
            }
            else
            {
                reponse.data.genero = null;
            }

            return reponse;

        }
        public SeguimientoEppResponse ListadoDetalleActaEntrega(int id_entrega, int idUsuario)
        {


            var response = new SeguimientoEppResponse();
            try
            {

                var ListadoDetalle = _usuarioDO.ListadoDetalleActaEntrega(id_entrega, idUsuario);
                var listaColaboradoresActaEntregra = _usuarioDO.ListarColaboradresActaEntrega(id_entrega).Select(x => new Colaboradores()
                {
                    nombres = x.nombres,
                    apellido_materno = x.apellido_materno,
                    apellido_paterno = x.apellido_paterno,
                    rut = x.rut,
                    id_colaborador_entrega = x.id_colaborador_entrega,
                    documento = x.documento,
                    estado_colaborador = x.estado_colaborador

                }).ToList();
                var listaEppColaborador = _usuarioDO.ListarEPPActaEntrega(id_entrega, idUsuario).Select(x => new Epp()
                {
                    descripcion = x.descripcion,
                    cantidad = x.cantidad,
                    clasificacion = x.clasificacion,

                }).ToList();

                if (ListadoDetalle != null)
                {
                    if (ListadoDetalle.vista_creador == 1)
                    {


                        CabeceraRes cabecera = new CabeceraRes();

                        cabecera.instalacion = ListadoDetalle.nombre_instalacion;
                        cabecera.codigo = ListadoDetalle.cod_entrega;
                        cabecera.fecha = ListadoDetalle.fecha_entrega.ToString();
                        cabecera.responsable = ListadoDetalle.Responsable;
                        cabecera.estado = ListadoDetalle.nombre_estado1;

                        List<Colaboradores> listacolaboradores = new List<Colaboradores>();

                        listacolaboradores = listaColaboradoresActaEntregra;


                        Responsable responsable = new Responsable();


                        responsable.cabecera = cabecera;
                        response.datosResponsable = responsable;
                        response.datosResponsable.listadoColaboradores = listacolaboradores;


                    }

                    else
                    {


                        //colaborador

                        Cabecera cabeceraCol = new Cabecera();
                        cabeceraCol.nombre = ListadoDetalle.colaborador;
                        cabeceraCol.rut = ListadoDetalle.rut;
                        cabeceraCol.empresa = ListadoDetalle.nombre_empresa;
                        cabeceraCol.estado = ListadoDetalle.nombre_estado;
                        cabeceraCol.id_colaboradorEntrega = ListadoDetalle.id_colaboradorEntrega;
                        cabeceraCol.responsable = ListadoDetalle.Responsable;
                        cabeceraCol.flag_boton_aprobado = ListadoDetalle.flag_boton_aprobado;
                        cabeceraCol.documento = ListadoDetalle.documento;
                        Colaborador colaborador = new Colaborador();

                        colaborador.cabecera = cabeceraCol;
                        response.datosColaborador = colaborador;
                        response.datosColaborador.listadoEpp = listaEppColaborador;


                    }


                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Se obtuvieron los detalles del acta de entrega correctamente.";


                }

                else
                {
                    return new SeguimientoEppResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "No se obtuvieron los detalles del Acta de entrega. "

                    };

                }

            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener el detalle del usuario";


            }

            return response;
        }



        public CambiarContraseñaResponse ObtenerRespuesta(int idUsuario, int id_aplicacion, string guid)
        {
            var response = new CambiarContraseñaResponse();
            try
            {

                var respuesta = _usuarioDO.ObtenerRespuesta(idUsuario, id_aplicacion, guid);

                response.codigoRes = respuesta.codigoRes;
                response.mensajeRes = respuesta.mensajeRes;
               



            }
            catch (Exception e)
            {

                log.Error($"UsuarioDO ({guid})->   ObtenerRespuesta. Mensaje al cliente: Error interno al obtener obtener respuesta. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = " Error interno al obtener obtener respuesta";



            }


            return response;

        }

        public CreacionProcesoCargaUsuariosResponse CrearProcesoCargaUsuarios(string UUIDusuario, HttpContent archivo, string nameFileExcel)
        {
            try
            {
                //string storageConnection = CloudConfigurationManager.GetSetting("StorageConnection");
                //CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnection);

                ////create a block blob 
                //CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

                ////create a container 
                //var namecontainer = CloudConfigurationManager.GetSetting("containerAzure");
                //CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(namecontainer);//namecontainer

                //if (cloudBlobContainer.CreateIfNotExists())// privado
                //{
                //    cloudBlobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Off });
                //}

                string nombreArchivoApi = $"{Guid.NewGuid()}_{nameFileExcel}";

                //CloudBlockBlob cloudBlockBlob1 = cloudBlobContainer.GetBlockBlobReference(nombreArchivoApi);
                //cloudBlockBlob1.Properties.ContentType = archivo.Headers.ContentType.ToString();
                //cloudBlockBlob1.UploadFromStream(archivo.ReadAsStreamAsync().Result);

                var urldocumento = ""; //cloudBlockBlob1.Uri.AbsoluteUri;


                var creacionProcesoCarga = _usuarioDO.CrearProcesoCargaUsuarios(UUIDusuario, urldocumento, nameFileExcel, nombreArchivoApi);
                return creacionProcesoCarga;
            }
            catch (Exception ex)
            {
                return new CreacionProcesoCargaUsuariosResponse() { codigo = -1, descripcion = "Error en carga de documentos" };
            }

        }

        public async Task<CrearProcesoCargaUsuariosResponse> CrearProcesoCargaUsuarios_Background(string UUIDusuario, List<RegistroProcesoCargaUsuariosRequest> listadoDatos,
            int idProcesoCarga, string codProcesoCarga)
        {
            try
            {
                log.Warn($"UsuarioBO ({idProcesoCarga})->  CrearProcesoCargaUsuarios_Background. Inicio carga de usuario en background.");
                var response = new CrearProcesoCargaUsuariosResponse() { };

                _usuarioDO.CambiarEstadoProcesoCargaUsuariosAEnProceso(UUIDusuario, idProcesoCarga, listadoDatos.Count);


                CrearProcesoCargaUsuarios_Background_RegistrarDatosCarga(listadoDatos, UUIDusuario, idProcesoCarga);
                var resTraspaso = _usuarioDO.TraspasoDatosProcesoCargaUsuarios(UUIDusuario, idProcesoCarga);
                log.Warn($"UsuarioBO ({idProcesoCarga})->  CrearProcesoCargaUsuarios_Background. Fin registro datos a finales.");
                if (resTraspaso.codigo != 1)
                {
                    response.codigo = resTraspaso.codigo;
                    response.descripcion = resTraspaso.descripcion;
                    response.idProcesoCargaUsuarios = idProcesoCarga;
                    response.codProcesoCargaUsuarios = codProcesoCarga;
                    return response;
                }


                response.codigo = resTraspaso.codigo;
                response.descripcion = resTraspaso.descripcion;
                response.idProcesoCargaUsuarios = idProcesoCarga;
                response.codProcesoCargaUsuarios = codProcesoCarga;
                return response;
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioBO ({idProcesoCarga})->  CrearProcesoCargaUsuarios_Background. Error interno, al procesar los datos para la carga de comprobantes." +
                    " Error detalle: " + JsonConvert.SerializeObject(ex));
                return new CrearProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno, al procesar los datos para la carga de usuarios.",
                    idProcesoCargaUsuarios = idProcesoCarga,
                    codProcesoCargaUsuarios = codProcesoCarga
                };
            }
        }

        public async Task<List<RegistroProcesoCargaUsuariosRequest>> CrearProcesoCargaUsuarios_Background_LeerArchivoGetData(HttpContent archivo)
        {
            List<RegistroProcesoCargaUsuariosRequest> listRequests = new List<RegistroProcesoCargaUsuariosRequest>();

            try
            {
                using (var stream = new MemoryStream())
                {
                    await archivo.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        ExcelWorksheet worksheet = package.Workbook.Worksheets["Usuarios"];
                        var rowCount = worksheet.Dimension.Rows;

                        for (int row = 2; row <= rowCount; row++)
                        {
                            CrearProcesoCargaUsuarios_Background_LeerArchivoGetData_GetRow(listRequests, row, worksheet); //validar tema de string
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return listRequests;
        }

        private void CrearProcesoCargaUsuarios_Background_LeerArchivoGetData_GetRow(List<RegistroProcesoCargaUsuariosRequest> listRequests, int row, ExcelWorksheet worksheet)
        {
            RegistroProcesoCargaUsuariosRequest request = new RegistroProcesoCargaUsuariosRequest();
            request.numfila = row;

            var rut = worksheet.Cells[row, 1].Value;
            request.rut = rut != null ? rut.ToString().Trim() : null;

            var apellido_paterno = worksheet.Cells[row, 2].Value;
            request.apellido_paterno = apellido_paterno != null ? apellido_paterno.ToString().Trim() : null;

            var apellido_materno = worksheet.Cells[row, 3].Value;
            request.apellido_materno = apellido_materno != null ? apellido_materno.ToString().Trim() : null;

            var nombre = worksheet.Cells[row, 4].Value;
            request.nombre = nombre != null ? nombre.ToString().Trim() : null;

            //var nombre2 = worksheet.Cells[row, 5].Value;
            // nombre2 != null ? nombre2.ToString().Trim() : null;

            var cargo = worksheet.Cells[row, 5].Value;
            request.cargo = cargo != null ? cargo.ToString().Trim() : null;

            var rol_usuario = worksheet.Cells[row, 6].Value;
            request.rol_usuario = rol_usuario != null ? rol_usuario.ToString().Trim() : null;

            var tipo_colaborador = worksheet.Cells[row, 7].Value;
            request.tipo_colaborador = tipo_colaborador != null ? tipo_colaborador.ToString().Trim() : null;

            var instalacion = worksheet.Cells[row, 8].Value;
            request.instalacion = instalacion != null ? instalacion.ToString().Trim() : null;

            var correo = worksheet.Cells[row, 9].Value;
            request.correo = correo != null ? correo.ToString().Trim() : null;

            var celular = worksheet.Cells[row, 10].Value;
            request.celular = celular != null ? celular.ToString().Trim() : null;

            var nombre_empresa = worksheet.Cells[row, 11].Value;
            request.nombre_empresa = nombre_empresa != null ? nombre_empresa.ToString().Trim() : null;

            var area = worksheet.Cells[row, 12].Value;
            request.area = area != null ? area.ToString().Trim() : null;

            var fecha_nacimiento = worksheet.Cells[row, 13].Value;
            request.fecha_nacimiento = fecha_nacimiento != null ? fecha_nacimiento.ToString().Trim() : null;

            var genero = worksheet.Cells[row, 14].Value;
            request.genero = genero != null ? genero.ToString().Trim() : null;

            listRequests.Add(request);
        }

        private void CrearProcesoCargaUsuarios_Background_RegistrarDatosCarga(List<RegistroProcesoCargaUsuariosRequest> listRequests, string UUIDusuario,
           int idProcesoCarga)
        {
            log.Warn($"UsuarioBO ({idProcesoCarga})->  CrearProcesoCargaUsuarios_Background_RegistrarDatosCarga. Registro datos temporales.");
            DataTable dtCargaUsuarios = createDataTableCargaUsuarios();
            fillDataTableCargaER(dtCargaUsuarios, listRequests);
            _usuarioDO.RegistrarDatosUsuarios(dtCargaUsuarios, idProcesoCarga, UUIDusuario);
        }

        private DataTable createDataTableCargaUsuarios()
        {
            DataTable dtCargaUsuarios = new DataTable() { Locale = CultureInfo.InvariantCulture };
            dtCargaUsuarios.Columns.Add("numfila", typeof(int));
            dtCargaUsuarios.Columns.Add("rut", typeof(string));
            dtCargaUsuarios.Columns.Add("apellido_paterno", typeof(string));
            dtCargaUsuarios.Columns.Add("apellido_materno", typeof(string));
            dtCargaUsuarios.Columns.Add("nombre", typeof(string));
            dtCargaUsuarios.Columns.Add("cargo", typeof(string));
            dtCargaUsuarios.Columns.Add("rol_usuario", typeof(string));
            dtCargaUsuarios.Columns.Add("tipo_colaborador", typeof(string));
            dtCargaUsuarios.Columns.Add("instalacion", typeof(string));
            dtCargaUsuarios.Columns.Add("correo", typeof(string));
            dtCargaUsuarios.Columns.Add("celular", typeof(string));
            dtCargaUsuarios.Columns.Add("nombre_empresa", typeof(string));
            dtCargaUsuarios.Columns.Add("area", typeof(string));

            dtCargaUsuarios.Columns.Add("fecha_nacimiento", typeof(string));
            dtCargaUsuarios.Columns.Add("genero", typeof(string));

            dtCargaUsuarios.Columns.Add("numRegistrosDataUsuarios", typeof(int));


            return dtCargaUsuarios;
        }

        private void fillDataTableCargaER(DataTable dtCargaUsuarios, List<RegistroProcesoCargaUsuariosRequest> listRequests)
        {
            int index = 0;
            foreach (var itemCargaUsuarios in listRequests)
            {
                dtCargaUsuarios.Rows.Add();
                dtCargaUsuarios.Rows[index]["numfila"] = itemCargaUsuarios.numfila;
                dtCargaUsuarios.Rows[index]["rut"] = String.IsNullOrEmpty(itemCargaUsuarios.rut) ? "" : itemCargaUsuarios.rut.Substring(0, (itemCargaUsuarios.rut.Length > 50 ? 50 : itemCargaUsuarios.rut.Length));
                dtCargaUsuarios.Rows[index]["apellido_paterno"] = String.IsNullOrEmpty(itemCargaUsuarios.apellido_paterno) ? "" : itemCargaUsuarios.apellido_paterno.Substring(0, (itemCargaUsuarios.apellido_paterno.Length > 50 ? 50 : itemCargaUsuarios.apellido_paterno.Length));
                dtCargaUsuarios.Rows[index]["apellido_materno"] = String.IsNullOrEmpty(itemCargaUsuarios.apellido_materno) ? "" : itemCargaUsuarios.apellido_materno.Substring(0, (itemCargaUsuarios.apellido_materno.Length > 50 ? 50 : itemCargaUsuarios.apellido_materno.Length));
                dtCargaUsuarios.Rows[index]["nombre"] = String.IsNullOrEmpty(itemCargaUsuarios.nombre) ? "" : itemCargaUsuarios.nombre.Substring(0, (itemCargaUsuarios.nombre.Length > 50 ? 50 : itemCargaUsuarios.nombre.Length));
                dtCargaUsuarios.Rows[index]["cargo"] = String.IsNullOrEmpty(itemCargaUsuarios.cargo) ? "" : itemCargaUsuarios.cargo.Substring(0, (itemCargaUsuarios.cargo.Length > 300 ? 300 : itemCargaUsuarios.cargo.Length));
                dtCargaUsuarios.Rows[index]["rol_usuario"] = String.IsNullOrEmpty(itemCargaUsuarios.rol_usuario) ? "" : itemCargaUsuarios.rol_usuario.Substring(0, (itemCargaUsuarios.rol_usuario.Length > 20 ? 20 : itemCargaUsuarios.rol_usuario.Length));
                dtCargaUsuarios.Rows[index]["tipo_colaborador"] = String.IsNullOrEmpty(itemCargaUsuarios.tipo_colaborador) ? "" : itemCargaUsuarios.tipo_colaborador.Substring(0, (itemCargaUsuarios.tipo_colaborador.Length > 20 ? 20 : itemCargaUsuarios.tipo_colaborador.Length));
                dtCargaUsuarios.Rows[index]["instalacion"] = String.IsNullOrEmpty(itemCargaUsuarios.instalacion) ? "" : itemCargaUsuarios.instalacion.Substring(0, (itemCargaUsuarios.instalacion.Length > 300 ? 300 : itemCargaUsuarios.instalacion.Length));
                dtCargaUsuarios.Rows[index]["correo"] = String.IsNullOrEmpty(itemCargaUsuarios.correo) ? "" : itemCargaUsuarios.correo.Substring(0, (itemCargaUsuarios.correo.Length > 100 ? 100 : itemCargaUsuarios.correo.Length));
                dtCargaUsuarios.Rows[index]["celular"] = String.IsNullOrEmpty(itemCargaUsuarios.celular) ? "" : itemCargaUsuarios.celular.Substring(0, (itemCargaUsuarios.celular.Length > 50 ? 50 : itemCargaUsuarios.celular.Length));
                dtCargaUsuarios.Rows[index]["nombre_empresa"] = String.IsNullOrEmpty(itemCargaUsuarios.nombre_empresa) ? "" : itemCargaUsuarios.nombre_empresa.Substring(0, (itemCargaUsuarios.nombre_empresa.Length > 200 ? 200 : itemCargaUsuarios.nombre_empresa.Length));
                dtCargaUsuarios.Rows[index]["area"] = String.IsNullOrEmpty(itemCargaUsuarios.area) ? "" : itemCargaUsuarios.area.Substring(0, (itemCargaUsuarios.area.Length > 50 ? 50 : itemCargaUsuarios.area.Length));

                dtCargaUsuarios.Rows[index]["fecha_nacimiento"] = String.IsNullOrEmpty(itemCargaUsuarios.fecha_nacimiento) ? "" : itemCargaUsuarios.fecha_nacimiento.Substring(0, (itemCargaUsuarios.fecha_nacimiento.Length > 20 ? 20 : itemCargaUsuarios.fecha_nacimiento.Length));
                dtCargaUsuarios.Rows[index]["genero"] = String.IsNullOrEmpty(itemCargaUsuarios.genero) ? "" : itemCargaUsuarios.genero.Substring(0, (itemCargaUsuarios.genero.Length > 20 ? 20 : itemCargaUsuarios.genero.Length));

                dtCargaUsuarios.Rows[index]["numRegistrosDataUsuarios"] = index + 1;

                index++;
            }
        }

        public CreacionProcesoCargaUsuariosResponse ActualizarProcesoCargaUsuariosFinalizado(int idProcesoCargaUsuarios, string UUIDusuario)
        {
            return _usuarioDO.ActualizarProcesoCargaUsuariosFinalizado(idProcesoCargaUsuarios, UUIDusuario);
        }

        public ListasMaestroWebResponse ListarTablasMaestrasWEB()
        {
            ListasMaestroWebResponse response = new ListasMaestroWebResponse();

            try
            {
                ListasMaestroWebResponse reponseclasificacion = _usuarioDO.ListarClasificaiconEppWEB();

                ListasMaestroWebResponse responseCategoria = _usuarioDO.ListarCategoriaEppWEB();
                ListasMaestroWebResponse responseEstadosActa = _usuarioDO.ListarEstadoEntregaWEB();
                ListasMaestroWebResponse responseTipoColaboradores = _usuarioDO.ListarTipoColaboradorWEB();
                ListasMaestroWebResponse reponseinstalaciones = _usuarioDO.ListarInstalacionWEB();
                ListasMaestroWebResponse responseEmpresa = _usuarioDO.ListarEmpresaWEB();
                ListasMaestroWebResponse responseRolesUsuario = _usuarioDO.ListarRolUsuarioWEB();
                ListasMaestroWebResponse responseArea = _usuarioDO.ListarAreaWEB();
                ListasMaestroWebResponse responseCargo = _usuarioDO.ListarCargoWEB();

                if (reponseclasificacion.codigoRes == HttpStatusCode.OK && responseCategoria.codigoRes == HttpStatusCode.OK)
                {
                    response.listaDataClasificacionEppWeb = reponseclasificacion.listaDataClasificacionEppWeb;
                    response.listaDataCategoriaEppWeb = responseCategoria.listaDataCategoriaEppWeb;
                    response.listaDataEstadoEntregaWeb = responseEstadosActa.listaDataEstadoEntregaWeb;
                    response.listaDataTipoColaboradorWeb = responseTipoColaboradores.listaDataTipoColaboradorWeb;
                    response.listaDataInstalacionesWeb = reponseinstalaciones.listaDataInstalacionesWeb;
                    response.listaDataEmpresaWeb = responseEmpresa.listaDataEmpresaWeb;
                    response.listaDataRolUsuarioWeb = responseRolesUsuario.listaDataRolUsuarioWeb;
                    response.listaDataAreasWeb = responseArea.listaDataAreasWeb;
                    response.listaDataCargoWeb = responseCargo.listaDataCargoWeb;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "los datos obtenios de las tablas maestras";

                }
                else
                {
                    response.listaDataClasificacionEppWeb = new List<DataClasificacionEppWeb>();
                    response.listaDataCategoriaEppWeb = new List<DataCategoriaEppWeb>();
                    response.listaDataEstadoEntregaWeb = new List<DataEstadoEntregaWeb>();
                    response.listaDataTipoColaboradorWeb = new List<DataTipoColaboradorWeb>();
                }




            }
            catch (Exception)
            {
                response.codigoRes = HttpStatusCode.BadRequest;
                response.mensajeRes = "Error interno al obtener el detalle de las listas";
            }

            return response;

        }

        public ProcesoCargalistadoResponse listarProcesoCargo(int num_pagina, int cant_registros, string criterioBusqueda, int idUsuario)
        {

            try
            {

                var datoscarga = _usuarioDO.listarProcesoCargo(num_pagina, cant_registros, criterioBusqueda, idUsuario);
                if (datoscarga.codigoRes != HttpStatusCode.OK)
                {
                    return new ProcesoCargalistadoResponse()
                    {
                        codigoRes = datoscarga.codigoRes,
                        mensajeRes = datoscarga.mensajeRes
                    };
                }
                if (datoscarga.data == null || datoscarga.data.Count <= 0)
                {
                    return new ProcesoCargalistadoResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron el detalle de las cargas."
                    };
                }
                return new ProcesoCargalistadoResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    total_registros = datoscarga.total_registros,
                    data = datoscarga.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"UsuarioBO ()-> Usuario: ({idUsuario}) ->listarProcesoCargo. Mensaje al cliente: Error interno en el servicio listar los procesos" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ProcesoCargalistadoResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar los procesos"
                };

            }



        }
        public DetalleCargaUsuarioResponse DetalleProcesoCarga(int id_proceso_Carga, int id_usuario)
        {

            try
            {
                if (id_proceso_Carga <= 0)
                {
                    return new DetalleCargaUsuarioResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Ingrese los datos obligatorios"


                    };
                }

                return _usuarioDO.DetalleProcesoCarga(id_proceso_Carga, id_usuario);

            }
            catch (Exception ex)
            {


                log.Error($"UsuarioBO ()-> Usuario: ({id_usuario}) ->DetalleProcesoCarga. Mensaje al cliente: Error interno en el servicio listar los procesos" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new DetalleCargaUsuarioResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar los procesos"
                };

            }



        }

        public ListaTiposErroresCargaResponse ObtenerListaTipoError()
        {
            ListaTiposErroresCargaResponse response = new ListaTiposErroresCargaResponse();
            try
            {

                ListaTiposErroresCargaResponse responseTipoError = _usuarioDO.ObtenerListaTipoError();
                if (responseTipoError.codigoRes == HttpStatusCode.OK)
                {

                    response.ListaErroesCarga = responseTipoError.ListaErroesCarga;
                    response.codigoRes = responseTipoError.codigoRes;
                    response.mensajeRes = responseTipoError.mensajeRes;

                }
                else
                {
                    response.ListaErroesCarga = new List<DataErrores>();

                }


            }
            catch (Exception ex)
            {


                log.Error($"UsuarioBO () ->ObtenerListaTipoError. Mensaje al cliente: Error interno en el servicio listar los procesos" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ListaTiposErroresCargaResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar los procesos"
                };

            }

            return response;

        }

        public ListarProcesoCargaErroresResponse listarProcesoCargoErrores(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int num_pagina, int cant_registro, int id_usuario)
        {

            try
            {

                var datoscarga = _usuarioDO.listarProcesoCargoErrores(idProcesoCarga, idsCargaErrorEstados, criterioBusqueda, num_pagina, cant_registro, id_usuario);
                if (datoscarga.codigoRes != HttpStatusCode.OK)
                {
                    return new ListarProcesoCargaErroresResponse()
                    {
                        codigoRes = datoscarga.codigoRes,
                        mensajeRes = datoscarga.mensajeRes
                    };
                }
                if (datoscarga.data == null || datoscarga.data.Count <= 0)
                {
                    return new ListarProcesoCargaErroresResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron las listas de las cargas  de los errores."
                    };
                }
                return new ListarProcesoCargaErroresResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    total_registros = datoscarga.total_registros,
                    data = datoscarga.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"UsuarioBO ()-> Usuario: ({id_usuario}) ->listarProcesoCargo. Mensaje al cliente: Error interno en el servicio listar de las cargas  de los errores" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ListarProcesoCargaErroresResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar de las cargas  de los errores"
                };

            }



        }

        public ListarProcesoCargaErroresResponseExcel listarProcesoCargoErroresExcel(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int id_usuario)
        {

            try
            {

                var datoscarga = _usuarioDO.listarProcesoCargoErroresExcel(idProcesoCarga, idsCargaErrorEstados, criterioBusqueda, id_usuario);

                if (datoscarga.codigoRes != HttpStatusCode.OK)
                {
                    return new ListarProcesoCargaErroresResponseExcel()
                    {
                        codigoRes = datoscarga.codigoRes,
                        mensajeRes = datoscarga.mensajeRes
                    };
                }
                if (datoscarga.data == null || datoscarga.data.Count <= 0)
                {
                    return new ListarProcesoCargaErroresResponseExcel()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvieron las listas de las cargas  de los errores."
                    };
                }
                return new ListarProcesoCargaErroresResponseExcel()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Datos obtenidos correctamente.",
                    data = datoscarga.data
                };

            }
            catch (Exception ex)
            {


                log.Error($"UsuarioBO ()-> Usuario: ({id_usuario}) ->listarProcesoCargo. Mensaje al cliente: Error interno en el servicio listar de las cargas  de los errores" +
                 "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ListarProcesoCargaErroresResponseExcel()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta  de servicio listar de las cargas  de los errores"
                };

            }



        }

    }
}

