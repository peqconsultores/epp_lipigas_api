﻿using EPPLipigas.Business.Contrato;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.Instalacion.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace EPPLipigas.Business.Implementacion
{
    public class InstalacionBO : IInstalacionBO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(IInstalacionBO));
        private readonly IInstalacionDO _instalacionDO;
        public InstalacionBO(IInstalacionDO instalacionDO)
        {
            _instalacionDO = instalacionDO;
        }
        public ListarInstalacionXFiltroResponse ListarInstalacionXFiltros(string instalacion, string cod_tipo_instalacion, string estado, int num_pagina, int cant_registros, int idUsuario)
        {
            var datosUsuariosXFiltros = _instalacionDO.ListarInstalacionXFiltros(instalacion, cod_tipo_instalacion, estado, num_pagina, cant_registros, idUsuario);
            if (datosUsuariosXFiltros.codigoRes != HttpStatusCode.OK)
            {
                return new ListarInstalacionXFiltroResponse()
                {
                    codigoRes = datosUsuariosXFiltros.codigoRes,
                    mensajeRes = datosUsuariosXFiltros.mensajeRes
                };
            }
            if (datosUsuariosXFiltros.data == null || datosUsuariosXFiltros.data.Count <= 0)
            {
                return new ListarInstalacionXFiltroResponse()
                {
                    codigoRes = HttpStatusCode.BadRequest,
                    mensajeRes = "No se obtuvieron el detalle de los usuarios por filtros."
                };
            }
            return new ListarInstalacionXFiltroResponse()
            {
                codigoRes = HttpStatusCode.OK,
                mensajeRes = "Datos obtenidos correctamente.",
                total_registros = datosUsuariosXFiltros.total_registros,
                data = datosUsuariosXFiltros.data
            };
        }
        public CrearInstalacionResponse CrearInstalacion(int aplicacion, int usuario, string nombre_instalacion, string cod_tipo_instalacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new CrearInstalacionResponse();

            try
            {
                var respuesta = _instalacionDO.CrearInstalacion(aplicacion, usuario, nombre_instalacion, cod_tipo_instalacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {

                    return new CrearInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
                else
                {

                    return new CrearInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionBO ({guid})-> Usuario: ({usuario}) ->CrearInstalacion. Mensaje al cliente: Error interno en el servicio al crear instalacion" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new CrearInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  crear instalacion "
                };
            }
        }
        public EliminarInstalacionResponse EliminarInstalacion(int aplicacion, int usuario, int id_instalacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new EliminarInstalacionResponse();

            try
            {
                var respuesta = _instalacionDO.EliminarInstalacion(aplicacion, usuario, id_instalacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new EliminarInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
                else
                {
                    return new EliminarInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionBO ({guid})-> Usuario: ({usuario}) ->EliminarInstalacion. Mensaje al cliente: Error interno en el servicio al eliminar la instalacion" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new EliminarInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  crear instalacion "
                };
            }
        }
        public EditarInstalacionResponse EditarInstalacion(int aplicacion, int usuario, int id_instalacion, string cod_tipo_instalacion, string nombre_instalacion, bool activo)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new EditarInstalacionResponse();

            try
            {
                var respuesta = _instalacionDO.EditarInstalacion(aplicacion, usuario, id_instalacion, cod_tipo_instalacion, nombre_instalacion, activo);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new EditarInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,
                    };
                }
                else
                {
                    return new EditarInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionBO ({guid})-> Usuario: ({usuario}) ->EditarInstalacion. Mensaje al cliente: Error interno en el servicio al editar la instalacion" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new EditarInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al editar instalacion "
                };
            }
        }
        public DetalleInstalacionResponse DetalleInstalacion(int id_instalacion)
        {
            string guid = Guid.NewGuid().ToString();
            var response = new DetalleInstalacionResponse();

            try
            {
                var respuesta = _instalacionDO.DetalleInstalacion(id_instalacion);
                if (respuesta.codigoRes == HttpStatusCode.OK)
                {
                    return new DetalleInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes,
                        datos = respuesta.datos
                    };
                }
                else
                {
                    return new DetalleInstalacionResponse()
                    {
                        codigoRes = respuesta.codigoRes,
                        mensajeRes = respuesta.mensajeRes
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionBO ({guid})-> ->EditarInstalacion. Mensaje al cliente: Error interno en el servicio al editar la instalacion" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new DetalleInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al obtener el detalle de la  instalacion "
                };
            }
        }
        public ListarParametrosInstalacionResponse ListarParametrosInstalacion(int idUsuario, string cod_aplicacion)
        {
            string guid = Guid.NewGuid().ToString();

            try
            {
                var respuesta = new ListarParametrosInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Parametros de instalación obtenidos correctamente",
                    datos = new ListadoParametrosInstalacionData()
                };
                ListadoParametrosInstalacionFillData(respuesta);
                return respuesta;
            }
            catch (Exception ex)
            {
                log.Error($"InstalacionBO ({guid})-> Usuario: ({idUsuario}) ->CrearInstalacion. Mensaje al cliente: Error interno en el servicio de listar parametros de instalación" +
                "Detalle error: " + JsonConvert.SerializeObject(ex));

                return new ListarParametrosInstalacionResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = " Error interno en el servicio de listar parametros de instalación"
                };
            }
        }
        public void ListadoParametrosInstalacionFillData(ListarParametrosInstalacionResponse respuesta)
        {
            var dataTipoInstalacion = _instalacionDO.ObtenerTipoInstalacion();
            if (dataTipoInstalacion != null && dataTipoInstalacion.codigoRes == HttpStatusCode.OK)
            {
                respuesta.datos.listaTipoInstalacion = new List<DatosTipoInstalacion>();
                respuesta.datos.listaTipoInstalacion = dataTipoInstalacion.datos;
            }
            else
            {
                respuesta.mensajeRes = respuesta.mensajeRes;
            }
        }
    }
}
