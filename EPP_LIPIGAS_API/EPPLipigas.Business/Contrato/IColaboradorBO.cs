﻿using EPPLipigas.Entities.Actas;
using EPPLipigas.Entities.Colaborador.Request;
using EPPLipigas.Entities.Colaborador.Response;

namespace EPPLipigas.Business.Contrato
{
    public interface IColaboradorBO
    {
        BuscarColaboradorResponse BuscarColaborador(string rut);
        RegistrarColaboradorResponse RegistrarColaborador(RegistrarColaboradorRequest request, int id_usuario, int id_aplicacion);
        DetalleEntregaResponse ObteneDetallaeEntregaPorColaborador(int id_colaborador_entrega, int idusuario);
        EntregaEppColaboradorResponse ObtenerRespuestaEstadoColaboradorEntrega(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid);
        DatosEntregaResponseporguid ObtenerDatosEntregaxGuid(string guid_entregado);
        AprobarxWebResponse ObtenerRespuestaAprobarxWeb(string guid_entregado, string guid_aprobacion_transaccion);
        RegistrarColaboradorAppResponse RegistrarColaboradorAPP(RegistrarColaboradorAppRequest request, int id_usuario, int id_aplicacion);
        ReporteColaboradoresResponse Listarcolaboradoresxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario);
        ReporteColaboradorExcelResponse ListarcolaboradoresExcelxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int idUsuario);
        DatosEntregaPDF_EppsResponse ObtenerRespuestaEstadoColaboradorEntregaDatosPDF(int id_Colaborador_entrega, int id_usuario, string guid);
        ActualizarColaboradorResponse ActualizarColaborador(int id_colaborador_entrega, string genero, string correo, string fecha_nacimiento, int id_aplicacion, int id_usuario, string guid);
        ObtenerDocumentoDescargarResponse ObtenerDocumentodescargar(string id_documento, string guid);
    }
}
