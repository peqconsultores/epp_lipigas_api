﻿using EPPLipigas.Entities;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.EDigital.Response;
using EPPLipigas.Entities.Epp;
using EPPLipigas.Entities.Instalacion;
using EPPLipigas.Entities.Rut;
using EPPLipigas.Entities.Usuario.Request;
using EPPLipigas.Entities.Usuario.Response;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace EPPLipigas.Business.Contrato
{
    public interface IUsuarioBO
    {
        BusquedaRutResponse BusquedaRut(string rut);
        GetTokenResponse gettoken();
        CreacionUsuarioResponse CreacionUsuario(CreacionUsuarioRequest req);
        //RegistrarUsuarioResponse crearUsuario(int id_persona);
        EditarUsuarioResponse EditarUsuario(EditarUsuarioRequest request, int _id_persona, int id_usuario_mod, int id_aplicacion_mod);
        ListarUsuariosXFiltrosResponse ListarUsuariosXFiltros(string nombres, string rut, int num_pagina, int cant_registros, int idUsuario);
        EliminarUsuarioResponse EliminarUsuario(int id_aplicacion_elim, int id_usuario_elim, int id_persona);
        ListaInstalacionResponse listarInstalacion();
        ListarInstalacionxusuario listarUsuarioxInstalacion(int idUsuario);
        ListarEppResponse listarEpp();
        ListasResponse Obternetdatalistas(int idUsuario);
        ListarClasificacionEppResponse ListarClasificaiconEpp();
        ListarCategoriaEppResponse ListarCategoriaEpp();
        ListarDatosUsuariosResponse ListarDatosUsuario(int idUsuario);
        ListarTipodecolaboradoresxUsuario ListarUsuariosxTipodecolaborador();
        ObtenerDetalleUsuarioResponse ObtenerDetalleUsuario(int id_persona, string cod_aplicacion);

        SeguimientoEppResponse ListadoDetalleActaEntrega(int id_entrega, int idUsuario);

        CambiarContraseñaResponse ObtenerRespuesta(int idUsuario, int id_aplicacion, string guid);
        CreacionProcesoCargaUsuariosResponse CrearProcesoCargaUsuarios(string UUIDusuario, HttpContent archivo, string nameFileExcel);
        Task<List<RegistroProcesoCargaUsuariosRequest>> CrearProcesoCargaUsuarios_Background_LeerArchivoGetData(HttpContent archivo);
        Task<CrearProcesoCargaUsuariosResponse> CrearProcesoCargaUsuarios_Background(string UUIDusuario, List<RegistroProcesoCargaUsuariosRequest> listadoDatos,
            int idProcesoCarga, string codProcesoCarga);
        CreacionProcesoCargaUsuariosResponse ActualizarProcesoCargaUsuariosFinalizado(int idProcesoCargaUsuarios, string UUIDusuario);
        ListasMaestroWebResponse ListarTablasMaestrasWEB();
        ProcesoCargalistadoResponse listarProcesoCargo(int num_pagina, int cant_registros, string criterioBusqueda, int idUsuario);

        DetalleCargaUsuarioResponse DetalleProcesoCarga(int id_proceso_Carga, int id_usuario);

        ListaTiposErroresCargaResponse ObtenerListaTipoError();
        ListarProcesoCargaErroresResponse listarProcesoCargoErrores(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int num_pagina, int cant_registro, int id_usuario);
        ListarProcesoCargaErroresResponseExcel listarProcesoCargoErroresExcel(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int id_usuario);
    }
}
