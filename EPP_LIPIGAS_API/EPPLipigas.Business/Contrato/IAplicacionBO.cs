﻿using EPPLipigas.Entities.Aplicacion;

namespace EPPLipigas.Business.Contrato
{
    public interface IAplicacionBO
    {
        ListarParametrosResponse ListarParametros(string clientname, string clientsecret, string guid);
    }
}
