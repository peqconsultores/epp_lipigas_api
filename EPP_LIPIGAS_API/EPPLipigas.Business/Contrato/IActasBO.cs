﻿using EPPLipigas.Entities.Actas;

namespace EPPLipigas.Business.Contrato
{
    public interface IActasBO
    {
        ListarActasEPPResponse ListarActasEPP(int idUsuario, string guid);
        SincronizarActaResponse SincronizarActa(SincronizarActasRequest req, int idUsuario, int id_aplicacion, string guid);
        AprobacionEntregaResponse AprobacionEntrega(int id_colaborador_entrega, string rut, int id_usuario, int aplicacion, string guid, System.Web.Mvc.ControllerContext controllerContext);
        string AprobarEntrega_EnvioCorreo(string correoColaborador, string documento_hash, string correoResponsable, string nombre, string apellido);
        RecepcionarEdigitalResponse Recepcion_edigital(RecepcionarEdigitalRequest req, string guid);
    }
}
