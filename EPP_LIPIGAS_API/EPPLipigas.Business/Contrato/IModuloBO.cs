﻿using EPPLipigas.Entities.Modulo;

namespace EPPLipigas.Business.Contrato
{
    public interface IModuloBO
    {
        ModuloResponse ModuloRolesXUsuarios(int idUsuario);
    }
}
