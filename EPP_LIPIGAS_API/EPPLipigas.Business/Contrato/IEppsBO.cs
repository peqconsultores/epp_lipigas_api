﻿using EPPLipigas.Entities.Epp.Request;
using EPPLipigas.Entities.Epp.Response;

namespace EPPLipigas.Business.Contrato
{
    public interface IEppsBO
    {
        ListarEppsXFiltroResponse ListarEppsXFiltros(string categoria, string clasificacion, string epp, string estado, int num_pagina, int cant_registros, int idUsuario);
        RegistrarEppsResponse RegistrarEpps(string cod_clasificacion, string cod_categoria, int aplicacion, int usuario, string epp);
        EliminarEppResponse EliminarEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion);
        EditarEppResponse EditarEpp(int idEquipoProteccionPersonal, EditarEppRequest request, int id_usuario, string cod_aplicacion);
        ObtenerDetalleEppResponse ObtenerDetalleEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion);
        ReporteEppsResponse ListarEppsxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario);
        ReportEppExcelResponse ListarEppsExcelxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int idUsuario);
    }
}
