﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Instalacion
{
    public class ListarInstalacionxusuario : GeneralResponse
    {
        public List<DataInstalacionUsu> listaInstalaciones { get; set; }

    }

    public class DataInstalacionUsu 
    {
        public int id_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public List<DatosUsuario> listaUsuario { get; set; }

        
    }

    public class DatosUsuario
    {
        public int id_persona { get; set; }
        public string rut { get; set; }
        public string nombres { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }

    }

    public class UsuariosPorInstalacionResponse : GeneralResponse
    {
        public List<DataUsuariosPorInstalacion> listaUsuariosPorInstalacion { get; set; }

       

    }

    public class DataUsuariosPorInstalacion 
    {
        public int id_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public int id_persona { get; set; }
        public string rut { get; set; }
        public string nombres { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
    }

   

}
