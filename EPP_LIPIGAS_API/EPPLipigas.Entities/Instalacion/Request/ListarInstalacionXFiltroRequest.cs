﻿using System.ComponentModel.DataAnnotations;

namespace EPPLipigas.Entities.Instalacion.Request
{
    public class ListarInstalacionXFiltroRequest
    {
        public string instalacion { get; set; }
        public string cod_tipo_instalacion { get; set; }
        public string estado { get; set; }

        [Required(ErrorMessage = "El número de pagina es obligatorio.")]
        public int num_pagina { get; set; }

        [Required(ErrorMessage = "La cantidad de registros es obligatorio.")]
        public int cant_registros { get; set; }
    }
}
