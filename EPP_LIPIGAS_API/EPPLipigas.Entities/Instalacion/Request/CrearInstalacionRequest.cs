﻿using System.ComponentModel.DataAnnotations;

namespace EPPLipigas.Entities.Instalacion.Request
{
    public class CrearInstalacionRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre de la instalación es obligatorio.")]
        public string nombre_instalacion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El tipo de instalación es obligatorio.")]
        public string cod_tipo_instalacion { get; set; }
    }
}
