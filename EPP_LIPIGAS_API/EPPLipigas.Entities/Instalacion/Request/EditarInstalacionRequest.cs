﻿namespace EPPLipigas.Entities.Instalacion.Request
{
    public class EditarInstalacionRequest
    {
        public string cod_tipo_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public bool activo { get; set; }

    }
}
