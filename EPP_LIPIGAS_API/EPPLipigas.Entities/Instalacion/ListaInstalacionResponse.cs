﻿

using System.Collections.Generic;

namespace EPPLipigas.Entities.Instalacion
{
    public class ListaInstalacionResponse : GeneralResponse
    {

        public List<DataInstalacion> listaInstalacion { get; set; }

    }

    public class DataInstalacion
    {
        public string  id_instalacion { get; set; }
        public string cod_instalacion { get; set; }

        public string nombre_instalacion { get; set; }
    }
}
