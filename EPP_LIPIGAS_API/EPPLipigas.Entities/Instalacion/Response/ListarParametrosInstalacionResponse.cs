﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Instalacion.Response
{
    public class ListarParametrosInstalacionResponse : GeneralResponse
    {
        public ListadoParametrosInstalacionData datos { get; set; }
    }
    public class ListadoParametrosInstalacionData
    {
        public List<DatosTipoInstalacion> listaTipoInstalacion { get; set; }
    }
    public class DatosTipoInstalacion
    {
        public string cod_tipo_instalacion { get; set; }
        public string nombre_tipo_instalacion { get; set; }
    }
    public class DatosTipoInstalacionResponse : GeneralResponse
    {
        public List<DatosTipoInstalacion> datos { get; set; }
    }
}
