﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Instalacion.Response
{
    public class ListarInstalacionXFiltroResponse : GeneralResponse
    {
        public int total_registros { get; set; }
        public List<ListarInstalacionXFiltroResponse_Data> data { get; set; }
    }

    public class ListarInstalacionXFiltroResponse_Data
    {
        public string fecha_creacion { get; set; }
        public int id_instalacion { get; set; }
        public string cod_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public string cod_tipo_instalacion { get; set; }
        public string nombre_tipo_instalacion { get; set; }
        public bool estado { get; set; }
        public string nombre_estado { get; set; }
    }
}
