﻿namespace EPPLipigas.Entities.Instalacion.Response
{
    public class DetalleInstalacionResponse : GeneralResponse
    {
        public DataInstalacion datos { get; set; }
    }
    public class DataInstalacion
    {
        public int id_instalacion { get; set; }
        public string cod_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public string cod_tipo_instalacion { get; set; }
        public string nombre_tipo_instalacion { get; set; }
        public bool estado { get; set; }
    }
}
