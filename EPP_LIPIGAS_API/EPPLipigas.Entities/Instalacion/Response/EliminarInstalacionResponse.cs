﻿namespace EPPLipigas.Entities.Instalacion.Response
{
    public class EliminarInstalacionResponse : GeneralResponse
    {
        public int flag { get; set; }
    }
}
