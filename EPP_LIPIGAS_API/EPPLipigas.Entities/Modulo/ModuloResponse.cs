﻿using System;
using System.Collections.Generic;

namespace EPPLipigas.Entities.Modulo
{
    public class ModuloResponse : GeneralResponse
    {
        public List<Modulos> data { get; set; }
    }
    public class Modulos
    {
        public string codModulo { get; set; }
        public string dModulo { get; set; }
        public string icon { get; set; }
        public Nullable<int> orden { get; set; }
        public Nullable<int> nivel { get; set; }
        public int nivelMaxSubModulos { get; set; }

        public List<submodulo> listasubmodulos { get; set; }
    }

    public class submodulo
    {
        public string codModulo { get; set; }
        public string dModulo { get; set; }
        public string controller { get; set; }
        public string actionName { get; set; }
        public string codModuloPadre { get; set; }
        public string ruta { get; set; }
        public string icon { get; set; }
        public Nullable<int> orden { get; set; }
        public Nullable<int> nivel { get; set; }
    }
}
