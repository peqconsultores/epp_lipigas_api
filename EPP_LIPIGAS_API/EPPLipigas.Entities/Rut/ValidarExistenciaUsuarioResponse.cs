﻿namespace EPPLipigas.Entities.Rut
{
    public class ValidarExistenciaUsuarioResponse : GeneralResponse
    {
        public int id_persona { get; set; }
    }
}
