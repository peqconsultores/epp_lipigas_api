﻿

namespace EPPLipigas.Entities.Rut
{
    public class BusquedaRutResponse : GeneralResponse
    {
        public DatosBusquedaRut datos { get; set; }
    }

    public class DatosBusquedaRut
    {
        public string rut { get; set; }
        public string nombres { get; set; }
        public string apellido_materno { get; set; }
        public string apellido_paterno { get; set; }
        public string correo { get; set; }
        public string instalacion { get; set; }
        public string celular { get; set; }
    }
}
