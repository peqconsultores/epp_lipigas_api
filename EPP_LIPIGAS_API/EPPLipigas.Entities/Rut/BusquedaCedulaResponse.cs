﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Rut
{
    public class BusquedaCedulaResponse : GeneralResponse
    {
        public List<BusquedaCedulaDatosResponse> datos { get; set; }
    }
    public class BusquedaCedulaDatosResponse
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public int numeroTelefonico { get; set; }
    }
}
