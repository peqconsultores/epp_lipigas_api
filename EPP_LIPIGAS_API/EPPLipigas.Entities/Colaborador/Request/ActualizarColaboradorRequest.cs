﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Colaborador.Request
{
   public class ActualizarColaboradorRequest
    {
        public int id_colaborador_entrega { get; set; }
        public string genero { get; set; }
        public string correo { get; set; }
        public string fecha_nacimiento { get; set; }
    }
}
