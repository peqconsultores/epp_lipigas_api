﻿namespace EPPLipigas.Entities.Colaborador.Request
{
    public class ReporteColaboradorExcelRequest
    {
        public string fechaDesde { get; set; }
        public string fechaHasta { get; set; }
        public string nro_actaEPP { get; set; }
        public string estado_acta { get; set; }
        public string rut_responsable { get; set; }
        public string rut_colaborador { get; set; }
        public string tipo_colaborador { get; set; }


    }
}
