﻿
using System.ComponentModel.DataAnnotations;


namespace EPPLipigas.Entities.Colaborador.Request
{
    public class ReporteColaboradoresRequest
    {
        public string fechaDesde { get; set; }
        public string fechaHasta { get; set; }
        public string nro_actaEPP { get; set; }
        public string estado_acta { get; set; }
        public string rut_responsable { get; set; }
        public string rut_colaborador { get; set; }
        public string tipo_colaborador { get; set; }

        [Required(ErrorMessage = "El número de pagina es obligatorio.")]
        public int num_pagina { get; set; }

        [Required(ErrorMessage = "La cantidad de registros es obligatorio.")]
        public int cant_registros { get; set; }


    }
}
