﻿namespace EPPLipigas.Entities.Colaborador.Request
{
    public class RegistrarColaboradorAppRequest
    {
        public string rut { get; set; }
        public string nombres { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string cod_instalacion { get; set; }
        public string cod_rol { get; set; }
        public string cod_tipo_colaborador { get; set; }
        public string cod_empresa { get; set; }
        public string nombre_empresa { get; set; }
        public string cod_area { get; set; }
        public string area { get; set; }
        public string correo { get; set; }
        public string celular { get; set; }
        public string cod_cargo { get; set; }

        public string fecha_nacimiento { get; set; }
        public string genero { get; set; }

    }
}
