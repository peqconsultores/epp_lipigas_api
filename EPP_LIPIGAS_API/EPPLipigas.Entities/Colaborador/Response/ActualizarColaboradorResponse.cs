﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ActualizarColaboradorResponse :GeneralResponse
    {
        public bool flag_vista { get; set; }
    }
}
