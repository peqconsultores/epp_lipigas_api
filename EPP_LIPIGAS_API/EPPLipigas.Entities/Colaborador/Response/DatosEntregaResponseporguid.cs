﻿using System;
using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class DatosEntregaResponseporguid : GeneralResponse
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }

        public Colaboradorporguid datosColaboradorporguid { get; set; }

    }

    public class Colaboradorporguid
    {
        public Cabeceraporguid cabeceraporguid { get; set; }
        public List<EppPorGuid> listadoEppporguid { get; set; }
    }


    public class Cabeceraporguid
    {
        public string guid_aprobacion { get; set; }
        public string responsable { get; set; }
        public string colaborador { get; set; }
        public string empresa { get; set; }
        public string rut { get; set; }
        public Nullable<int> id_colaborador_entrega { get; set; }
        public string estado_colaborador_entrega { get; set; }

    }
    public class EppPorGuid
    {
        public string descripcion { get; set; }
        public string cantidad { get; set; }
        public string clasificacion { get; set; }

    }
}
