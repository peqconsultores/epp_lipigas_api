﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ListarTipodecolaboradoresxUsuario : GeneralResponse
    {
        public List<DataTipoColaborador> ListadeTiposColaborador { get; set; }
    }

    public class DataTipoColaborador
    {
        public int id_tipo_colaborador { get; set; }

        public string cod_colaborador { get; set; }
        public string descripcion { get; set; }


    }



}
