﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class DetalleEntregaResponse : GeneralResponse
    {
        public int flag_boton_aprobar { get; set; }
        public int flag_boton_entrega { get; set; }
        public int id_colaborador_entrega { get; set; }

        public CabeceraCol datosCabecera { get; set; }

        public List<Epps> listadoEpp { get; set; }

    }

    public class CabeceraCol
    {

        public string nombre { get; set; }

        public string empresa { get; set; }
        public string estado { get; set; }
        public string codigo { get; set; }
        public string responsable { get; set; }

    }


    public class Epps
    {
        public string descripcion { get; set; }
        public string cantidad { get; set; }
        public string clasificacion { get; set; }

    }
}
