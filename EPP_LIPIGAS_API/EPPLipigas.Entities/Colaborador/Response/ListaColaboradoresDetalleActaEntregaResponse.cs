﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ListaColaboradoresDetalleActaEntregaResponse : GeneralResponse
    {
        public List<ListaColaboradoresActa> datosColaboradores { get; set; }

    }

    public class ListaColaboradoresActa
    {
        public string nombres { get; set; }
        public string apellido_materno { get; set; }
        public string apellido_paterno { get; set; }
        public string rut { get; set; }
    }
}
