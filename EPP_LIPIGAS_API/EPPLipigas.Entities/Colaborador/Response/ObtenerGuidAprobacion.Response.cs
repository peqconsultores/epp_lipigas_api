﻿using System;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ObtenerGuidAprobacion : GeneralResponse
    {
        public Nullable<int> codigo { get; set; }

        public string guid_entregado { get; set; }
        public string link { get; set; }
        public string correo { get; set; }
        public string correo_responsable { get; set; }




    }
}
