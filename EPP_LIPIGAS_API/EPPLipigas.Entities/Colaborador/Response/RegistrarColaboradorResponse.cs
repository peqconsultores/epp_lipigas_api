﻿namespace EPPLipigas.Entities.Colaborador.Response
{
    public class RegistrarColaboradorResponse : GeneralResponse
    {
        public int id_persona { get; set; }
    }
}
