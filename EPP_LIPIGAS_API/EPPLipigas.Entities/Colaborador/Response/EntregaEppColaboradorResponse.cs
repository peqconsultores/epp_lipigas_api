﻿
namespace EPPLipigas.Entities.Colaborador.Response
{
    public class EntregaEppColaboradorResponse : GeneralResponse
    {
        public string document_hash { get; set; }
        public bool flag_mostrar { get; set; }
    }
    public class EntregaEppColaboradorIDResponse : GeneralResponse
    {
        public EntregaEppColaboradorIDResponse_Data data { get; set; }
    }
    public class EntregaEppColaboradorIDResponse_Data
    {
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string rut { get; set; }
        public string tipo_documento { get; set; }
        public string correo { get; set; }
        public string genero { get; set; }
        public string fecha_nacimiento { get; set; }
        public string celular { get; set; }
        public string tipo_usuario { get; set; }
        public int id_entrega { get; set; }
    }

}
