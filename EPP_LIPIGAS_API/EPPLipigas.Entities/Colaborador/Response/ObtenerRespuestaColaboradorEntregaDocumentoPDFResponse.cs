﻿namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ObtenerRespuestaColaboradorEntregaDocumentoPDFResponse : GeneralResponse
    {
        public string pdfBase64 { get; set; }
    }
}
