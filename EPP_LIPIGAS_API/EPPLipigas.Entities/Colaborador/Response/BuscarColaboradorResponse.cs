﻿namespace EPPLipigas.Entities.Colaborador.Response
{
    public class BuscarColaboradorResponse : GeneralResponse
    {
        public bool flagEncontrado { get; set; }
        public BuscarColaboradorResponse_Data data { get; set; }
    }
    public class BuscarColaboradorResponse_Data
    {
        public string nombre_colaborador { get; set; }
        public string nombre_instalacion { get; set; }
    }
}
