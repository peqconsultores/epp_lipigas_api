﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ReporteColaboradorExcelResponse : GeneralResponse
    {


        public List<ListarReporteColaboradoresExcelXFiltroResponse_Data> data { get; set; }
    }

    public class ListarReporteColaboradoresExcelXFiltroResponse_Data
    {
        public int nro { get; set; }
        public int id_colaborador_entrega { get; set; }
        public string documento { get; set; }
        public string fecha_creacion { get; set; }
        public string nro_acta_epp { get; set; }
        public string nombre_instalacion { get; set; }
        public string nombre_acta { get; set; }
        public string rut_responsable { get; set; }
        public string nombre_responsable { get; set; }
        public string cod_estado { get; set; }
        public string rut_colaborador { get; set; }
        public string nombre_Colaborador { get; set; }
        public string cod_colaborador { get; set; }
        public string nombre_tipo_colaborador { get; set; }
        public string fecha_entrega { get; set; }
        public string estado_colaborador { get; set; }



    }
}
