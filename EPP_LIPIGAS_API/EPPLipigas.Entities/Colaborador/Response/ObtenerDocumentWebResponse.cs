﻿namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ObtenerDocumentWebResponse : GeneralResponse
    {

        public datosDocumentoWeb datos { get; set; }
    }
    public class datosDocumentoWeb
    {

        public int id_documento { get; set; }
        public string documento { get; set; }
        public string rut_colaborador { get; set; }
    }
}
