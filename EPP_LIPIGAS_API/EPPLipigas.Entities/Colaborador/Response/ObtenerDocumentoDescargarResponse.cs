﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Colaborador.Response
{
    public class ObtenerDocumentoDescargarResponse :GeneralResponse
    {
        public string link_descarga_documento { get; set; }
    }
}
