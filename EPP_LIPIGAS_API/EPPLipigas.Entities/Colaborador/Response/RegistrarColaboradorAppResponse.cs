﻿namespace EPPLipigas.Entities.Colaborador.Response
{
    public class RegistrarColaboradorAppResponse : GeneralResponse
    {
        public int id_persona { get; set; }
    }
}
