﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Colaborador.ViewModels
{
    public class DetalleColaboradorEntregaVM
    {
        public string codigoacta { get; set; }
        public string fecha_entrega { get; set; }
        public string entidad { get; set; }
        public string estado { get; set; }
        public string responsable { get; set; }
        public string colaborador_nombre { get; set; }
        public string colaborador_apellido { get; set; }
        public string rut_colaborador { get; set; }
        public string area { get; set; }
        public string correo_responsable { get; set; }
        public string correo_colaborador { get; set; }
        public List<DetalleColaboradorEntregaVM_EPP> epps { get; set; }

    }

    public class DetalleColaboradorEntregaVM_EPP
    {
        public string nombreEpp { get; set; }
        public string clasificacion { get; set; }
        public int cantidad { get; set; }
    }
}
