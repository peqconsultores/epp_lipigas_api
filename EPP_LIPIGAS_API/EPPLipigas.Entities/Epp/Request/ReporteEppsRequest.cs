﻿using System.ComponentModel.DataAnnotations;

namespace EPPLipigas.Entities.Epp.Request
{
    public class ReporteEppsRequest
    {
        public string fechaDesde { get; set; }
        public string fechaHasta { get; set; }
        public string clasificacion { get; set; }
        public string categoria { get; set; }
        public string nombre_producto_epp { get; set; }
        public string rut_colaborador { get; set; }
        public string tipo_colaborador { get; set; }

        [Required(ErrorMessage = "El número de pagina es obligatorio.")]
        public int num_pagina { get; set; }

        [Required(ErrorMessage = "La cantidad de registros es obligatorio.")]
        public int cant_registros { get; set; }


    }
}
