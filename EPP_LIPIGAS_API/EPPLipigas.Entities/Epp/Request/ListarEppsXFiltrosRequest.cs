﻿using System.ComponentModel.DataAnnotations;

namespace EPPLipigas.Entities.Epp.Request
{
    public class ListarEppsXFiltrosRequest
    {
        public string categoria { get; set; }
        public string clasificacion { get; set; }
        public string epp { get; set; }

        public string estado { get; set; }

        [Required(ErrorMessage = "El número de pagina es obligatorio.")]
        public int num_pagina { get; set; }

        [Required(ErrorMessage = "La cantidad de registros es obligatorio.")]
        public int cant_registros { get; set; }


    }
}
