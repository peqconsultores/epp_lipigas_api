﻿namespace EPPLipigas.Entities.Epp.Request
{
    public class RegistrarEppsRequest
    {
        public string cod_clasificacion { get; set; }
        public string cod_categoria { get; set; }
        public string epp { get; set; }

    }
}
