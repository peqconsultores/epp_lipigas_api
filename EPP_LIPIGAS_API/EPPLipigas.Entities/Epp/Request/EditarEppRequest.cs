﻿namespace EPPLipigas.Entities.Epp.Request
{
    public class EditarEppRequest
    {
        public string nombreEpp { get; set; }
        public string codNivel1 { get; set; }
        public string codNivel2 { get; set; }
        public string codNivel3 { get; set; }
        public bool estado { get; set; }
    }
}
