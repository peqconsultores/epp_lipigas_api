﻿namespace EPPLipigas.Entities.Epp.Request
{
    public class ReporteEppsExcelRequest
    {
        public string fechaDesde { get; set; }
        public string fechaHasta { get; set; }
        public string clasificacion { get; set; }
        public string categoria { get; set; }
        public string nombre_producto_epp { get; set; }
        public string rut_colaborador { get; set; }
        public string tipo_colaborador { get; set; }
    }
}
