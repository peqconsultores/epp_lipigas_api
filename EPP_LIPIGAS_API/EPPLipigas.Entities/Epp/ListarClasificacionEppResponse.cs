﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Epp
{
   public  class ListarClasificacionEppResponse :GeneralResponse
    {

        public List<DataClasificacionEpp> listaDataClasificacionEpp { get; set; }
      
    }


    public class DataClasificacionEpp
    {
        public int id_clasificacion { get; set; }

        public string codigo { get; set; }
        public string clasificacion { get; set; }


    }
}
