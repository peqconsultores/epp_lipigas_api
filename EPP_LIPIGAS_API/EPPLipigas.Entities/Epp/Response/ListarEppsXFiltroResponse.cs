﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Epp.Response
{
    public class ListarEppsXFiltroResponse : GeneralResponse
    {
        public int total_registros { get; set; }
        public List<ListarEppXFiltroResponse_Data> data { get; set; }

    }
    public class ListarEppXFiltroResponse_Data
    {


        public int id_equipo_proteccion_personal { get; set; }
        public string descripcion { get; set; }
        public string clasificacion { get; set; }
        public string categoria { get; set; }
        public string Estado { get; set; }






    }
}
