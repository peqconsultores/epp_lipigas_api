﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Epp.Response
{
    public class ReportEppExcelResponse : GeneralResponse
    {
        public List<ListarReporteEppsXFiltroExcelResponse_Data> data { get; set; }

    }

    public class ListarReporteEppsXFiltroExcelResponse_Data
    {
        public int nro { get; set; }
        public int id_entrega { get; set; }
        public string fecha_creacion { get; set; }
        public string nro_acta_epp { get; set; }
        public string cantidad { get; set; }
        public string nombre_producto_epp { get; set; }
        public string clasificacion { get; set; }
        public string categoria { get; set; }
        public string rut_colaborador { get; set; }
        public string nombre_colaborador { get; set; }
        public string cod_colaborador { get; set; }
        public string nombre_tipo_colaborador { get; set; }
        public string rut_responsable { get; set; }
        public string nombre_responsable { get; set; }
    }
}
