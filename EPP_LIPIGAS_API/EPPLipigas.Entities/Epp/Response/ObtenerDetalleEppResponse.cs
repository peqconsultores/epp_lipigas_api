﻿namespace EPPLipigas.Entities.Epp.Response
{
    public class ObtenerDetalleEppResponse : GeneralResponse
    {
        public DetalleEpp datos { get; set; }
    }
    public class DetalleEpp
    {
        public int id_equipo_proteccion_personal { get; set; }
        public int id_nivel_1 { get; set; }
        public string cod_nivel_1 { get; set; }
        public string nombre_nivel_1 { get; set; }
        public int id_nivel_2 { get; set; }
        public string cod_nivel_2 { get; set; }
        public string nombre_nivel_2 { get; set; }
        public int id_nivel_3 { get; set; }
        public string cod_nivel_3 { get; set; }
        public string nombre_nivel_3 { get; set; }
        public string cod_epp { get; set; }
        public string nombre_epp { get; set; }
        public bool estado { get; set; }
    }
}
