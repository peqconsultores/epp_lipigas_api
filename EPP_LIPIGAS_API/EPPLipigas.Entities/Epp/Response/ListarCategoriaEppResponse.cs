﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Epp
{
    public class ListarCategoriaEppResponse : GeneralResponse
    {

        public List<DataCategoriaEpp> listaDataCategoriaEpp { get; set; }
    }

    public class DataCategoriaEpp
    {
        public int id_categoria { get; set; }
        public string codigo { get; set; }
        public string categoria { get; set; }


    }
}
