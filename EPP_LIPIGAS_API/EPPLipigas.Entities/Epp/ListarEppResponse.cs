﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Epp
{
     public class ListarEppResponse : GeneralResponse
    {
        public List<DataEpp> listaEpp{ get; set; }
    }


    public class DataEpp
    {

        public int id_equipo_proteccion_personal { get; set; }
        public string cod_epp { get; set; }
        public int id_clasificacion { get; set; }
        public string clasificacion { get; set; }
        public int id_categoria { get; set; }
        public string categoria { get; set; }
        public string descripcion { get; set; }

    }
}
