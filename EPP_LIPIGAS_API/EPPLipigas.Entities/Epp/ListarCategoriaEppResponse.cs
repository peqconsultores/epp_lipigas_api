﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.Epp
{
  public   class ListarCategoriaEppResponse :GeneralResponse
    {

        public List<DataCategoriaEpp> listaDataCategoriaEpp { get; set; }
    }

    public class DataCategoriaEpp
    {
        public int id_categoria { get; set; }
        public string codigo { get; set; }
        public string categoria { get; set; }


    }
}
