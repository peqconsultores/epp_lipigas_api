﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Aplicacion
{
    public class ListarParametrosResponse : GeneralResponse
    {
        public List<ListarParametrosDatosResponse> datos { get; set; }
    }
    public class ListarParametrosDatosResponse
    {
        public int id_parametro { get; set; }
        public string cod_parametro { get; set; }
        public string descripcion_corta { get; set; }
        public string descripcion_largo { get; set; }
        public string valor_1 { get; set; }
        public string valor_2 { get; set; }
    }
}
