﻿using EPPLipigas.Entities.Epp;
using EPPLipigas.Entities.Instalacion;
using EPPLipigas.Entities.Usuario.Response;
using System.Collections.Generic;

namespace EPPLipigas.Entities
{
    public class ListasResponse : GeneralResponse
    {
        public List<DataInstalacionUsu> listaUsuariosPorInstalacion { get; set; }
        public List<DataInstalacion> listaInstalacion { get; set; }
        public List<DataEpp> listaEpp { get; set; }

        public List<DataClasificacionEpp> listaDataClasificacionEpp { get; set; }

        public List<DataCargo> listaCargo { get; set; }
        public List<DataCategoriaEpp> listaDataCategoriaEpp { get; set; }

        public List<DataArea> listaArea { get; set; }

        public List<DataEmpresa> listaEmpresa { get; set; }
    }

}
