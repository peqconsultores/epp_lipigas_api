﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarDatosUsuariosResponse : GeneralResponse
    {


        public List<DatosUsuarios> listaDatosUsuarios { get; set; }
    }


    public class DatosUsuarios
    {

        public string rut { get; set; }
        public string nro_serie { get; set; }
        public string nombres { get; set; }
        public string apellido_materno { get; set; }
        public string apellido_paterno { get; set; }
        public string nombre_instalacion { get; set; }
        public string correo { get; set; }

        public string celular { get; set; }
        public string tipo_rol { get; set; }


    }
}
