﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListaTiposErroresCargaResponse : GeneralResponse
    {
        public List<DataErrores> ListaErroesCarga { get; set; }

    }

    public class DataErrores
    {
        public string codUsuarioProceso { get; set; }
        public string descripcion { get; set; }

    }
}
