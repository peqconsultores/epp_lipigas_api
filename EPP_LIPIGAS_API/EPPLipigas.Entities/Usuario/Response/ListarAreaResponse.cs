﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarAreaResponse : GeneralResponse
    {
        public List<DataArea> listaArea { get; set; }

    }

    public class DataArea
    {
        public string cod_area { get; set; }

        public string nombre_area { get; set; }
    }
}
