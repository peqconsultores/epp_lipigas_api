﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarEmpresaResponse : GeneralResponse
    {
        public List<DataEmpresa> listaEmpresa { get; set; }
    }

    public class DataEmpresa
    {

        public string cod_empresa { get; set; }

        public string nombre_empresa { get; set; }
    }
}
