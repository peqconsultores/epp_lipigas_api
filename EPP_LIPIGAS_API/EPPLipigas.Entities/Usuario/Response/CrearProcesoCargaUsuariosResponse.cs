﻿namespace EPPLipigas.Entities.Usuario.Response
{
    public class CrearProcesoCargaUsuariosResponse
    {
        public int idProcesoCargaUsuarios { get; set; }
        public string codProcesoCargaUsuarios { get; set; }
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }
}
