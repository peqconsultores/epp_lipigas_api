﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarCargoResponse : GeneralResponse
    {
        public List<DataCargo> listaCargo { get; set; }
    }

    public class DataCargo
    {
        public string codigo { get; set; }
        public string cargo { get; set; }

    }
}
