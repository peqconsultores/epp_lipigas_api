﻿using System;
using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarUsuariosXFiltrosResponse : GeneralResponse
    {
        public int total_registros { get; set; }
        public List<ListarUsuariosXFiltrosResponse_Data> data { get; set; }
    }
    public class ListarUsuariosXFiltrosResponse_Data
    {
        public Nullable<int> total_registros { get; set; }
        public int id_persona { get; set; }
        public string rut { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string nombre_instalacion { get; set; }
        public string tipo_rol { get; set; }
        public string correo { get; set; }
        public string nro_serie { get; set; }
        public string descripcion { get; set; }
        public string nombre_empresa { get; set; }
        public string nombre_area { get; set; }
        public string celular { get; set; }
        public string nombre_cargo { get; set; }
        public string estado_persona { get; set; }
    }
}
