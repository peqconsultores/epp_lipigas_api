﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ProcesoCargalistadoResponse : GeneralResponse
    {
        public int total_registros { get; set; }
        public List<ListarProcesoCargaResponse_Data> data { get; set; }
    }
    public class ListarProcesoCargaResponse_Data
    {

        public int id_usuarios_proceso { get; set; }
        public string cod_usuarios_proceso_modificacion { get; set; }
        public int id_usuarios_proceso_carga_estado { get; set; }
        public string cod_usuarios_proceso_carga_estado { get; set; }
        public string nombre { get; set; }
        public string descripcion_proceso_carga_estado { get; set; }
        public string nombre_archivo { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
        public int total_global { get; set; }
        public int total_completos { get; set; }

    }
}
