﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class ListarProcesoCargaErroresResponseExcel : GeneralResponse
    {
        public List<ListarProcesoCargaErroresExcelResponse_Data> data { get; set; }
    }

    public class ListarProcesoCargaErroresExcelResponse_Data
    {

        public int id_usuarios_proceso_modificacion_final_error_cab { get; set; }
        public int numfila { get; set; }
        public string rut { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string nombre { get; set; }

        public string cargo { get; set; }
        public string rol_usuario { get; set; }
        public string tipo_colaborador { get; set; }
        public string instalacion { get; set; }
        public string correo { get; set; }
        public string celular { get; set; }
        public string nombre_empresa { get; set; }
        public string area { get; set; }
        public string fecha_nacimiento { get; set; }
        public string genero { get; set; }


        public int id_usuarios_proceso_modificacion_final_error_det { get; set; }
        public string derror { get; set; }



    }
}
