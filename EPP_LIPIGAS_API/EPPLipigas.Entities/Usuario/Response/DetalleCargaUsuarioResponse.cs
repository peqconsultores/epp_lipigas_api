﻿namespace EPPLipigas.Entities.Usuario.Response
{
    public class DetalleCargaUsuarioResponse : GeneralResponse
    {
        public DetalleCarga data { get; set; }
    }

    public class DetalleCarga
    {

        public int id_usuarios_proceso { get; set; }
        public string cod_usuarios_proceso_modificacion { get; set; }
        public string cod_usuarios_proceso_carga_estado { get; set; }
        public string sigla { get; set; }
        public string descripcionProCarEstado { get; set; }
        public string nombre_archivo { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
        public int total_global { get; set; }
        public int total_completos { get; set; }
        public int cantidadErrores { get; set; }
        public double porcentaje_avance { get; set; }


    }
}
