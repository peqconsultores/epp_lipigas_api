﻿using System;
using System.Collections.Generic;

namespace EPPLipigas.Entities.Usuario.Response
{
    public class SeguimientoEppResponse : GeneralResponse
    {



        public string codigorol { get; set; }
        public Colaborador datosColaborador { get; set; }
        public Responsable datosResponsable { get; set; }

    }
    public class Colaborador
    {
        public Cabecera cabecera { get; set; }
        public List<Epp> listadoEpp { get; set; }
    }
    public class Cabecera
    {
        public string nombre { get; set; }
        public string rut { get; set; }
        public string empresa { get; set; }
        public string estado { get; set; }
        public string responsable { get; set; }

        public Nullable<int> id_colaboradorEntrega { get; set; }

        public Nullable<int> flag_boton_aprobado { get; set; }
        public string documento { get; set; }
    }
    public class Epp
    {
        public string descripcion { get; set; }
        public string cantidad { get; set; }
        public string clasificacion { get; set; }

    }

    public class Responsable
    {

        public CabeceraRes cabecera { get; set; }

        public List<Colaboradores> listadoColaboradores { get; set; }


    }


    public class CabeceraRes
    {
        public string instalacion { get; set; }
        public string codigo { get; set; }
        public string responsable { get; set; }
        public string estado { get; set; }
        public string fecha { get; set; }



    }
    public class Colaboradores
    {

        public string nombres { get; set; }
        public string apellido_materno { get; set; }
        public string apellido_paterno { get; set; }
        public string rut { get; set; }
        public int id_colaborador_entrega { get; set; }

        public string documento { get; set; }
        public int estado_colaborador { get; set; }


    }



}
