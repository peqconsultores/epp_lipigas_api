﻿namespace EPPLipigas.Entities.Usuario.Response
{
    public class CreacionProcesoCargaUsuariosResponse
    {
        public int idProcesoCargaUsuarios { get; set; }
        public string codProcesoCargaUsuarios { get; set; }
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }

    public class RegistroDatosProcesoCargaUsuariosResponse
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }
}
