﻿namespace EPPLipigas.Entities.Usuario.Request
{
    public class ListarUsuariosXFiltrosRequest
    {
        public string nombre { get; set; }
        public string rut { get; set; }
        public int num_pagina { get; set; }
        public int cant_registros { get; set; }
    }
}
