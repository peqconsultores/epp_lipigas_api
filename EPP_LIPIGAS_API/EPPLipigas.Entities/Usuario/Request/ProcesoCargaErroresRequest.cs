﻿namespace EPPLipigas.Entities.Usuario.Request
{
    public class ProcesoCargaErroresRequest
    {
        public int idProcesoCarga { get; set; }
        public string idsCargaErrorEstados { get; set; }
        public string criterioBusqueda { get; set; }
        public int num_pagina { get; set; }
        public int cant_registro { get; set; }

    }
}
