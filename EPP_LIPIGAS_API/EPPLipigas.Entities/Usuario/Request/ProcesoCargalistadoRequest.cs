﻿namespace EPPLipigas.Entities.Usuario.Request
{
    public class ProcesoCargalistadoRequest
    {
        public int num_pagina { get; set; }

        public int cant_registros { get; set; }

        public string criterio_busqueda { get; set; }


    }
}
