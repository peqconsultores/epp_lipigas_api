﻿namespace EPPLipigas.Entities.Usuario.Request
{
    public class RegistroProcesoCargaUsuariosRequest
    {
        public int numfila { get; set; }
        public string rut { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string nombre { get; set; }

        public string cargo { get; set; }
        public string rol_usuario { get; set; }
        public string tipo_colaborador { get; set; }
        public string instalacion { get; set; }

        public string correo { get; set; }
        public string celular { get; set; }
        public string nombre_empresa { get; set; }
        public string area { get; set; }
        public string fecha_nacimiento { get; set; }
        public string genero { get; set; }

    }
}
