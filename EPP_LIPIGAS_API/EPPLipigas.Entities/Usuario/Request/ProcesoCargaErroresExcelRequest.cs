﻿namespace EPPLipigas.Entities.Usuario.Request
{
    public class ProcesoCargaErroresExcelRequest
    {
        public int idProcesoCarga { get; set; }
        public string idsCargaErrorEstados { get; set; }
        public string criterioBusqueda { get; set; }

    }
}
