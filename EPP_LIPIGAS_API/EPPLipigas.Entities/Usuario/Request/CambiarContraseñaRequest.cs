﻿using System.ComponentModel.DataAnnotations;

namespace EPPLipigas.Entities.Usuario.Request
{
    public class CambiarContraseñaRequest
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Actual ")]
        public string ActualContrasenia { get; set; }

        [Required]
        [StringLength(15, ErrorMessage = "El {0} debe tener al menos {2} caracteres.", MinimumLength = 8)]
        //[StringLength(15, MinimumLength = 8, ErrorMessage = "* Part numbers must be between 3 and 50 character in length.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña")]
        public string NuevaContrasenia { get; set; }


    }
}
