﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.EDigital.Request
{
    public class CrearDocumentoRequest
    {
        public string description { get; set; }
        public string template_id { get; set; }

        public List<string> signers { get; set; }
        public List<string> final_recipients { get; set; }

        public object metadata { get; set; }
        public object settings { get; set; }
        public string document_file { get; set; }
        public string document_b64 { get; set; }
        public string document_link { get; set; }

    }


}
