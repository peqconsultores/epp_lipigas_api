﻿namespace EPPLipigas.Entities.EDigital.Request
{
    public class CrearUsuarioRequest
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string identity_document { get; set; }
        public string type_identity_document { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string birth { get; set; }
        public string phone { get; set; }
        public string type { get; set; }
    }
}
