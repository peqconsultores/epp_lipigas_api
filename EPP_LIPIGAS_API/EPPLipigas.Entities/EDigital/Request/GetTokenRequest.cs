﻿namespace EPPLipigas.Entities.EDigital.Request
{
    public class GetTokenRequest
    {
        public string grant_type { get; set; }
        public string cod_user { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
    }
}
