﻿namespace EPPLipigas.Entities.EDigital.Response
{
    public class TokenResponse
    {
        public int code { get; set; }
        public int duration { get; set; }

        public string time { get; set; }
        public string message { get; set; }
        public string trackingId { get; set; }

        public GetTokenResponse_OK token { get; set; }

    }



}
