﻿using System.Collections.Generic;
using System.Net;

namespace EPPLipigas.Entities.EDigital.Response
{
    public class ObtenerDocumentoResponse : GeneralResponse
    {
        public HttpStatusCode codeHTTP { get; set; }
        public string messageHTTP { get; set; }
        public string code { get; set; }

        public ObtenerDocumentoResponse_OK data { get; set; }
        public ObtenerDocumentoResponse_BadRequestYOtros data_badquest_otros { get; set; }

    }

    public class ObtenerDocumentoResponse_OK
    {

        public string code { get; set; }
        public string duration { get; set; }
        public string time { get; set; }
        public string message { get; set; }
        public string trackingId { get; set; }
        public ObtenerDocumentoResponse_OK_document document { get; set; }

    }
    public class ObtenerDocumentoResponse_OK_document
    {

        public string hash { get; set; }
        public string document_id { get; set; }
        public string description { get; set; }
        public string state { get; set; }

        public ObtenerDocumentoResponse_OK_metadata metadata { get; set; }
        public List<ObtenerDocumentoResponse_OK_actions> actions { get; set; }
        public string output { get; set; }

    }
    public class ObtenerDocumentoResponse_OK_metadata
    {
        public string id_acta { get; set; }

        public string id_Colaborador_entrega { get; set; }
    }

    public class ObtenerDocumentoResponse_OK_actions
    {
        public string identity_document { get; set; }

        public string action { get; set; }
        public string date { get; set; }
    }

    public class ObtenerDocumentoResponse_BadRequestYOtros
    {
        public string message { get; set; }
    }
}
