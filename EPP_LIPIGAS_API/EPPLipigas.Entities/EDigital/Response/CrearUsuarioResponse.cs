﻿using System.Net;

namespace EPPLipigas.Entities.EDigital.Response
{
    public class CrearUsuarioResponse
    {
        public HttpStatusCode codeHTTP { get; set; }
        public string messageHTTP { get; set; }
        public CrearUsuarioResponse_OK data { get; set; }
        public CrearUsuarioResponse_BadRequestYOtros data_badquest_otros { get; set; }
    }

    public class CrearUsuarioResponse_OK
    {
        public int code { get; set; }
        public int duration { get; set; }

        public string time { get; set; }
        public string message { get; set; }
        public string trackingId { get; set; }
        public string cod_user { get; set; }
    }

    public class CrearUsuarioResponse_BadRequestYOtros
    {
        public string message { get; set; }
    }

}
