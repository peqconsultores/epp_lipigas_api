﻿using System.Net;

namespace EPPLipigas.Entities.EDigital.Response
{
    public class GetTokenResponse
    {

        public HttpStatusCode codeHTTP { get; set; }
        public string messageHTTP { get; set; }
        public GetTokenResponse_OK token { get; set; }
        public GetTokenResponse_BadRequestYOtros data_badquest_otros { get; set; }
    }
    public class GetTokenResponse_OK
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }

        public string token_type { get; set; }
        public string scope { get; set; }
    }

    public class GetTokenResponse_BadRequestYOtros
    {
        public string message { get; set; }
    }
}
