﻿using System.Collections.Generic;
using System.Net;

namespace EPPLipigas.Entities.EDigital.Response
{
    public class GetValidUsuarioResponse
    {
        public HttpStatusCode codeHTTP { get; set; }
        public string messageHTTP { get; set; }
        public GetValidUsuarioResponse_OK data { get; set; }
        public GetValidUsuarioResponse_BadRequestYOtros data_badquest_otros { get; set; }
    }
    public class GetValidUsuarioResponse_OK
    {
        public string rut { get; set; }
        public string message { get; set; }
        public List<GetValidUsuarioResponse_OK_Detalle> detalle { get; set; }
    }
    public class GetValidUsuarioResponse_OK_Detalle
    {
        public string name { get; set; }
        public string surname { get; set; }

        public string email { get; set; }
        public string identity_document { get; set; }
        public string type_identity_document { get; set; }
        public string serie_identity_document { get; set; }
        public string address { get; set; }
        public string gender { get; set; }
        public string birth { get; set; }
        public string phone { get; set; }
        public string type { get; set; }
        public string certificate { get; set; }
        public int validated { get; set; }

    }

    public class GetValidUsuarioResponse_BadRequestYOtros
    {
        public string message { get; set; }
        public string errors { get; set; }

    }
}

