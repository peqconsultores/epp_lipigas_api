﻿using System.Net;

namespace EPPLipigas.Entities.EDigital.Response
{
    public class CrearDocumentoResponse
    {
        public HttpStatusCode codeHTTP { get; set; }
        public string messageHTTP { get; set; }
        public CrearDocumentoResponse_OK data { get; set; }
        public CrearDocumentoResponse_BadRequestYOtros data_badquest_otros { get; set; }
    }
    public class CrearDocumentoResponse_OK
    {
        public int code { get; set; }
        public int duration { get; set; }

        public string time { get; set; }
        public string message { get; set; }
        public string trackingId { get; set; }
        public string document_hash { get; set; }

    }
    public class CrearDocumentoResponse_BadRequestYOtros
    {
        public string message { get; set; }
    }
}
