﻿namespace EPPLipigas.Entities.Actas
{
    public class ObtenerDocumentoResponseConfirm : GeneralResponse
    {

        public datosDocumento datos { get; set; }
    }
    public class datosDocumento
    {

        public int id_documento { get; set; }
        public string documento { get; set; }
        public string rut_colaborador { get; set; }
    }
}
