﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class ObtenerdatosdeEppPDFResponse : GeneralResponse
    {
        public List<DataEppPDF> datos { get; set; }
    }
    public class DataEppPDF
    {
        public string epp { get; set; }
        public string clasificaicon { get; set; }
        public string categoria { get; set; }
        public string cantidad { get; set; }

    }
}
