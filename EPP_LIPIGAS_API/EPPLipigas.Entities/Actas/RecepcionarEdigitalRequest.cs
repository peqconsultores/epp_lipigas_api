﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class RecepcionarEdigitalRequest
    {
        public string document_id { get; set; }
        public string document_hash { get; set; }
        public string document_description { get; set; }
        public string document_type { get; set; }
        public string document_state { get; set; }
        public int document_progress { get; set; }
        public List<string> recipients { get; set; }

        public string file_url { get; set; }
        public string file_md5 { get; set; }
        public string unity_cod { get; set; }

        public GuardardatosDocumentoResponse_OK_metadata document_meta { get; set; }

        public List<GuardardatosDocumentoResponse_OK_actions> actions { get; set; }

    }
    public class GuardardatosDocumentoResponse_OK_metadata
    {
        public string client_document_id { get; set; }
        public string cod_sap { get; set; }

    }

    public class GuardardatosDocumentoResponse_OK_actions
    {

        public string identity_document { get; set; }

        public string action { get; set; }
        public string date { get; set; }


    }




}
