﻿namespace EPPLipigas.Entities.Actas
{
    public class ObtenerDatosColaboradorResponse : GeneralResponse
    {
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string rut { get; set; }
        public string tipo_documento { get; set; }
        public string correo { get; set; }
        public string genero { get; set; }
        public string fecha_nacimiento { get; set; }
        public string celular { get; set; }
        public string tipo_usuario { get; set; }
    }
}
