﻿namespace EPPLipigas.Entities.Actas
{
    public class AprobacionEntregaRequest
    {
        public int id_colaborador_entrega { get; set; }
        public string rut { get; set; }
        public string document_hash { get; set; }

    }
}
