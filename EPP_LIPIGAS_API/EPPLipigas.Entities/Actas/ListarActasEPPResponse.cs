﻿using System;
using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class ListarActasEPPResponse : GeneralResponse
    {
        public Nullable<bool> flag { get; set; }



        public List<ListarActasEPPDatosResponse> datos { get; set; }
    }
    public class ListarActasEPPDatosResponse
    {


        public int id_entrega { get; set; }
        public string cod_entrega { get; set; }
        public string fecha_entrega { get; set; }
        public Nullable<int> id_estado_entrega { get; set; }
        public string nombre_estado_acta { get; set; }

        public string nombre_estado_entrega { get; set; }



    }
}
