﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class DatosEntregaPEFResponse : GeneralResponse
    {
        public datosEntrega datos { get; set; }
    }
    public class datosEntrega
    {

        public string responsable { get; set; }
        public string estado { get; set; }
        public string fecha_entrega { get; set; }
        public string rut_responsable { get; set; }
        public string codigo_entrega { get; set; }
        public string entidad { get; set; }
        public string correo_responsable { get; set; }
        public string tipo_colaborador { get; set; }
        public string empresa { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string rut { get; set; }
        public string area_empresa { get; set; }
        public string correo_colaborador { get; set; }
    }


    public class DatosEntregaPDF_EppsResponse : GeneralResponse
    {
        public DatosEntregaPDF_EppsResponse_Detalle datos { get; set; }
    }
    public class DatosEntregaPDF_EppsResponse_Detalle
    {

        public string responsable { get; set; }
        public string estado { get; set; }
        public string fecha_entrega { get; set; }
        public string rut_responsable { get; set; }
        public string codigo_entrega { get; set; }
        public string entidad { get; set; }
        public string correo_responsable { get; set; }
        public string tipo_colaborador { get; set; }
        public string empresa { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string rut { get; set; }
        public string area_empresa { get; set; }
        public string correo_colaborador { get; set; }
        public List<DatosEntregaPDF_EppsResponse_Epps> epps { get; set; }
    }
    public class DatosEntregaPDF_EppsResponse_Epps
    {

        public string epp { get; set; }
        public string clasificaicon { get; set; }
        public string categoria { get; set; }
        public string cantidad { get; set; }
    }
}
