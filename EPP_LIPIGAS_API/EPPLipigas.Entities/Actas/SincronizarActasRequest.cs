﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class SincronizarActasRequest
    {


        public int id_instalacion { get; set; }




        public string fecha { get; set; }

        public List<listacolaboradores> datoscolaboradores { get; set; }
    }




    public class listacolaboradores
    {
        public int id_persona { get; set; }


        public List<listaEPP> datosEpp { get; set; }


    }

    public class listaEPP
    {
        public int id_epp { get; set; }

        public string cantidad { get; set; }


    }
}
