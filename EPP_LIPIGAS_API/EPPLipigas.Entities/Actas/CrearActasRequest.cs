﻿namespace EPPLipigas.Entities.Actas
{
    public class CrearActasRequest
    {
        public string instalacion { get; set; }
        public string rut { get; set; }

        public string area { get; set; }

        public string empresa { get; set; }

        public string descripcion_epp { get; set; }

        public string tipo_colaborador { get; set; }

        public string cantidad { get; set; }

    }
}
