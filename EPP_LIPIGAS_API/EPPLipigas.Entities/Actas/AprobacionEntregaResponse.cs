﻿using System.Collections.Generic;

namespace EPPLipigas.Entities.Actas
{
    public class AprobacionEntregaResponse : GeneralResponse
    {

        public int Codigo { get; set; }
        public string descripcion { get; set; }

        public string documento_hash { get; set; }


        public List<DatosEpp> datos { get; set; }

        public string responsable { get; set; }
        public string estado { get; set; }
        public string fecha_entrega { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string rut { get; set; }
        public string area_empresa { get; set; }
        public string link { get; set; }



    }
    public class DatosEpp
    {
        public string epp { get; set; }
        public string clasificaicon { get; set; }
        public string cantidad { get; set; }

    }
}
