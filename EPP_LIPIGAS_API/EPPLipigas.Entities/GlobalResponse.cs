﻿using System.Net;

namespace EPPLipigas.Entities
{
    public abstract class GeneralResponse
    {
        public HttpStatusCode codigoRes { get; set; }
        public string mensajeRes { get; set; }
    }
}
