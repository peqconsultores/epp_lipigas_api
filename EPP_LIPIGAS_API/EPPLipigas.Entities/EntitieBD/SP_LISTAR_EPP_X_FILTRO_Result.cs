﻿namespace EPPLipigas.Entities.EntitieBD
{
    public class SP_LISTAR_EPP_X_FILTRO_Result
    {
        public int id_equipo_proteccion_personal { get; set; }
        public string descripcion { get; set; }
        public string clasificacion { get; set; }
        public string categoria { get; set; }
        public string Estado { get; set; }
    }
}
