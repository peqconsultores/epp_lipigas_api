﻿namespace EPPLipigas.Entities.EntitieBD
{
    public class SP_LISTAR_INSTALACION_X_FILTRO_Result
    {
        public string fecha_creacion { get; set; }
        public int id_instalacion { get; set; }
        public string cod_instalacion { get; set; }
        public string nombre_instalacion { get; set; }
        public string cod_tipo_instalacion { get; set; }
        public string nombre_tipo_instalacion { get; set; }
        public bool estado { get; set; }
        public string nombre_estado { get; set; }
    }
}
