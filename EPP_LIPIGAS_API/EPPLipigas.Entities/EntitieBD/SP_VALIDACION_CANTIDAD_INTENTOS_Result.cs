﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.EntitieBD
{
   public class SP_VALIDACION_CANTIDAD_INTENTOS_Result
    {
        public string codigo { get; set; }
        public string descripcion { get; set; }
    }
}
