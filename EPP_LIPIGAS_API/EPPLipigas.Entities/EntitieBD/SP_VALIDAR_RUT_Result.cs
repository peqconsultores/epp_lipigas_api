﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPLipigas.Entities.EntitieBD
{
   public class SP_VALIDAR_RUT_Result
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }
}
