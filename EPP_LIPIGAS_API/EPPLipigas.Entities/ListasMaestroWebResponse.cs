﻿using System.Collections.Generic;

namespace EPPLipigas.Entities
{
    public class ListasMaestroWebResponse : GeneralResponse
    {

        public List<DataClasificacionEppWeb> listaDataClasificacionEppWeb { get; set; }

        public List<DataCategoriaEppWeb> listaDataCategoriaEppWeb { get; set; }

        public List<DataEstadoEntregaWeb> listaDataEstadoEntregaWeb { get; set; }
        public List<DataTipoColaboradorWeb> listaDataTipoColaboradorWeb { get; set; }
        public List<DataInstalacionesWeb> listaDataInstalacionesWeb { get; set; }
        public List<DataEmpresasWeb> listaDataEmpresaWeb { get; set; }
        public List<DataRolUsuarioWeb> listaDataRolUsuarioWeb { get; set; }
        public List<DataAreaWeb> listaDataAreasWeb { get; set; }
        public List<DataCargoWeb> listaDataCargoWeb { get; set; }
    }

    public class DataClasificacionEppWeb
    {

        public int id_nivel_1 { get; set; }
        public string cod_nivel_1 { get; set; }
        public string descripcion { get; set; }



    }
    public class DataCategoriaEppWeb
    {
        public int id_nivel_2 { get; set; }
        public string cod_nivel_2 { get; set; }
        public string categoria { get; set; }

    }

    public class DataEstadoEntregaWeb
    {

        public int id_estado_entrega { get; set; }
        public string cod_estado { get; set; }
        public string nombre_estado { get; set; }

    }

    public class DataTipoColaboradorWeb
    {

        public int id_tipo_colaborador { get; set; }
        public string cod_colaborador { get; set; }
        public string nombre_tipo_colaborador { get; set; }


    }
    public class DataInstalacionesWeb
    {
        public int id_instalacion { get; set; }
        public string cod_instalacion { get; set; }
        public string nombre_instalacion { get; set; }

    }
    public class DataEmpresasWeb
    {
        public int id_empresa { get; set; }
        public string cod_empresa { get; set; }
        public string nombre_empresa { get; set; }

    }
    public class DataRolUsuarioWeb
    {
        public int id_rol { get; set; }
        public string cod_rol { get; set; }
        public string nombre_rol { get; set; }

    }
    public class DataAreaWeb
    {
        public int id_area { get; set; }
        public string cod_area { get; set; }
        public string nombre_area { get; set; }

    }
    public class DataCargoWeb
    {

        public string cod_cargo { get; set; }
        public string nombre_cargo { get; set; }

    }
}
