﻿using EPPLipigas.Entities.Modulo;
using System.Collections.Generic;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface ModuloDO
    {
        List<Modulos> ModuloRolesXUsuarios(int id_usuario);
    }
}
