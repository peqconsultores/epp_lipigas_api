﻿
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.Epp;
using EPPLipigas.Entities.Instalacion;
using EPPLipigas.Entities.Rut;
using EPPLipigas.Entities.Usuario;
using EPPLipigas.Entities.Usuario.Request;
using EPPLipigas.Entities.Usuario.Response;
using System.Collections.Generic;
using System.Data;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IUsuarioDO
    {
        BusquedaRutResponse BusquedaRut(string rut);
        //RegistrarUsuarioResponse crearUsuario(int id_persona);
        EditarUsuarioResponse EditarUsuario(EditarUsuarioRequest request, int id_usuario_mod, int id_aplicacion_mod, int _id_persona);
        EliminarUsuarioResponse EliminarUsuario(int id_aplicacion_elim, int id_usuario_elim, int id_persona);
        ValidarExistenciaUsuarioResponse ValidarExistenciaUsuario(string rut);
        ListaInstalacionResponse ListarInstalacion();
        ListarUsuariosXFiltrosResponse ListarUsuariosXFiltros(string nombres, string rut, int num_pagina, int cant_registros, int idUsuario);
        UsuariosPorInstalacionResponse listarUsuarioxInstalacion(int idUsuario);
        ListarEppResponse listarEpp();
        ListarCargoResponse listarCargos();
        ListarClasificacionEppResponse ListarClasificaiconEpp();
        ListarCategoriaEppResponse ListarCategoriaEpp();
        ListarDatosUsuariosResponse ListarDatosUsuario(int idUsuario);
        ListarAreaResponse ListarArea();
        ListarEmpresaResponse ListarEmpresa();
        ListarTipodecolaboradoresxUsuario ListarUsuariosxTipodecolaborador();
        ObtenerDetalleUsuarioResponse ObtenerDetalleUsuario(int id_persona, string cod_aplicacion);
        SP_Listado_detalle_Acta_Entrega_Result ListadoDetalleActaEntrega(int id_entrega, int idUsuario);

        List<SP_LISTA_COLABORADORES_DETALLE_ACTA_ENTREGRA_Result> ListarColaboradresActaEntrega(int id_entrega);

        List<sp_listar_epp_colaborador_Result> ListarEPPActaEntrega(int id_entrega, int idUsuario);

        CambiarContraseñaResponse ObtenerRespuesta(int idUsuario, int id_aplicacion, string guid);
        CreacionProcesoCargaUsuariosResponse CrearProcesoCargaUsuarios(string UUIDusuario, string urldocumento, string nombreArchivo, string nombreArchivoApi);
        CreacionProcesoCargaUsuariosResponse CambiarEstadoProcesoCargaUsuariosAEnProceso(string UUIDusuario, int idProcesoCarga, int totalGlobal);
        RegistroDatosProcesoCargaUsuariosResponse RegistrarDatosUsuarios(DataTable dtCargaUsuarios, int idProcesoCarga, string UUIDusuario);
        CreacionProcesoCargaUsuariosResponse TraspasoDatosProcesoCargaUsuarios(string UUIDusuario, int idProcesoCarga);
        CreacionProcesoCargaUsuariosResponse ActualizarProcesoCargaUsuariosFinalizado(int idProcesoCargaUsuarios, string UUIDusuario);

        ListasMaestroWebResponse ListarClasificaiconEppWEB();
        ListasMaestroWebResponse ListarCategoriaEppWEB();

        ListasMaestroWebResponse ListarEstadoEntregaWEB();

        ListasMaestroWebResponse ListarTipoColaboradorWEB();
        ListasMaestroWebResponse ListarInstalacionWEB();
        ListasMaestroWebResponse ListarEmpresaWEB();
        ListasMaestroWebResponse ListarRolUsuarioWEB();
        ListasMaestroWebResponse ListarAreaWEB();
        ListasMaestroWebResponse ListarCargoWEB();
        ProcesoCargalistadoResponse listarProcesoCargo(int num_pagina, int cant_registro, string criterioBusqueda, int idUsuario);
        DetalleCargaUsuarioResponse DetalleProcesoCarga(int id_proceso_Carga, int id_usuario);
        ListaTiposErroresCargaResponse ObtenerListaTipoError();
        ListarProcesoCargaErroresResponse listarProcesoCargoErrores(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int num_pagina, int cant_registro, int id_usuario);
        ListarProcesoCargaErroresResponseExcel listarProcesoCargoErroresExcel(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int id_usuario);

      
    }
}
