﻿using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Actas;
using System.Collections.Generic;
using System.Data;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IActasDO
    {
        List<SP_SEGURIDAD_LISTAR_ACTAS_EPP_Result> ListarActasEPP(int idUsuario, string guid);
        SP_VALIDAR_ROL_Result Validar_rol(int id_usuario, string guid);
        SincronizarActaResponse SincronizarActa(SincronizarActasRequest req, int idUsuario, int id_aplicacion, in DataTable Datoscolaboradores, DataTable DatosEPP, string guid);
        AprobacionEntregaResponse AprobacionEntrega(int id_colaborador_entrega, string rut, int id_usuario, int aplicacion, string guid);
        ObtenerdatosdeEppPDFResponse DatosEppPDF(int id_colaborador_entrega, string guid);
        DatosEntregaPEFResponse DatosEntregaPDF(int id_colaborador_entrega, string guid);
        List<SP_OBTENER_DATOS_ENTREGA_PDF_Result> DatosEntregaPDF2(int id_colaborador_entrega);
        ObtenerDocumentoResponseConfirm ObtenerDatosDocumentos(int id_colaborador_entrega);
        RecepcionarEdigitalResponse Recepcion_edigital(string respuesta, string documento_hash, int codigo_respuesta);
    }
}
