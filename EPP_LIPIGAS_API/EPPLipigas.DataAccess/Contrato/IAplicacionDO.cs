﻿using EPPLipigas.Entities.Aplicacion;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IAplicacionDO
    {
        ListarParametrosResponse ListarParametros(string clientname, string clientsecret, string guid);
    }
}
