﻿using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Actas;
using EPPLipigas.Entities.Colaborador.Request;
using EPPLipigas.Entities.Colaborador.Response;
using System.Collections.Generic;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IColaboradorDO
    {
        BuscarColaboradorResponse BuscarColaborador(string rut);
        RegistrarColaboradorResponse RegistrarColaborador(RegistrarColaboradorRequest request, int id_usuario, int id_aplicacion);
        RegistrarColaboradorAppResponse RegistrarColaboradorAPP(RegistrarColaboradorAppRequest request, int id_usuario, int id_aplicacion);
        SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_Result ObtenerDetalleActaPorColaborador(int id_colaborador_entrega);
        List<SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_EPPS_Result> ListarEPPActaEntrega(int id_colaborador_entrega);
        EntregaEppColaboradorResponse ObtenerRespuestaEstadoColaboradorEntrega(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid);
        ObtenerGuidAprobacion ObtenerGuiddeAprobacion(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid);
        SP_OBTENER_DATOS_ENTREGA_COLABORADOR_X_GUID_Result ObtenerDatosEntregaxGuid(string guid_entregado);
        List<SP_LISTAR_DATOS_EPP_X_GUID_Result> ObtenerDatosEEPEntregaxGuid(string guid_entregado);
        AprobarxWebResponse ObtenerRespuestaAprobarxWeb(string guid_entregado, string guid_aprobacion_transaccion);
        ReporteColaboradoresResponse Listarcolaboradoresxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario);
        ReporteColaboradorExcelResponse ListarcolaboradoresExcelxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int idUsuario);
        SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result GuardarDatosDocumento(string documento, int id_usuario, int id_aplicacion, int id_colaborador_entrega, string guid);
        ObtenerDatosColaboradorResponse obtenerDatosColaborador(string rut);
        EntregaEppColaboradorIDResponse ObtenerDatosColaboradorPorId(int id_Colaborador_entrega, int id_usuario, string guid);
        DatosEntregaPEFResponse DatosEntregaPDF(int id_colaborador_entrega, string guid);
        ObtenerDocumentWebResponse ObtenerDatosDocumentosWeb(string gui_entregado);
        ActualizarColaboradorResponse ActualizarColaborador(int id_colaborador_entrega, string genero, string correo, string fecha_nacimiento, int id_aplicacion, int id_usuario, string guid);
    }
}
