﻿using EPPLipigas.Entities.EDigital.Response;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IApiEDigitalDO

    {
        string tokenAutenticacion { get; set; }
        GetTokenResponse GetToken();

        GetValidUsuarioResponse GetValidUsuario(string rut, string tipo_documento, string token, string guid);
        CrearUsuarioResponse CrearUsuario(string nombres, string apellidos, string rut, string tipo_documento, string correo, string genero, string fecha_nacimiento, string celular, string tipo_usuario, string token);
        CrearDocumentoResponse CrearDocumento(string rut_colaborador, string description, string template_id, string rut_responsable, object metadata,
            string document_file, string document_b64, string document_link, object settings,
            string token, string guid);
        ObtenerDocumentoResponse ObtenerDocumento(string document_hash, string token);
        ObtenerDocumentoResponse DescargarDocumento(string document_hash, string token);
    }

}
