﻿using EPPLipigas.Entities.Instalacion.Response;

namespace EPPLipigas.DataAccess.Contrato
{
    public interface IInstalacionDO
    {
        CrearInstalacionResponse CrearInstalacion(int aplicacion, int usuario, string nombre_instalacion, string tipo_instalacion);
        EliminarInstalacionResponse EliminarInstalacion(int aplicacion, int usuario, int id_instalacion);
        EditarInstalacionResponse EditarInstalacion(int aplicacion, int usuario, int id_instalacion, string cod_tipo_instalacion, string nombre_instalacion, bool activo);
        DetalleInstalacionResponse DetalleInstalacion(int id_instalacion);
        ListarInstalacionXFiltroResponse ListarInstalacionXFiltros(string instalacion, string cod_tipo_instalacion, string estado, int num_pagina, int cant_registros, int idUsuario);
        DatosTipoInstalacionResponse ObtenerTipoInstalacion();
    }
}
