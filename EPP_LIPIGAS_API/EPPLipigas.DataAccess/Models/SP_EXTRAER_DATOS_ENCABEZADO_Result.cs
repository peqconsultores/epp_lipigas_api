//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EPPLipigas.DataAccess.Models
{
    using System;
    
    public partial class SP_EXTRAER_DATOS_ENCABEZADO_Result
    {
        public string nombre_instalacion { get; set; }
        public string cod_entrega { get; set; }
        public Nullable<System.DateTime> fecha_entrega { get; set; }
        public string nombres { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
    }
}
