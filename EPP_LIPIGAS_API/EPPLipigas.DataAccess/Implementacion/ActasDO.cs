﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Actas;
using EPPLipigas.Entities.EntitieBD;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class ActasDO : IActasDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActasDO));
        public List<SP_SEGURIDAD_LISTAR_ACTAS_EPP_Result> ListarActasEPP(int idUsuario, string guid)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_SEGURIDAD_LISTAR_ACTAS_EPP(idUsuario).ToList();



                return datosBusqueda;

            }

            catch (Exception e)
            {
                log.Error($"ActasDO   ({guid})-> IdUsuario:{idUsuario}  ListarActasEPP. Mensaje al cliente: Error interno al listar Actas EPP. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new List<SP_SEGURIDAD_LISTAR_ACTAS_EPP_Result>();
            }
        }

        public SP_VALIDAR_ROL_Result Validar_rol(int id_usuario, string guid)
        {

            try
            {
                var ctx = new EPPEntities();
                var ValidarRol = ctx.SP_VALIDAR_ROL(id_usuario).FirstOrDefault();
                if (ValidarRol != null)
                {
                    ValidarRol.decripcion = "No se pudo traer los datos ";


                }

                return ValidarRol;


            }
            catch (Exception e)
            {

                log.Error($"ActasDO   ({guid})-> IdUsuario:{id_usuario} -> Validar_rol. Mensaje al cliente: Error interno al valdiar rol. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new SP_VALIDAR_ROL_Result();
            }


        }

        public SincronizarActaResponse SincronizarActa(SincronizarActasRequest req, int idUsuario, int id_aplicacion, in DataTable Datoscolaboradores, DataTable DatosEPP, string guid)
        {

            try
            {
                var ctx = new EPPEntities();
                var id_instalacionParameter = new SqlParameter("id_instalacion", (object)req.id_instalacion ?? DBNull.Value);
                var id_usuarioParameter = new SqlParameter("id_usuario", (object)idUsuario ?? DBNull.Value);
                var id_aplicacionParameter = new SqlParameter("id_aplicacion", (object)id_aplicacion ?? DBNull.Value);
                var fechaParameter = new SqlParameter("fecha", (object)req.fecha ?? DBNull.Value);
                var DatoscolaboradoresParameter = new SqlParameter
                {
                    ParameterName = "@tblcolaborador_entrega",
                    SqlDbType = SqlDbType.Structured,
                    Value = Datoscolaboradores,
                    TypeName = "type_colaborador_entrega"


                };
                var DatosEPPParameter = new SqlParameter
                {
                    ParameterName = "@tblcolaborador_entrega_epp",
                    SqlDbType = SqlDbType.Structured,
                    Value = DatosEPP,
                    TypeName = "type_colaborador_entrega_epp"


                };


                var SincronizarActa = ctx.Database.SqlQuery<SP_SINCRONIZAR_ACTA_Resultibd>(
                    "EXEC SP_SINCRONIZAR_ACTA @id_instalacion," +
                    "@id_usuario,@id_aplicacion,@fecha," +
                    "@tblcolaborador_entrega,@tblcolaborador_entrega_epp",
                    id_instalacionParameter,
                    id_usuarioParameter,
                    id_aplicacionParameter,
                    fechaParameter,
                    DatoscolaboradoresParameter,
                    DatosEPPParameter).FirstOrDefault();

                if (SincronizarActa != null)
                {
                    if (SincronizarActa.codigo == 200)
                    {
                        return new SincronizarActaResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = SincronizarActa.descripcion

                        };
                    }
                    else if (SincronizarActa.codigo == 400)
                    {
                        return new SincronizarActaResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = SincronizarActa.descripcion
                        };
                    }
                    else if (SincronizarActa.codigo == 404)
                    {
                        return new SincronizarActaResponse()
                        {
                            codigoRes = HttpStatusCode.NotFound,
                            mensajeRes = SincronizarActa.descripcion
                        };
                    }
                    else if (SincronizarActa.codigo == 500)
                    {
                        return new SincronizarActaResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = SincronizarActa.descripcion
                        };
                    }
                    else
                    {
                        return new SincronizarActaResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "Error al crear la nueva acta."
                        };
                    }


                }
                else
                {
                    return new SincronizarActaResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se pudo obtener los datos de la nueva acta"

                    };
                }

            }

            catch (Exception e)
            {
                log.Error($"ActasDO  ({guid}) ->  CrearActa. Mensaje al cliente: Error interno  " +
                       "Detalle error: " + JsonConvert.SerializeObject(e));
                return new SincronizarActaResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al validar la creación de la acta."
                };



            }

        }

        public AprobacionEntregaResponse AprobacionEntrega(int id_colaborador_entrega, string rut, int id_usuario, int aplicacion, string guid)
        {
            try
            {
                var ctx = new EPPEntities();
                var RespuestaValidacion = ctx.SP_APROBACION_ENTREGA(id_colaborador_entrega, rut, id_usuario, aplicacion).FirstOrDefault();
                if (RespuestaValidacion.codigo == 200)
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = RespuestaValidacion.descripcion
                    };
                }
                else if (RespuestaValidacion.codigo == 404)
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = RespuestaValidacion.descripcion
                    };
                }
                else
                {
                    return new AprobacionEntregaResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = RespuestaValidacion.descripcion
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"ActasDO ->({guid}) ->({id_usuario})  AprobacionEntrega. Mensaje al cliente: Error interno al obtener Respuesta Estado Colaborador Entrega. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new AprobacionEntregaResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener Respuesta Estado Colaborador Entrega."
                };

            }


        }

        public ObtenerdatosdeEppPDFResponse DatosEppPDF(int id_colaborador_entrega, string guid)
        {
            var response = new ObtenerdatosdeEppPDFResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_LISTAR_EPP_COLABORADOR_PDF(id_colaborador_entrega).ToList();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_EPP_COLABORADOR_PDF_Result, DataEppPDF>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_EPP_COLABORADOR_PDF_Result>, List<DataEppPDF>>(datosBusqueda);

                    return new ObtenerdatosdeEppPDFResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new ObtenerdatosdeEppPDFResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"ActasDO ({guid}) ->  DatosEppPDF. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle de los Epp";
            }
            return response;
        }

        public DatosEntregaPEFResponse DatosEntregaPDF(int id_colaborador_entrega, string guid)
        {
            var response = new DatosEntregaPEFResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DATOS_ENTREGA_PDF(id_colaborador_entrega).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DATOS_ENTREGA_PDF_Result, datosEntrega>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DATOS_ENTREGA_PDF_Result, datosEntrega>(datosBusqueda);

                    return new DatosEntregaPEFResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new DatosEntregaPEFResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"ActasDO ({guid}) ->  DatosEppPDF. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle de los Epp";
            }
            return response;
        }

        public List<SP_OBTENER_DATOS_ENTREGA_PDF_Result> DatosEntregaPDF2(int id_colaborador_entrega)
        {
            var response = new DatosEntregaPEFResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DATOS_ENTREGA_PDF(id_colaborador_entrega).ToList();
                return datosBusqueda;
            }
            catch (Exception e)
            {
                log.Error("ActasDO ->  DatosEppPDF. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                return new List<SP_OBTENER_DATOS_ENTREGA_PDF_Result>();
            }

        }

        public ObtenerDocumentoResponseConfirm ObtenerDatosDocumentos(int id_colaborador_entrega)
        {
            var response = new ObtenerDocumentoResponseConfirm();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR(id_colaborador_entrega).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR_Result, datosDocumento>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR_Result, datosDocumento>(datosBusqueda);

                    return new ObtenerDocumentoResponseConfirm()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new ObtenerDocumentoResponseConfirm()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("ActasDO ->  ObtenerDatosDocumentos. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle del documento";
            }
            return response;
        }


        public RecepcionarEdigitalResponse Recepcion_edigital(string respuesta, string documento_hash, int codigo_respuesta)
        {
            var response = new RecepcionarEdigitalResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_APROBAR_FIRMA_E_DIGITAL(respuesta, documento_hash, codigo_respuesta).FirstOrDefault();
                if (datosBusqueda != null)
                {


                    return new RecepcionarEdigitalResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",

                    };
                }
                else
                {
                    return new RecepcionarEdigitalResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("ActasDO ->  AprobarFirmaDigital. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al aprobar el documento";
            }
            return response;
        }

    }
}
