﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Aplicacion;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class AplicacionDO : IAplicacionDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AplicacionDO));
        public ListarParametrosResponse ListarParametros(string clientname, string clientsecret, string guid)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_SEGURIDAD_LISTAR_PARAMETROS(clientname, clientsecret).ToList();
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_SEGURIDAD_LISTAR_PARAMETROS_Result, ListarParametrosDatosResponse>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_SEGURIDAD_LISTAR_PARAMETROS_Result>, List<ListarParametrosDatosResponse>>(datosBusqueda);

                    return new ListarParametrosResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados.ToList()
                    };
                }
                else
                {
                    return new ListarParametrosResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        datos = new List<ListarParametrosDatosResponse>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"AplicacionDO ->  ({guid}) ListarParametros. Mensaje al cliente: Error interno al listar los parametros para la aplicación. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarParametrosResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar los parametros."
                };
            }
        }
    }
}
