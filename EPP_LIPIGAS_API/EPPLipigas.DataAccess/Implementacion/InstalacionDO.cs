﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.EntitieBD;
using EPPLipigas.Entities.Instalacion.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;


namespace EPPLipigas.DataAccess.Implementacion
{
    public class InstalacionDO : IInstalacionDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InstalacionDO));
        public CrearInstalacionResponse CrearInstalacion(int aplicacion, int usuario, string nombre_instalacion, string cod_tipo_instalacion)
        {
            var response = new CrearInstalacionResponse();
            try
            {

                var ctx = new EPPEntities();
                var respuesta = ctx.SP_CREAR_INSTALACION(aplicacion, usuario, nombre_instalacion, cod_tipo_instalacion).FirstOrDefault();

                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new CrearInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else if (respuesta.codigo == 400)
                    {
                        return new CrearInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new CrearInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                }
                else
                {

                    return new CrearInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  CrearInstalacion. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al crear la instalacion";
            }
            return response;
        }
        public EliminarInstalacionResponse EliminarInstalacion(int aplicacion, int usuario, int id_instalacion)
        {
            var response = new EliminarInstalacionResponse();
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_ELIMINAR_INSTALACION(usuario, aplicacion, id_instalacion).FirstOrDefault();

                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new EliminarInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new EliminarInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                }
                else
                {
                    return new EliminarInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  EliminarInstalacion. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al eliminar instalalcion";
            }
            return response;
        }
        public EditarInstalacionResponse EditarInstalacion(int aplicacion, int usuario, int id_instalacion, string cod_tipo_instalacion, string nombre_instalacion, bool activo)
        {
            var response = new EditarInstalacionResponse();
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_EDITAR_INSTALACION(usuario, aplicacion, id_instalacion, cod_tipo_instalacion, nombre_instalacion, activo).FirstOrDefault();

                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new EditarInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new EditarInstalacionResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                }
                else
                {
                    return new EditarInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  EditarInstalacion. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al editar instalacion";
            }
            return response;
        }
        public DetalleInstalacionResponse DetalleInstalacion(int id_instalacion)
        {
            var response = new DetalleInstalacionResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DETALLE_INSTALACION(id_instalacion).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DETALLE_INSTALACION_Result, DataInstalacion>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DETALLE_INSTALACION_Result, DataInstalacion>(datosBusqueda);

                    return new DetalleInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new DetalleInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  DetalleInstalacion. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle de la instalacion";
            }
            return response;
        }
        public ListarInstalacionXFiltroResponse ListarInstalacionXFiltros(string nombre_instalacion, string cod_tipo_instalacion, string estado, int num_pagina, int cant_registro, int idUsuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var nombre_instalacionParameter = new SqlParameter("nombre_instalacion", (object)nombre_instalacion ?? DBNull.Value);
                var cod_tipo_instalacionParameter = new SqlParameter("cod_tipo_instalacion", (object)cod_tipo_instalacion ?? DBNull.Value);
                var estadoParameter = new SqlParameter("estado", (object)estado ?? DBNull.Value);
                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var datosBusqueda = ctx.Database.SqlQuery<SP_LISTAR_INSTALACION_X_FILTRO_Result>("EXEC SP_LISTAR_INSTALACION_X_FILTRO " +
                "@nombre_instalacion, @cod_tipo_instalacion,@estado, @num_pagina, @cant_registro, @idUsuario, @total_registros OUT",
                nombre_instalacionParameter, cod_tipo_instalacionParameter, estadoParameter, num_paginaParameter, cant_registroParameter, idUsuarioParameter, total_registrosParameter).ToList();

                int total_registros = Convert.ToInt32(total_registrosParameter.Value);

                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_INSTALACION_X_FILTRO_Result, ListarInstalacionXFiltroResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_INSTALACION_X_FILTRO_Result>, List<ListarInstalacionXFiltroResponse_Data>>(datosBusqueda);

                    return new ListarInstalacionXFiltroResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ListarInstalacionXFiltroResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos de los epps.",
                        data = new List<ListarInstalacionXFiltroResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  ListarInstalacionXFiltros. Mensaje al cliente: Error interno al listar instalaciones por filtros. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarInstalacionXFiltroResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar instalaciones por filtros."
                };
            }




        }
        public DatosTipoInstalacionResponse ObtenerTipoInstalacion()
        {
            var response = new DatosTipoInstalacionResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_PARAMETROS_INSTALACION_OBTENER_TIPO_INSTALACION().ToList();
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_PARAMETROS_INSTALACION_OBTENER_TIPO_INSTALACION_Result, DatosTipoInstalacion>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_PARAMETROS_INSTALACION_OBTENER_TIPO_INSTALACION_Result>, List<DatosTipoInstalacion>>(datosBusqueda);

                    return new DatosTipoInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new DatosTipoInstalacionResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("InstalacionDO ->  ObtenerTipoInstalacion. Mensaje al cliente: Error interno al obtener listado de tipos de instalacion" +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener listado de tipos de instalacion";
            }
            return response;
        }
    }




}
