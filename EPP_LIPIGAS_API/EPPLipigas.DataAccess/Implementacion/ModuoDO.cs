﻿using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Modulo;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class ModuoDO : ModuloDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ModuoDO));

        public List<Modulos> ModuloRolesXUsuarios(int id_usuario)
        {
            var response = new List<Modulos>();
            var ctx = new EPPEntities();
            var listaOpciones = ctx.SP_SEGURIDAD_MODULO_GET_MODULO_X_ROL_USUARIO(id_usuario).ToList();
            var nivelMaxSubModulos = listaOpciones.Max(x => x.nivel);
            nivelMaxSubModulos = nivelMaxSubModulos == null ? 1 : nivelMaxSubModulos - 1;
            var listaModulosPadre = listaOpciones.Where(x => x.codModuloPadre == null).Select(x => new Modulos()
            {
                codModulo = x.codModulo,
                orden = x.orden.GetValueOrDefault(),
                dModulo = x.dModulo,
                icon = x.icon,
                nivelMaxSubModulos = nivelMaxSubModulos.GetValueOrDefault()
            }).OrderBy(x => x.orden).ToList();

            foreach (var moduloPadre in listaModulosPadre)
            {
                var listaSubmodulos = listaOpciones.Where(x => x.codModuloPadre == moduloPadre.codModulo).Select(x => new submodulo()
                {
                    codModulo = x.codModulo,
                    dModulo = x.dModulo,
                    controller = x.controller,
                    actionName = x.actionName,
                    ruta = x.ruta,
                    icon = x.icon,
                    orden = x.orden.GetValueOrDefault(),
                    nivel = x.nivel.GetValueOrDefault()
                }).OrderBy(x => x.orden).ToList();

                if (listaSubmodulos != null && listaSubmodulos.Count > 0)
                {
                    moduloPadre.listasubmodulos = listaSubmodulos;
                }
            }
            if (listaModulosPadre != null && listaModulosPadre.Count > 0)
            {
                response = listaModulosPadre;
            }

            return response;

        }
    }
}
