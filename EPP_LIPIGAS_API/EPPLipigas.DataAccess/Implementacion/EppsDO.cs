﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.EntitieBD;
using EPPLipigas.Entities.Epp.Request;
using EPPLipigas.Entities.Epp.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class EppsDO : IEppsDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InstalacionDO));

        public ListarEppsXFiltroResponse ListarEppsXFiltros(string categoria, string clasificacion, string epp, string estado, int num_pagina, int cant_registro, int idUsuario)
        {

            try
            {

                var ctx = new EPPEntities();
                var categoriaParameter = new SqlParameter("categoria", (object)categoria ?? DBNull.Value);
                var clasificacionParameter = new SqlParameter("clasificacion", (object)clasificacion ?? DBNull.Value);
                var eppParameter = new SqlParameter("epp", (object)epp ?? DBNull.Value);
                var estadoParameter = new SqlParameter("estado", (object)estado ?? DBNull.Value);
                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var datosBusqueda = ctx.Database.SqlQuery<SP_LISTAR_EPP_X_FILTRO_Result>("EXEC SP_LISTAR_EPP_X_FILTRO " +
                 "@categoria, @clasificacion,@epp,@estado,@num_pagina, @cant_registro, @idUsuario, @total_registros OUT",
                 categoriaParameter, clasificacionParameter, eppParameter, estadoParameter, num_paginaParameter,
                 cant_registroParameter, idUsuarioParameter, total_registrosParameter).ToList();
                int total_registros = Convert.ToInt32(total_registrosParameter.Value);
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_EPP_X_FILTRO_Result, ListarEppXFiltroResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_EPP_X_FILTRO_Result>, List<ListarEppXFiltroResponse_Data>>(datosBusqueda);

                    return new ListarEppsXFiltroResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ListarEppsXFiltroResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarEppXFiltroResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("IEppsDO ->  ListarInstalacionXFiltros. Mensaje al cliente: Error interno al listar Epps por filtros. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarEppsXFiltroResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar Epps por filtros."
                };
            }
        }

        public RegistrarEppsResponse RegistrarEpps(string cod_clasificacion, string cod_categoria, int aplicacion, int usuario, string epp)
        {

            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_REGISTRAR_EPP(cod_clasificacion, cod_categoria, aplicacion, usuario, epp).FirstOrDefault();
                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new RegistrarEppsResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else if (respuesta.codigo == 400)
                    {
                        return new RegistrarEppsResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new RegistrarEppsResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion
                        };

                    }

                }
                else
                {
                    return new RegistrarEppsResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("IEppsDO ->  ListarInstalacionXFiltros. Mensaje al cliente: Error interno al registrar Epps . " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new RegistrarEppsResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al registrar Epps ."
                };
            }


        }
        public EliminarEppResponse EliminarEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion)
        {
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_ELIMINAR_EPP(idEquipoProteccionPersonal, id_usuario.ToString(), cod_aplicacion).FirstOrDefault();
                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new EliminarEppResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else if (respuesta.codigo == 400)
                    {
                        return new EliminarEppResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new EliminarEppResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                }
                else
                {
                    return new EliminarEppResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "No se obtuvo respuesta al eliminar epp"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("EppsDO ->  EliminarEpp. Mensaje al cliente: Error interno al eliminar epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new EliminarEppResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al eliminar Epps ."
                };
            }
        }
        public EditarEppResponse EditarEpp(int idEquipoProteccionPersonal, EditarEppRequest request, int id_usuario, string cod_aplicacion)
        {
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_EDITAR_EPP(idEquipoProteccionPersonal, request.nombreEpp, request.codNivel1, request.codNivel2, request.codNivel3, request.estado, id_usuario.ToString(), cod_aplicacion).FirstOrDefault();
                if (respuesta != null)
                {
                    if (respuesta.codigo == 200)
                    {
                        return new EditarEppResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else if (respuesta.codigo == 400)
                    {
                        return new EditarEppResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                    else
                    {
                        return new EditarEppResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion
                        };
                    }
                }
                else
                {
                    return new EditarEppResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "No se obtuvo respuesta al editar epp"
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("EppsDO ->  EditarEpp. Mensaje al cliente: Error interno al editar epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new EditarEppResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al editar epp."
                };
            }
        }
        public ObtenerDetalleEppResponse ObtenerDetalleEpp(int idEquipoProteccionPersonal, int id_usuario, string cod_aplicacion)
        {
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_OBTENER_DETALLE_EPP(idEquipoProteccionPersonal, id_usuario.ToString(), cod_aplicacion).FirstOrDefault();
                if (respuesta != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DETALLE_EPP_Result, DetalleEpp>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DETALLE_EPP_Result, DetalleEpp>(respuesta);

                    return new ObtenerDetalleEppResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new ObtenerDetalleEppResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("EppsDO ->  ObtenerDetalleEpp. Mensaje al cliente: Error interno al obtener detalle epp. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ObtenerDetalleEppResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener detalle epp."
                };
            }
        }

        public ReporteEppsResponse ListarEppsxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var fechaDesdeParameter = new SqlParameter("fechaDesde", (object)fechaDesde ?? DBNull.Value);
                var fechaHastaParameter = new SqlParameter("fechaHasta", (object)fechaHasta ?? DBNull.Value);
                var clasificacionParameter = new SqlParameter("clasificacion", (object)clasificacion ?? DBNull.Value);
                var categoriaParameter = new SqlParameter("categoria ", (object)categoria ?? DBNull.Value);
                var nombre_producto_eppParameter = new SqlParameter("nombre_producto_epp", (object)nombre_producto_epp ?? DBNull.Value);
                var rut_colaboradorParameter = new SqlParameter("rut_colaborador", (object)rut_colaborador ?? DBNull.Value);
                var tipo_colaboradorParameter = new SqlParameter("tipo_colaborador", (object)tipo_colaborador ?? DBNull.Value);
                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                var datosBusqueda = ctx.Database.SqlQuery<SP_BUSCAR_REPORTE_EPP_Result>("EXEC SP_BUSCAR_REPORTE_EPP " +
               "@fechaDesde, @fechaHasta,@clasificacion,@categoria,@nombre_producto_epp,@rut_colaborador,@tipo_colaborador, @num_pagina, @cant_registro, @idUsuario, @total_registros OUT",
               fechaDesdeParameter, fechaHastaParameter, clasificacionParameter, categoriaParameter, nombre_producto_eppParameter, rut_colaboradorParameter, tipo_colaboradorParameter, num_paginaParameter, cant_registroParameter, idUsuarioParameter, total_registrosParameter).ToList();
                int total_registros = Convert.ToInt32(total_registrosParameter.Value);
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_BUSCAR_REPORTE_EPP_Result, ListarReporteEppsXFiltroResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_BUSCAR_REPORTE_EPP_Result>, List<ListarReporteEppsXFiltroResponse_Data>>(datosBusqueda);

                    return new ReporteEppsResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ReporteEppsResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarReporteEppsXFiltroResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("EppsDO ->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno al listar reportes de epps. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ReporteEppsResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar reportes de epps."
                };
            }
        }

        public ReportEppExcelResponse ListarEppsExcelxfiltro(string fechaDesde, string fechaHasta, string clasificacion, string categoria, string nombre_producto_epp, string rut_colaborador, string tipo_colaborador, int idUsuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var fechaDesdeParameter = new SqlParameter("fechaDesde", (object)fechaDesde ?? DBNull.Value);
                var fechaHastaParameter = new SqlParameter("fechaHasta", (object)fechaHasta ?? DBNull.Value);
                var clasificacionParameter = new SqlParameter("clasificacion", (object)clasificacion ?? DBNull.Value);
                var categoriaParameter = new SqlParameter("categoria ", (object)categoria ?? DBNull.Value);
                var nombre_producto_eppParameter = new SqlParameter("nombre_producto_epp", (object)nombre_producto_epp ?? DBNull.Value);
                var rut_colaboradorParameter = new SqlParameter("rut_colaborador", (object)rut_colaborador ?? DBNull.Value);
                var tipo_colaboradorParameter = new SqlParameter("tipo_colaborador", (object)tipo_colaborador ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);

                var datosBusqueda = ctx.Database.SqlQuery<SP_BUSCAR_REPORTE_EPP_EXCEL_Result>("EXEC SP_BUSCAR_REPORTE_EPP_EXCEL " +
               "@fechaDesde, @fechaHasta,@clasificacion,@categoria,@nombre_producto_epp,@rut_colaborador,@tipo_colaborador, @idUsuario",
               fechaDesdeParameter, fechaHastaParameter, clasificacionParameter, categoriaParameter, nombre_producto_eppParameter, rut_colaboradorParameter, tipo_colaboradorParameter, idUsuarioParameter).ToList();

                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_BUSCAR_REPORTE_EPP_EXCEL_Result, ListarReporteEppsXFiltroExcelResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_BUSCAR_REPORTE_EPP_EXCEL_Result>, List<ListarReporteEppsXFiltroExcelResponse_Data>>(datosBusqueda);

                    return new ReportEppExcelResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        data = datosMapeados.ToList()
                    };
                }
                else
                {
                    return new ReportEppExcelResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarReporteEppsXFiltroExcelResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("EppsDO ->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno al listar reportes de epps. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ReportEppExcelResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar reportes de epps."
                };
            }
        }
    }
}
