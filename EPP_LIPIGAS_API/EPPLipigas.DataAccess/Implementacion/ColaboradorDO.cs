﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities.Actas;
using EPPLipigas.Entities.Colaborador.Request;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.EntitieBD;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class ColaboradorDO : IColaboradorDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActasDO));
        public RegistrarColaboradorResponse RegistrarColaborador(RegistrarColaboradorRequest request, int id_usuario, int id_aplicacion)
        {
            try
            {


                var ctx = new EPPEntities();
                var id_usuarioParameter = new SqlParameter("id_usuario", (object)id_usuario ?? DBNull.Value);
                var id_aplicacionParameter = new SqlParameter("id_aplicacion", (object)id_aplicacion ?? DBNull.Value);
                var rutParameter = new SqlParameter("rut", (object)request.rut ?? DBNull.Value);
                var nombresParameter = new SqlParameter("nombres", (object)request.nombres ?? DBNull.Value);
                var apellido_paternoParameter = new SqlParameter("apellido_paterno", (object)request.apellido_paterno ?? DBNull.Value);
                var apellido_maternoParameter = new SqlParameter("apellido_materno", (object)request.apellido_materno ?? DBNull.Value);
                var cod_instalacionParameter = new SqlParameter("cod_instalacion", (object)request.cod_instalacion ?? DBNull.Value);
                var cod_rolParameter = new SqlParameter("cod_rol", (object)request.cod_rol ?? DBNull.Value);
                var cod_tipo_colaboradorParameter = new SqlParameter("cod_tipo_colaborador", (object)request.cod_tipo_colaborador ?? DBNull.Value);
                var nombre_empresaParameter = new SqlParameter("nombre_empresa", (object)request.nombre_empresa ?? DBNull.Value);
                var areaParameter = new SqlParameter("area", (object)request.area ?? DBNull.Value);
                var correoParameter = new SqlParameter("correo", (object)request.correo ?? DBNull.Value);
                var celularParameter = new SqlParameter("celular", (object)request.celular ?? DBNull.Value);
                var cod_cargoParameter = new SqlParameter("cod_cargo", (object)request.cod_cargo ?? DBNull.Value);
                var fecha_nacimientoParameter = new SqlParameter("fecha_nacimiento", (object)request.fecha_nacimiento ?? DBNull.Value);
                var generoParameter = new SqlParameter("genero", (object)request.genero ?? DBNull.Value);


                var datosRegistro = ctx.Database.SqlQuery<SP_SEGURIDAD_REGISTRA_PERSONA_COLABORADOR_Result>(
                    "EXEC SP_SEGURIDAD_REGISTRA_PERSONA_COLABORADOR @id_usuario," +
                    "@id_aplicacion, @rut, @nombres, @apellido_paterno, " +
                    "@apellido_materno, @cod_instalacion, @cod_rol, @cod_tipo_colaborador," +
                    "@nombre_empresa, @area, @correo, @celular, @cod_cargo , @fecha_nacimiento, @genero",
                    id_usuarioParameter, id_aplicacionParameter, rutParameter, nombresParameter, apellido_paternoParameter,
                    apellido_maternoParameter, cod_instalacionParameter, cod_rolParameter, cod_tipo_colaboradorParameter, nombre_empresaParameter,
                    areaParameter, correoParameter, celularParameter, cod_cargoParameter, fecha_nacimientoParameter, generoParameter).FirstOrDefault();

                if (datosRegistro != null)
                {
                    if (datosRegistro.codigo == 201)
                    {
                        return new RegistrarColaboradorResponse()
                        {
                            codigoRes = HttpStatusCode.Created,
                            mensajeRes = datosRegistro.descripcion,
                            id_persona = (int)datosRegistro.id_persona
                        };
                    }
                    else
                    {
                        if (datosRegistro.codigo == 400)
                        {
                            return new RegistrarColaboradorResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 401)
                        {
                            return new RegistrarColaboradorResponse()
                            {
                                codigoRes = HttpStatusCode.Unauthorized,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 404)
                        {
                            return new RegistrarColaboradorResponse()
                            {
                                codigoRes = HttpStatusCode.NotFound,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else
                        {
                            return new RegistrarColaboradorResponse()
                            {
                                codigoRes = HttpStatusCode.InternalServerError,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                    }
                }
                else
                {
                    return new RegistrarColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvo respuesta al registrar al colaborador."
                    };
                }
            }

            catch (Exception e)
            {

                log.Error($"ColaboradorDO ->()  RegistrarColaborador. Mensaje al cliente: Error interno al crear Colaborador. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new RegistrarColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al registrar al colaborador."
                };
            }
        }

        public RegistrarColaboradorAppResponse RegistrarColaboradorAPP(RegistrarColaboradorAppRequest request, int id_usuario, int id_aplicacion)
        {
            try
            {
                var ctx = new EPPEntities();
                var id_usuarioParameter = new SqlParameter("id_usuario", (object)id_usuario ?? DBNull.Value);
                var id_aplicacionParameter = new SqlParameter("id_aplicacion", (object)id_aplicacion ?? DBNull.Value);
                var rutParameter = new SqlParameter("rut", (object)request.rut ?? DBNull.Value);
                var nombresParameter = new SqlParameter("nombres", (object)request.nombres ?? DBNull.Value);
                var apellido_paternoParameter = new SqlParameter("apellido_paterno", (object)request.apellido_paterno ?? DBNull.Value);
                var apellido_maternoParameter = new SqlParameter("apellido_materno", (object)request.apellido_materno ?? DBNull.Value);
                var cod_instalacionParameter = new SqlParameter("cod_instalacion", (object)request.cod_instalacion ?? DBNull.Value);
                var cod_rolParameter = new SqlParameter("cod_rol", (object)request.cod_rol ?? DBNull.Value);
                var cod_tipo_colaboradorParameter = new SqlParameter("cod_tipo_colaborador", (object)request.cod_tipo_colaborador ?? DBNull.Value);
                var cod_empresaParameter = new SqlParameter("cod_empresa", (object)request.cod_empresa ?? DBNull.Value);
                var nombre_empresaParameter = new SqlParameter("nombre_empresa", (object)request.nombre_empresa ?? DBNull.Value);
                var cod_areaParameter = new SqlParameter("cod_area", (object)request.cod_area ?? DBNull.Value);
                var nombre_areaParameter = new SqlParameter("nombre_area", (object)request.area ?? DBNull.Value);
                var correoParameter = new SqlParameter("correo", (object)request.correo ?? DBNull.Value);
                var celularParameter = new SqlParameter("celular", (object)request.celular ?? DBNull.Value);
                var cod_cargoParameter = new SqlParameter("cod_cargo", (object)request.cod_cargo ?? DBNull.Value);
                var fecha_nacimientoParameter = new SqlParameter("fecha_nacimiento", (object)request.fecha_nacimiento ?? DBNull.Value);
                var generoParameter = new SqlParameter("genero", (object)request.genero ?? DBNull.Value);


                var datosRegistro = ctx.Database.SqlQuery<SP_SEGURIDAD_REGISTRA_PERSONA_COLABORADOR_APP_Result>(
                    "EXEC SP_SEGURIDAD_REGISTRA_PERSONA_COLABORADOR_APP  @id_usuario," +
                    "@id_aplicacion, @rut, @nombres, @apellido_paterno, " +
                    "@apellido_materno, @cod_instalacion, @cod_rol, @cod_tipo_colaborador," +
                    "@cod_empresa,@nombre_empresa,@cod_area,@nombre_area, @correo, @celular, @cod_cargo, @fecha_nacimiento, @genero",
                    id_usuarioParameter, id_aplicacionParameter, rutParameter, nombresParameter, apellido_paternoParameter,
                    apellido_maternoParameter, cod_instalacionParameter, cod_rolParameter, cod_tipo_colaboradorParameter, cod_empresaParameter, nombre_empresaParameter,
                    cod_areaParameter, nombre_areaParameter, correoParameter, celularParameter, cod_cargoParameter, fecha_nacimientoParameter, generoParameter).FirstOrDefault();

                if (datosRegistro != null)
                {
                    if (datosRegistro.codigo == 201)
                    {
                        return new RegistrarColaboradorAppResponse()
                        {
                            codigoRes = HttpStatusCode.Created,
                            mensajeRes = datosRegistro.descripcion,
                            id_persona = (int)datosRegistro.id_persona
                        };
                    }
                    else
                    {
                        if (datosRegistro.codigo == 400)
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 401)
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.Unauthorized,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 404)
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.NotFound,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else
                        {
                            return new RegistrarColaboradorAppResponse()
                            {
                                codigoRes = HttpStatusCode.InternalServerError,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                    }
                }
                else
                {
                    return new RegistrarColaboradorAppResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvo respuesta al registrar al colaborador."
                    };
                }
            }


            catch (Exception e)
            {
                log.Error($"ColaboradorDO ->()  RegistrarColaborador. Mensaje al cliente: Error interno al crear Colaborador. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new RegistrarColaboradorAppResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al registrar al colaborador."
                };
            }
        }

        public BuscarColaboradorResponse BuscarColaborador(string rut)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_SEGURIDAD_VALIDAR_EXISTENCIA_COLABORADOR(rut).FirstOrDefault();

                return new BuscarColaboradorResponse()
                {
                    codigoRes = (System.Net.HttpStatusCode)datosBusqueda.codigo,
                    mensajeRes = datosBusqueda.descripcion,
                    flagEncontrado = (bool)datosBusqueda.flag_encontrado,
                    data = new BuscarColaboradorResponse_Data()
                    {
                        nombre_colaborador = datosBusqueda.nombre_colaborador,
                        nombre_instalacion = datosBusqueda.nombre_instalacion
                    }
                };
            }
            catch (Exception e)
            {
                log.Error("ActasDO ->  ListarActasEPP. Mensaje al cliente: Error interno al Buscar Colaborador. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new BuscarColaboradorResponse()
                {
                    codigoRes = System.Net.HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al Buscar Colaborador"
                };
            }
        }


        public SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_Result ObtenerDetalleActaPorColaborador(int id_colaborador_entrega)
        {

            try
            {
                var ctx = new EPPEntities();
                var ObtenerdetalleActa = ctx.SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR(id_colaborador_entrega).FirstOrDefault();
                return ObtenerdetalleActa;

            }

            catch (Exception e)
            {

                log.Error("UsuarioDO ->  ListadoDetalleActaEntrega. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                return new SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_Result();
            }


        }

        public List<SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_EPPS_Result> ListarEPPActaEntrega(int id_colaborador_entrega)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosEppColaborador = ctx.SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_EPPS(id_colaborador_entrega).ToList();
                if (datosEppColaborador == null || datosEppColaborador.Count() <= 0)
                {
                    return new List<SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_EPPS_Result>();
                }
                else
                {

                    return datosEppColaborador;
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new List<SP_DETALLE_ACTA_ENTREGRA_X_COLABORADOR_EPPS_Result>();

            }




        }

        public EntregaEppColaboradorIDResponse ObtenerDatosColaboradorPorId(int id_Colaborador_entrega, int id_usuario, string guid)
        {

            try
            {
                var ctx = new EPPEntities();
                var datosEppColaborador = ctx.SP_EXTRAER_DATOS_COLABORADOR_ID(id_Colaborador_entrega, id_usuario).FirstOrDefault();
                if (datosEppColaborador != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_EXTRAER_DATOS_COLABORADOR_ID_Result, EntregaEppColaboradorIDResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_EXTRAER_DATOS_COLABORADOR_ID_Result, EntregaEppColaboradorIDResponse_Data>(datosEppColaborador);

                    return new EntregaEppColaboradorIDResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos encontrados.",
                        data = datosMapeados
                    };


                }
                else
                {

                    return new EntregaEppColaboradorIDResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Mo se encontró los datos del colaborador"
                    };
                }

            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ({guid}) usuario ->({id_usuario})  ObtenerDatosColaboradorPorId. Mensaje al cliente: Error interno al obtener datos del colaborador. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new EntregaEppColaboradorIDResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener datos del colaborador."
                };

            }


        }

        public EntregaEppColaboradorResponse ObtenerRespuestaEstadoColaboradorEntrega(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosEppColaborador = ctx.SP_ENTREGA_EPP_COLABORADOR(id_Colaborador_entrega, id_usuario, aplicacion).FirstOrDefault();
                if (datosEppColaborador.codigo == 200)
                {

                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = datosEppColaborador.descripcion,
                    };


                }
                else if (datosEppColaborador.codigo == 400)
                {
                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = datosEppColaborador.descripcion
                    };
                }
                else
                {

                    return new EntregaEppColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.InternalServerError,
                        mensajeRes = datosEppColaborador.descripcion
                    };
                }

            }
            catch (Exception e)
            {
                log.Error($"UsuarioDO ->({guid}) usuario -> ({id_usuario}) ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new EntregaEppColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener Respuesta Estado Colaborador Entrega."
                };

            }


        }

        public ObtenerGuidAprobacion ObtenerGuiddeAprobacion(int id_Colaborador_entrega, int id_usuario, int aplicacion, string guid)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosgenerados = ctx.SP_GENERAR_GUID_APROBACION(id_usuario, aplicacion, id_Colaborador_entrega).FirstOrDefault();
                if (datosgenerados.codigo == 200)
                {

                    return new ObtenerGuidAprobacion()
                    {
                        codigoRes = HttpStatusCode.OK,
                        codigo = datosgenerados.codigo,
                        mensajeRes = datosgenerados.mensaje,
                        guid_entregado = datosgenerados.guid_entregado,
                        link = datosgenerados.link,
                        correo = datosgenerados.correo,
                        correo_responsable = datosgenerados.correo_responsable
                    };


                }
                else if (datosgenerados.codigo == 400)
                {
                    return new ObtenerGuidAprobacion()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = datosgenerados.mensaje,
                    };
                }
                else
                {

                    return new ObtenerGuidAprobacion()
                    {
                        codigoRes = HttpStatusCode.InternalServerError,
                        mensajeRes = "Error interno en el SP_GENERAR_GUID_APROBACION  "
                    };
                }

            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ->({guid}) usuario -> ({id_usuario}) ObtenerGuiddeAprobacion. Mensaje al cliente: Error interno al obtener los datos generados. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new ObtenerGuidAprobacion()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener los datos generados."
                };

            }


        }

        public SP_OBTENER_DATOS_ENTREGA_COLABORADOR_X_GUID_Result ObtenerDatosEntregaxGuid(string guid_entregado)
        {

            try
            {
                var ctx = new EPPEntities();
                var datosColaborador = ctx.SP_OBTENER_DATOS_ENTREGA_COLABORADOR_X_GUID(guid_entregado).FirstOrDefault();

                if (datosColaborador == null)
                {
                    return new SP_OBTENER_DATOS_ENTREGA_COLABORADOR_X_GUID_Result();
                }
                else
                {

                    return datosColaborador;
                }

            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ->()  ObtenerDatosEntregaxGuid. Mensaje al cliente: Error interno al obtener datos de entrega. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new SP_OBTENER_DATOS_ENTREGA_COLABORADOR_X_GUID_Result();

            }


        }
        public List<SP_LISTAR_DATOS_EPP_X_GUID_Result> ObtenerDatosEEPEntregaxGuid(string guid_entregado)
        {

            try
            {
                var ctx = new EPPEntities();
                var datosEppColaboradorporguid = ctx.SP_LISTAR_DATOS_EPP_X_GUID(guid_entregado).ToList();
                if (datosEppColaboradorporguid == null)
                {
                    return new List<SP_LISTAR_DATOS_EPP_X_GUID_Result>();
                }
                else
                {

                    return datosEppColaboradorporguid;
                }

            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ->()  ObtenerDatosEEPEntregaxGuid. Mensaje al cliente: Error interno al obtener datos de entrega. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new List<SP_LISTAR_DATOS_EPP_X_GUID_Result>();

            }


        }


        public AprobarxWebResponse ObtenerRespuestaAprobarxWeb(string guid_entregado, string guid_aprobacion_transaccion)
        {

            try
            {
                var ctx = new EPPEntities();
                var Respuesta = ctx.SP_ENTREGA_COLABORADOR_WEB(guid_entregado, guid_aprobacion_transaccion).FirstOrDefault();
                if (Respuesta != null)
                {
                    if (Respuesta.codigo == 200)
                    {

                        return new AprobarxWebResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = Respuesta.mensaje,
                        };


                    }
                    else if (Respuesta.codigo == 400)
                    {
                        return new AprobarxWebResponse()
                        {
                            codigoRes = HttpStatusCode.NotFound,
                            mensajeRes = Respuesta.mensaje
                        };
                    }
                    else
                    {

                        return new AprobarxWebResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = Respuesta.mensaje
                        };
                    }
                }
                else
                {
                    return new AprobarxWebResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "Error, interno al entrega colaborador web"
                    };
                }


            }
            catch (Exception e)
            {


                log.Error($"ColaboradorDO ->()  ObtenerRespuestaAprobarxWeb. Mensaje al cliente: Error interno al obtener respuesta de aprobacion  " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new AprobarxWebResponse()
                {

                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error Interno al  obtener respuesta de aprobacion "
                };

            }

        }


        public ReporteColaboradoresResponse Listarcolaboradoresxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int num_pagina, int cant_registro, int idUsuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var fechaDesdeParameter = new SqlParameter("fechaDesde", (object)fechaDesde ?? DBNull.Value);
                var fechaHastaParameter = new SqlParameter("fechaHasta", (object)fechaHasta ?? DBNull.Value);
                var nro_actaEPPParameter = new SqlParameter("nro_actaEPP", (object)nro_actaEPP ?? DBNull.Value);
                var estado_actaParameter = new SqlParameter("estado_acta", (object)estado_acta ?? DBNull.Value);
                var rut_responsableParameter = new SqlParameter("rut_responsable", (object)rut_responsable ?? DBNull.Value);
                var rut_colaboradorParameter = new SqlParameter("rut_colaborador", (object)rut_colaborador ?? DBNull.Value);
                var tipo_colaboradorParameter = new SqlParameter("tipo_colaborador", (object)tipo_colaborador ?? DBNull.Value);
                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                var datosBusqueda = ctx.Database.SqlQuery<SP_BUSCAR_REPORTE_COLABORADOR_Result>("EXEC SP_BUSCAR_REPORTE_COLABORADOR " +
               "@fechaDesde, @fechaHasta,@nro_actaEPP,@estado_acta,@rut_responsable,@rut_colaborador,@tipo_colaborador, @num_pagina, @cant_registro, @idUsuario, @total_registros OUT",
               fechaDesdeParameter, fechaHastaParameter, nro_actaEPPParameter, estado_actaParameter, rut_responsableParameter, rut_colaboradorParameter, tipo_colaboradorParameter, num_paginaParameter, cant_registroParameter, idUsuarioParameter, total_registrosParameter).ToList();

                int total_registros = Convert.ToInt32(total_registrosParameter.Value);
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_BUSCAR_REPORTE_COLABORADOR_Result, ListarReporteColaboradoresXFiltroResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_BUSCAR_REPORTE_COLABORADOR_Result>, List<ListarReporteColaboradoresXFiltroResponse_Data>>(datosBusqueda);

                    return new ReporteColaboradoresResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ReporteColaboradoresResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarReporteColaboradoresXFiltroResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("ColaboradorDO ->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno al listar reportes de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ReporteColaboradoresResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar reportes de colaboradores."
                };
            }

        }

        public ReporteColaboradorExcelResponse ListarcolaboradoresExcelxfiltro(string fechaDesde, string fechaHasta, string nro_actaEPP, string estado_acta, string rut_responsable, string rut_colaborador, string tipo_colaborador, int idUsuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var fechaDesdeParameter = new SqlParameter("fechaDesde", (object)fechaDesde ?? DBNull.Value);
                var fechaHastaParameter = new SqlParameter("fechaHasta", (object)fechaHasta ?? DBNull.Value);
                var nro_actaEPPParameter = new SqlParameter("nro_actaEPP", (object)nro_actaEPP ?? DBNull.Value);
                var estado_actaParameter = new SqlParameter("estado_acta", (object)estado_acta ?? DBNull.Value);
                var rut_responsableParameter = new SqlParameter("rut_responsable", (object)rut_responsable ?? DBNull.Value);
                var rut_colaboradorParameter = new SqlParameter("rut_colaborador", (object)rut_colaborador ?? DBNull.Value);
                var tipo_colaboradorParameter = new SqlParameter("tipo_colaborador", (object)tipo_colaborador ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)idUsuario ?? DBNull.Value);

                var datosBusqueda = ctx.Database.SqlQuery<SP_BUSCAR_REPORTE_COLABORADOR_EXCEL_Result>("EXEC SP_BUSCAR_REPORTE_COLABORADOR_EXCEL " +
               "@fechaDesde, @fechaHasta,@nro_actaEPP,@estado_acta,@rut_responsable,@rut_colaborador,@tipo_colaborador, @idUsuario",
               fechaDesdeParameter, fechaHastaParameter, nro_actaEPPParameter, estado_actaParameter, rut_responsableParameter, rut_colaboradorParameter, tipo_colaboradorParameter, idUsuarioParameter).ToList();


                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_BUSCAR_REPORTE_COLABORADOR_EXCEL_Result, ListarReporteColaboradoresExcelXFiltroResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_BUSCAR_REPORTE_COLABORADOR_EXCEL_Result>, List<ListarReporteColaboradoresExcelXFiltroResponse_Data>>(datosBusqueda);

                    return new ReporteColaboradorExcelResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),

                    };
                }
                else
                {
                    return new ReporteColaboradorExcelResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarReporteColaboradoresExcelXFiltroResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("ColaboradorDO ->  Listarcolaboradoresxfiltro. Mensaje al cliente: Error interno al listar reportes de colaboradores. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ReporteColaboradorExcelResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar reportes de colaboradores."
                };
            }

        }

        public SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result GuardarDatosDocumento(string documento, int id_usuario, int id_aplicacion, int id_colaborador_entrega, string guid)
        {

            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR(documento, id_usuario, id_aplicacion, id_colaborador_entrega).FirstOrDefault();
                if (datosBusqueda != null)
                {

                    return new SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result()
                    {
                        codigo = 200,
                        descripcion = datosBusqueda.descripcion

                    };
                }
                else if (datosBusqueda.codigo == 400)
                {
                    return new SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result()
                    {
                        codigo = datosBusqueda.codigo,
                        descripcion = datosBusqueda.descripcion
                    };
                }
                else
                {
                    return new SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result()
                    {
                        codigo = datosBusqueda.codigo,
                        descripcion = datosBusqueda.descripcion
                    };


                }
            }
            catch (Exception e)
            {
                log.Error($"ActasDO ({guid}) usuario -> ({id_usuario}) ->  GuardarDatosDocumento. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                return new SP_GUARDAR_DATOS_DOCUMENTO_COLABORADOR_Result()
                {
                    codigo = 500,
                    descripcion = "Error interno al guardar los datos del documento"
                };

            }

        }
        public ObtenerDocumentWebResponse ObtenerDatosDocumentosWeb(string gui_entregado)
        {
            var response = new ObtenerDocumentWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR_WEB(gui_entregado).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR_WEB_Result, datosDocumentoWeb>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DATOS_DOCUMENTO_COLABORADOR_WEB_Result, datosDocumentoWeb>(datosBusqueda);

                    return new ObtenerDocumentWebResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new ObtenerDocumentWebResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("ActasDO ->  ObtenerDatosDocumentos. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle del documento";
            }
            return response;
        }

        public ObtenerDatosColaboradorResponse obtenerDatosColaborador(string rut)
        {

            try
            {
                var ctx = new EPPEntities();
                var RespuestaValidacion = ctx.SP_EXTRAER_DATOS_COLABORADOR(rut).FirstOrDefault();
                return new ObtenerDatosColaboradorResponse()
                {
                    nombres = RespuestaValidacion.nombres,
                    apellidos = RespuestaValidacion.apellidos,
                    rut = RespuestaValidacion.rut,
                    tipo_documento = RespuestaValidacion.tipo_documento,
                    correo = RespuestaValidacion.correo,
                    genero = RespuestaValidacion.genero,
                    fecha_nacimiento = RespuestaValidacion.fecha_nacimiento,
                    celular = RespuestaValidacion.celular,
                    tipo_usuario = RespuestaValidacion.tipo_usuario,
                    codigoRes = HttpStatusCode.OK,
                    mensajeRes = "Se obtuvo los datos del  colaborador correctamente."
                };


            }
            catch (Exception e)
            {
                log.Error($"UsuarioDO ->  obtenerDatosColaborador. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ObtenerDatosColaboradorResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al obtener Respuesta."
                };

            }


        }
        public DatosEntregaPEFResponse DatosEntregaPDF(int id_colaborador_entrega, string guid)
        {
            var response = new DatosEntregaPEFResponse();
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_OBTENER_DATOS_ENTREGA_PDF(id_colaborador_entrega).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_OBTENER_DATOS_ENTREGA_PDF_Result, datosEntrega>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DATOS_ENTREGA_PDF_Result, datosEntrega>(datosBusqueda);

                    return new DatosEntregaPEFResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        datos = datosMapeados
                    };
                }
                else
                {
                    return new DatosEntregaPEFResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ({guid}) ->  DatosEppPDF -> Mensaje al cliente: Error interno obtener detalle de los Epp." +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno obtener detalle de los Epp.";
            }
            return response;
        }

        public  ActualizarColaboradorResponse ActualizarColaborador(int id_colaborador_entrega , string genero,string correo,string fecha_nacimiento , int id_aplicacion, int id_usuario,string guid)
        {
            var response = new ActualizarColaboradorResponse();
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_ACTUALIZAR_DATOS_COLABORADOR(id_colaborador_entrega, genero, correo, fecha_nacimiento, id_aplicacion, id_usuario).FirstOrDefault();
                if (respuesta != null)
                { 
                   if(respuesta.codigo==200)
                    {
                        return new ActualizarColaboradorResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = respuesta.descripcion,
                         
                        };


                    }
                    else if(respuesta.codigo==400)
                    {
                        return new ActualizarColaboradorResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = respuesta.descripcion,
                          
                        };

                    }
                   else
                    {
                        return new ActualizarColaboradorResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = respuesta.descripcion,

                        };

                    }
                    
                   
                }
                else
                {
                    return new ActualizarColaboradorResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos."
                    };
                }

            }
            catch (Exception e)
            {
                log.Error($"ColaboradorDO ({guid}) -> id_usuario ->{id_usuario} ActualizarColaborador -> Mensaje al cliente: Error interno " +
                                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno en el servicio de actualizar colaborador.";
            }
            return response;
        }

    }


}
