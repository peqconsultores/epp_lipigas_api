﻿using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.Entities.EDigital.Request;
using EPPLipigas.Entities.EDigital.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace EPPLipigas.DataAccess.Implementacion
{
    public class ApiEDigitalDO : IApiEDigitalDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AplicacionDO));
        private int _timeOut = 5;
        private string _urlApiEDigital = ConfigurationManager.AppSettings["API_EDIGITAL_URL"];
        private string _userApiEDigital = ConfigurationManager.AppSettings["API_EDIGITAL_USER"];
        private string _clientIdApiEDigital = ConfigurationManager.AppSettings["API_EDIGITAL_CLIENT_ID"];
        private string _passApiEDigital = ConfigurationManager.AppSettings["API_EDIGITAL_PASSWORD"];

        public string tokenAutenticacion { get; set; }


        public GetTokenResponse GetToken()
        {
            GetTokenResponse responseMethod = new GetTokenResponse();

            try
            {
                string urlService = "api/v2/auth";
                var dataRequest = new GetTokenRequest()
                {
                    grant_type = "client_credentials",
                    cod_user = _userApiEDigital,
                    client_id = _clientIdApiEDigital,
                    client_secret = _passApiEDigital
                };

                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);
                //httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                //httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");

                string json = JsonConvert.SerializeObject(dataRequest);

                StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var responseService = httpClient.PostAsync(_urlApiEDigital + urlService, httpContent).Result;

                var test = JsonConvert.SerializeObject(responseService);

                 GetToken_GetDataService(responseService, responseMethod);
            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ->   GetToken. Mensaje al cliente: Error interno al obtener el token " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de EPP del token.";
            }
            return responseMethod;
        }
        private void GetToken_GetDataService(HttpResponseMessage responseService, GetTokenResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        var objJson = JsonConvert.DeserializeObject<TokenResponse>(json);


                        var datatoken = new GetTokenResponse_OK()
                        {
                            access_token = objJson.token.access_token,
                            expires_in = objJson.token.expires_in,
                            token_type = objJson.token.token_type,
                            scope = objJson.token.scope
                        };

                        responseMethod.token = datatoken;
                        responseMethod.codeHTTP = HttpStatusCode.OK;

                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (GetTokenResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(GetTokenResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }

        public GetValidUsuarioResponse GetValidUsuario(string rut, string tipo_documento, string token, string guid)
        {
            GetValidUsuarioResponse responseMethod = new GetValidUsuarioResponse();

            try
            {
                string urlService = $"api/v2/users/{tipo_documento}-{rut}";
                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);

                if (!String.IsNullOrEmpty(token))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }

                var responseService = httpClient.GetAsync(_urlApiEDigital + urlService).Result;
                var test = JsonConvert.SerializeObject(responseService);

                GetValidUsuario_GetDataService(responseService, responseMethod);

            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ({guid}) ->   GetValidUsuario. Mensaje al cliente: Error interno al validar usuario " +
                    "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de E-Digital  del validar usuario.";
            }
            return responseMethod;
        }


        private void GetValidUsuario_GetDataService(HttpResponseMessage responseService, GetValidUsuarioResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        responseMethod.data = (GetValidUsuarioResponse_OK)JsonConvert.DeserializeObject(json, typeof(GetValidUsuarioResponse_OK));
                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (GetValidUsuarioResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(GetValidUsuarioResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }

        public CrearUsuarioResponse CrearUsuario(string nombres, string apellidos, string rut, string tipo_documento, string correo, string genero, string fecha_nacimiento, string celular, string tipo_usuario, string token)
        {
            CrearUsuarioResponse responseMethod = new CrearUsuarioResponse();

            try
            {
                string urlService = "api/v2/users";
                var dataRequest = new CrearUsuarioRequest()
                {
                    name = nombres,
                    surname = apellidos,
                    identity_document = rut,
                    type_identity_document = tipo_documento,
                    email = correo,
                    gender = genero,
                    birth = fecha_nacimiento,
                    phone = celular,
                    type = tipo_usuario,
                };

                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);

                if (!String.IsNullOrEmpty(token))
                {

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                                        token);
                }
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");


                string json = JsonConvert.SerializeObject(dataRequest);

                StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var responseService = httpClient.PostAsync(_urlApiEDigital + urlService, httpContent).Result;

                var test = JsonConvert.SerializeObject(responseService);

                crearUsuario_GetDataService(responseService, responseMethod);
            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ->   CrearUsuario. Mensaje al cliente: Error interno al crear usuario " +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de crear Usuario para la firma digital.";
            }
            return responseMethod;


        }

        private void crearUsuario_GetDataService(HttpResponseMessage responseService, CrearUsuarioResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        responseMethod.data = (CrearUsuarioResponse_OK)JsonConvert.DeserializeObject(json, typeof(CrearUsuarioResponse_OK));
                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (CrearUsuarioResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(CrearUsuarioResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                            responseMethod.messageHTTP = responseMethod.data_badquest_otros.message;
                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }
        public CrearDocumentoResponse CrearDocumento(string rut_colaborador, string description, string template_id, string rut_responsable, object metadata,
            string document_file, string document_b64, string document_link, object settings,
            string token, string guid)

        {
            CrearDocumentoResponse responseMethod = new CrearDocumentoResponse();

            try
            {
                string urlService = "api/v2/document";

                List<string> listasigners = new List<string>();
                listasigners.Add(rut_colaborador);

                List<string> final_recipients = new List<string>();
                final_recipients.Add("signer-1");

                var dataRequest = new CrearDocumentoRequest()
                {
                    description = description,
                    template_id = template_id,
                    signers = listasigners,
                    final_recipients = final_recipients,
                    metadata = metadata,
                    document_file = document_file,
                    document_b64 = document_b64,
                    document_link = document_link,
                    settings = settings
                };

                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);

                if (!String.IsNullOrEmpty(token))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }

                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                string json = JsonConvert.SerializeObject(dataRequest);

                StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var responseService = httpClient.PostAsync(_urlApiEDigital + urlService, httpContent).Result;

                var test = JsonConvert.SerializeObject(responseService);

                crearDocumento_GetDataService(responseService, responseMethod);
            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ({guid}) -> CrearDocumento. Mensaje al cliente: Error interno al crear documento" +
                   "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de crear documento";
            }
            return responseMethod;


        }
        private void crearDocumento_GetDataService(HttpResponseMessage responseService, CrearDocumentoResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        responseMethod.data = (CrearDocumentoResponse_OK)JsonConvert.DeserializeObject(json, typeof(CrearDocumentoResponse_OK));
                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (CrearDocumentoResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(CrearDocumentoResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                            if (responseMethod.data_badquest_otros != null && !String.IsNullOrEmpty(responseMethod.data_badquest_otros.message))
                            {
                                responseMethod.messageHTTP = responseMethod.data_badquest_otros.message;
                            }
                            else
                            {
                                responseMethod.messageHTTP = "No se pudo crear el usuario para la firma digital en el proveedor.";
                            }

                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }
        public ObtenerDocumentoResponse ObtenerDocumento(string document_hash, string token)
        {
            ObtenerDocumentoResponse responseMethod = new ObtenerDocumentoResponse();

            try
            {
                string urlService = $"api/v2/document/{document_hash}";

                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);

                if (!String.IsNullOrEmpty(token))
                {

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                                        token);
                }
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");


                var responseService = httpClient.GetAsync(_urlApiEDigital + urlService).Result;

                var test = JsonConvert.SerializeObject(responseService);

                ObtenerDocumento_GetDataService(responseService, responseMethod);
            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ->   ObtenerDocumento. Mensaje al cliente: Error interno al crear documento" +
                  "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de obtener documento";
            }
            return responseMethod;


        }

        private void ObtenerDocumento_GetDataService(HttpResponseMessage responseService, ObtenerDocumentoResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        responseMethod.data = (ObtenerDocumentoResponse_OK)JsonConvert.DeserializeObject(json, typeof(ObtenerDocumentoResponse_OK));
                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (ObtenerDocumentoResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(ObtenerDocumentoResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }

        public ObtenerDocumentoResponse DescargarDocumento(string document_hash, string token)
        {
            ObtenerDocumentoResponse responseMethod = new ObtenerDocumentoResponse();

            try
            {
                string urlService = $"api/v2/document/{document_hash}?format_output=url_S3";

                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);
                httpClient.Timeout = new TimeSpan(0, _timeOut, 0);

                if (!String.IsNullOrEmpty(token))
                {

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                                        token);
                }
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");


                var responseService = httpClient.GetAsync(_urlApiEDigital + urlService).Result;

                var test = JsonConvert.SerializeObject(responseService);

                DescargarDocumento_GetDataService(responseService, responseMethod);
            }
            catch (Exception ex)
            {
                log.Error($"ApiEDigitalDO ->   DescargarDocumento. Mensaje al cliente: Error interno al descargar documento" +
                  "Detalle error: " + JsonConvert.SerializeObject(ex));
                responseMethod.codeHTTP = HttpStatusCode.InternalServerError;
                responseMethod.messageHTTP = "Error en la petición del servicio de obtener documento";
            }
            return responseMethod;


        }
        private void DescargarDocumento_GetDataService(HttpResponseMessage responseService, ObtenerDocumentoResponse responseMethod)
        {
            if (responseService.StatusCode == HttpStatusCode.OK)
            {
                using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                {
                    using (StreamReader re = new StreamReader(stream))
                    {
                        String json = re.ReadToEnd();

                        responseMethod.data = (ObtenerDocumentoResponse_OK)JsonConvert.DeserializeObject(json, typeof(ObtenerDocumentoResponse_OK));
                    }
                }
            }
            else
            {
                if (responseService.StatusCode != HttpStatusCode.NoContent)
                {
                    using (Stream stream = responseService.Content.ReadAsStreamAsync().Result)
                    {
                        using (StreamReader re = new StreamReader(stream))
                        {
                            String json = re.ReadToEnd();

                            var dataBadRequest = (ObtenerDocumentoResponse_BadRequestYOtros)JsonConvert.DeserializeObject(json, typeof(ObtenerDocumentoResponse_BadRequestYOtros));
                            responseMethod.data_badquest_otros = dataBadRequest;
                        }
                    }
                }
            }
            responseMethod.codeHTTP = responseService.StatusCode;
        }

    }
}
