﻿using AutoMapper;
using EPPLipigas.DataAccess.Contrato;
using EPPLipigas.DataAccess.Models;
using EPPLipigas.Entities;
using EPPLipigas.Entities.Colaborador.Response;
using EPPLipigas.Entities.EntitieBD;
using EPPLipigas.Entities.Epp;
using EPPLipigas.Entities.Instalacion;
using EPPLipigas.Entities.Rut;
using EPPLipigas.Entities.Usuario;
using EPPLipigas.Entities.Usuario.Request;
using EPPLipigas.Entities.Usuario.Response;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace EPPLipigas.DataAccess.Implementacion
{

    public class UsuarioDO : IUsuarioDO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UsuarioDO));
        public BusquedaRutResponse BusquedaRut(string rut)
        {
            try
            {
                var ctx = new EPPEntities();
                var response = new BusquedaRutResponse() { datos = new DatosBusquedaRut() };
                var data = ctx.SP_EXTRAER_DATOS_PERSONA_X_RUT(rut).FirstOrDefault();

                if (data != null)
                {
                    if (data.codigoRes.GetValueOrDefault() == 200)
                    {
                        response.codigoRes = HttpStatusCode.OK;
                        response.mensajeRes = data.mensajeRes;
                        response.datos.rut = rut;
                        response.datos.nombres = !String.IsNullOrEmpty(data.nombres) ? data.nombres : "";
                        response.datos.apellido_materno = !String.IsNullOrEmpty(data.apellido_materno) ? data.apellido_materno : "";
                        response.datos.apellido_paterno = !String.IsNullOrEmpty(data.apellido_paterno) ? data.apellido_paterno : "";
                        response.datos.correo = !String.IsNullOrEmpty(data.correo) ? data.correo : "";
                        response.datos.instalacion = !String.IsNullOrEmpty(data.nombre_instalacion) ? data.nombre_instalacion : "";
                        response.datos.celular = !String.IsNullOrEmpty(data.celular) ? data.celular : "";
                    }
                    else
                    {
                        response.codigoRes = HttpStatusCode.NoContent;
                        response.mensajeRes = data.mensajeRes;
                    }
                }
                else
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = "No se encontro registros";
                }

                return response;
            }
            catch (Exception e)
            {
                log.Error("IUsuarioDO ->  BusquedaRut. Mensaje al cliente: Error interno al buscar rut. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new BusquedaRutResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno buscar rut."
                };
            }
        }
        public ValidarExistenciaUsuarioResponse ValidarExistenciaUsuario(string rut)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_SEGURIDAD_VALIDAR_EXISTENCIA_USUARIO(rut).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    if (datosBusqueda.codigo == 200)
                    {
                        return new ValidarExistenciaUsuarioResponse()
                        {
                            id_persona = datosBusqueda.id_persona.GetValueOrDefault(),
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else if (datosBusqueda.codigo == 400)
                    {
                        return new ValidarExistenciaUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.NotFound,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else if (datosBusqueda.codigo == 500)
                    {
                        return new ValidarExistenciaUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else
                    {
                        return new ValidarExistenciaUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "No se pudo validar la existencia del usuario."
                        };
                    }
                }
                else
                {
                    return new ValidarExistenciaUsuarioResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvo respuesta al validar la existencia del usuario."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ValidarExistenciaUsuario. Mensaje al cliente: Error interno al validar la existencia del usuario. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ValidarExistenciaUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al validar la existencia del usuario."
                };
            }
        }

        //public RegistrarUsuarioResponse crearUsuario(int id_persona,string tipo_documento,string fecha_nacimiento,string genero,string tipo_usuario)
        //{
        //    try
        //    {
        //        var ctx = new EPPEntities();
        //        var respuesta = ctx.SP_REGISTRAR_USUARIO(id_persona, tipo_documento, fecha_nacimiento, genero, tipo_usuario).FirstOrDefault();
        //        if (respuesta.codigo == 200)
        //        {

        //            return new RegistrarUsuarioResponse()
        //            {
        //                codigoRes = HttpStatusCode.OK,
        //                mensajeRes = respuesta.descripcion
        //            };

        //        }
        //        else
        //        {
        //            return new RegistrarUsuarioResponse()
        //            {
        //                codigoRes = HttpStatusCode.InternalServerError,
        //                mensajeRes = respuesta.descripcion
        //            };

        //        }

        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("UsuarioDO ->  crearUsuario. Mensaje al cliente: Error interno al crear un usuario. " +
        //           "Detalle error: " + JsonConvert.SerializeObject(e));
        //        return new RegistrarUsuarioResponse()
        //        {
        //            codigoRes = HttpStatusCode.InternalServerError,
        //            mensajeRes = "Error interno al crear un usuario."
        //        };

        //    }




        //}
        public EditarUsuarioResponse EditarUsuario(EditarUsuarioRequest request, int id_usuario_mod, int id_aplicacion_mod, int _id_persona)
        {
            try
            {
                var ctx = new EPPEntities();
                var id_usuario_modParameter = new SqlParameter("id_usuario_mod", (object)id_usuario_mod ?? DBNull.Value);
                var id_aplicacion_modParameter = new SqlParameter("id_aplicacion_mod", (object)id_aplicacion_mod ?? DBNull.Value);
                var _id_personaParameter = new SqlParameter("_id_persona", (object)_id_persona ?? DBNull.Value);
                var rutParameter = new SqlParameter("rut", (object)request.rut ?? DBNull.Value);
                var nombresParameter = new SqlParameter("nombres", (object)request.nombres ?? DBNull.Value);
                var apellido_paternoParameter = new SqlParameter("apellido_paterno", (object)request.apellido_paterno ?? DBNull.Value);
                var apellido_maternoParameter = new SqlParameter("apellido_materno", (object)request.apellido_materno ?? DBNull.Value);
                var cod_instalacionParameter = new SqlParameter("cod_instalacion", (object)request.cod_instalacion ?? DBNull.Value);
                var cod_rolParameter = new SqlParameter("cod_rol", (object)request.cod_rol ?? DBNull.Value);
                var cod_tipo_colaboradorParameter = new SqlParameter("cod_tipo_colaborador", (object)request.cod_tipo_colaborador ?? DBNull.Value);
                var nombre_empresaParameter = new SqlParameter("nombre_empresa", (object)request.nombre_empresa ?? DBNull.Value);
                var areaParameter = new SqlParameter("area", (object)request.area ?? DBNull.Value);
                var correoParameter = new SqlParameter("correo", (object)request.correo ?? DBNull.Value);
                var celularParameter = new SqlParameter("celular", (object)request.celular ?? DBNull.Value);
                var cod_cargoParameter = new SqlParameter("cod_cargo", (object)request.cod_cargo ?? DBNull.Value);
                var estadoParameter = new SqlParameter("estado", (object)request.estado ?? DBNull.Value);
                var generoParameter = new SqlParameter("genero", (object)request.genero ?? DBNull.Value);
                var fecha_nacimientoParameter = new SqlParameter("fecha_nacimiento", (object)request.fecha_nacimiento ?? DBNull.Value);


                var datosRegistro = ctx.Database.SqlQuery<SP_SEGURIDAD_EDITAR_USUARIO_Result>(
                    "EXEC SP_SEGURIDAD_EDITAR_USUARIO " +
                    "@id_usuario_mod, " +
                    "@id_aplicacion_mod, " +
                    "@_id_persona, " +
                    "@rut, " +
                    "@nombres, " +
                    "@apellido_paterno, " +
                    "@apellido_materno, " +
                    "@cod_instalacion, " +
                    "@cod_rol, " +
                    "@cod_tipo_colaborador, " +
                    "@nombre_empresa, " +
                    "@area, " +
                    "@correo, " +
                    "@celular, " +
                    "@cod_cargo, " +
                    "@genero, " +
                    "@fecha_nacimiento," +
                    "@estado",
                    id_usuario_modParameter, id_aplicacion_modParameter, _id_personaParameter, rutParameter, nombresParameter,
                    apellido_paternoParameter, apellido_maternoParameter, cod_instalacionParameter, cod_rolParameter,
                    cod_tipo_colaboradorParameter, nombre_empresaParameter, areaParameter, correoParameter,
                    celularParameter, cod_cargoParameter, estadoParameter, generoParameter, fecha_nacimientoParameter).FirstOrDefault();

                if (datosRegistro != null)
                {
                    if (datosRegistro.codigo == 201)
                    {
                        return new EditarUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.Created,
                            mensajeRes = datosRegistro.descripcion
                        };
                    }
                    else
                    {
                        if (datosRegistro.codigo == 400)
                        {
                            return new EditarUsuarioResponse()
                            {
                                codigoRes = HttpStatusCode.BadRequest,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 401)
                        {
                            return new EditarUsuarioResponse()
                            {
                                codigoRes = HttpStatusCode.Unauthorized,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else if (datosRegistro.codigo == 404)
                        {
                            return new EditarUsuarioResponse()
                            {
                                codigoRes = HttpStatusCode.NotFound,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                        else
                        {
                            return new EditarUsuarioResponse()
                            {
                                codigoRes = HttpStatusCode.InternalServerError,
                                mensajeRes = datosRegistro.descripcion
                            };
                        }
                    }
                }
                else
                {
                    return new EditarUsuarioResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvo respuesta al editar el usuario."
                    };
                }
            }
            catch (Exception ex)
            {
                return new EditarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al editar el usuario."
                };
            }
        }
        public EliminarUsuarioResponse EliminarUsuario(int id_aplicacion_elim, int id_usuario_elim, int id_persona)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_SEGURIDAD_ELIMINAR_USUARIO(id_aplicacion_elim, id_usuario_elim, id_persona).FirstOrDefault();
                if (datosBusqueda != null)
                {
                    if (datosBusqueda.codigo == 200)
                    {
                        return new EliminarUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else if (datosBusqueda.codigo == 404)
                    {
                        return new EliminarUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.NotFound,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else if (datosBusqueda.codigo == 500)
                    {
                        return new EliminarUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.InternalServerError,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                    else
                    {
                        return new EliminarUsuarioResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = "No se pudo eliminar usuario."
                        };
                    }
                }
                else
                {
                    return new EliminarUsuarioResponse()
                    {
                        codigoRes = HttpStatusCode.BadRequest,
                        mensajeRes = "No se obtuvo respuesta al eliminar usuario."
                    };
                }
            }
            catch (Exception e)
            {
                log.Error($"UsuarioDO ->  EliminarUsuario. Mensaje al cliente: Error interno al eliminar usuario. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new EliminarUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al eliminar usuario."
                };
            }
        }



        public ListaInstalacionResponse ListarInstalacion()
        {
            var response = new ListaInstalacionResponse();
            try
            {

                var ctx = new EPPEntities();
                var listarInst = ctx.SP_LISTAR_INSTALACION().ToList();

                if (listarInst == null || listarInst.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = "No se obtuvo lista de instalacion";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_INSTALACION_Result, DataInstalacion>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_INSTALACION_Result>, List<DataInstalacion>>(listarInst);
                    response.listaInstalacion = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Se obtuvo la lista  de instalacion";

                }



            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarInstalacion. Mensaje al cliente: Error interno " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de instalaciones";

            }


            return response;

        }
        public UsuariosPorInstalacionResponse listarUsuarioxInstalacion(int id_usuario)
        {
            var response = new UsuariosPorInstalacionResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarxUusario = ctx.SP_LISTAR_USUARIO_X_INSTALACION(id_usuario).ToList();

                if (listarxUusario == null || listarxUusario.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de usuarios por instalación";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_USUARIO_X_INSTALACION_Result, DataUsuariosPorInstalacion>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_USUARIO_X_INSTALACION_Result>, List<DataUsuariosPorInstalacion>>(listarxUusario);
                    response.listaUsuariosPorInstalacion = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Se obtuvo la lista de usuario por instalación";

                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarUsuarioxInstalacion. Mensaje al cliente: Error interno . " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de instalaciones";

            }
            return response;


        }

        public ListarTipodecolaboradoresxUsuario ListarUsuariosxTipodecolaborador()
        {
            var response = new ListarTipodecolaboradoresxUsuario();
            try
            {
                var ctx = new EPPEntities();
                var listarxusarioColab = ctx.SP_LISTAR_TIPOS_USUARIO_COLABORADOR().ToList();

                if (listarxusarioColab == null || listarxusarioColab.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de usuarios por tipo de colaborador";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_TIPOS_USUARIO_COLABORADOR_Result, DataTipoColaborador>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_TIPOS_USUARIO_COLABORADOR_Result>, List<DataTipoColaborador>>(listarxusarioColab);
                    response.ListadeTiposColaborador = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de usuario por instalacion";

                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarUsuarioxInstalacion. Mensaje al cliente: Error interno . " +
                      "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de instalaciones";

            }
            return response;



        }

        public ListarUsuariosXFiltrosResponse ListarUsuariosXFiltros(string nombres, string rut, int num_pagina, int cant_registros, int idUsuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosBusqueda = ctx.SP_LISTAR_USUARIOS_X_FILTROS(nombres, rut, num_pagina, cant_registros, idUsuario).ToList();
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_USUARIOS_X_FILTROS_Result, ListarUsuariosXFiltrosResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_USUARIOS_X_FILTROS_Result>, List<ListarUsuariosXFiltrosResponse_Data>>(datosBusqueda);

                    return new ListarUsuariosXFiltrosResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Se obtuvieron los datos correctamente.",
                        total_registros = datosMapeados[0].total_registros.GetValueOrDefault(),
                        data = datosMapeados.ToList()
                    };
                }
                else
                {
                    return new ListarUsuariosXFiltrosResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarUsuariosXFiltrosResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarUsuariosXFiltros. Mensaje al cliente: Error interno al listar usuarios por filtros. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarUsuariosXFiltrosResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar usuarios por filtros."
                };
            }
        }
        public ListarEppResponse listarEpp()
        {
            var response = new ListarEppResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarEpp = ctx.SP_LISTAR_EPP().ToList();
                if (listarEpp == null || listarEpp.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_EPP_Result, DataEpp>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_EPP_Result>, List<DataEpp>>(listarEpp);
                    response.listaEpp = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista equipo proteccion personal";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de equipo proteccion personal";

            }
            return response;


        }

        public ListarCargoResponse listarCargos()
        {
            var response = new ListarCargoResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarCargo = ctx.SP_LISTA_CARGO().ToList();
                if (listarCargo == null || listarCargo.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de cargos";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTA_CARGO_Result, DataCargo>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTA_CARGO_Result>, List<DataCargo>>(listarCargo);
                    response.listaCargo = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de los cargos";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarCargos. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de los cargos";

            }
            return response;


        }
        public ListarClasificacionEppResponse ListarClasificaiconEpp()
        {

            var response = new ListarClasificacionEppResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarClasifiacionEPP = ctx.SP_LISTAR_CLASIFICACION().ToList();
                if (listarClasifiacionEPP == null || listarClasifiacionEPP.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_CLASIFICACION_Result, DataClasificacionEpp>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_CLASIFICACION_Result>, List<DataClasificacionEpp>>(listarClasifiacionEPP);
                    response.listaDataClasificacionEpp = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de calsificacion de epp";

            }
            return response;


        }
        public ListarCategoriaEppResponse ListarCategoriaEpp()
        {
            var response = new ListarCategoriaEppResponse();

            try
            {
                var ctx = new EPPEntities();
                var listarCategoriaEPP = ctx.SP_listar_categoria().ToList();
                if (listarCategoriaEPP == null || listarCategoriaEPP.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_listar_categoria_Result, DataCategoriaEpp>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_listar_categoria_Result>, List<DataCategoriaEpp>>(listarCategoriaEPP);
                    response.listaDataCategoriaEpp = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de calsificacion de epp";

            }
            return response;




        }

        public ListarAreaResponse ListarArea()
        {
            var response = new ListarAreaResponse();

            try
            {
                var ctx = new EPPEntities();
                var listarArea = ctx.SP_LISTAR_AREA().ToList();
                if (listarArea == null || listarArea.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de area";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_AREA_Result, DataArea>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_AREA_Result>, List<DataArea>>(listarArea);
                    response.listaArea = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de area";

            }
            return response;




        }
        public ListarEmpresaResponse ListarEmpresa()
        {
            var response = new ListarEmpresaResponse();

            try
            {
                var ctx = new EPPEntities();
                var listarEmpresa = ctx.SP_LISTAR_EMPRESA().ToList();
                if (listarEmpresa == null || listarEmpresa.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_EMPRESA_Result, DataEmpresa>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_EMPRESA_Result>, List<DataEmpresa>>(listarEmpresa);
                    response.listaEmpresa = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de empresa";

            }
            return response;




        }
        public ListarDatosUsuariosResponse ListarDatosUsuario(int idUsuario)
        {
            var response = new ListarDatosUsuariosResponse();

            try
            {
                var ctx = new EPPEntities();
                var listarDatosUsuario = ctx.SP_EXTRAER_DATOS_USUARIO(idUsuario).ToList();
                if (listarDatosUsuario == null || listarDatosUsuario.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de datos de usuario";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_EXTRAER_DATOS_USUARIO_Result, DatosUsuarios>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_EXTRAER_DATOS_USUARIO_Result>, List<DatosUsuarios>>(listarDatosUsuario);
                    response.listaDatosUsuarios = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de datos de usuario ";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarDatosUsuario. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de datos de usuarios ";

            }
            return response;
        }

        public SP_Listado_detalle_Acta_Entrega_Result ListadoDetalleActaEntrega(int id_entrega, int idUsuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var listarDatosUsuario = ctx.SP_Listado_detalle_Acta_Entrega(id_entrega, idUsuario).FirstOrDefault();
                return listarDatosUsuario;
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListadoDetalleActaEntrega. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                return new SP_Listado_detalle_Acta_Entrega_Result();
            }
        }


        public List<SP_LISTA_COLABORADORES_DETALLE_ACTA_ENTREGRA_Result> ListarColaboradresActaEntrega(int id_entrega)
        {
            try
            {
                var ctx = new EPPEntities();
                var datoColaboradorActaEntrega = ctx.SP_LISTA_COLABORADORES_DETALLE_ACTA_ENTREGRA(id_entrega).ToList();
                if (datoColaboradorActaEntrega == null || datoColaboradorActaEntrega.Count() <= 0)
                {
                    return new List<SP_LISTA_COLABORADORES_DETALLE_ACTA_ENTREGRA_Result>();
                }
                else
                {

                    return datoColaboradorActaEntrega;
                }

            }

            catch (Exception e)
            {


                log.Error("UsuarioDO ->  ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));

                return new List<SP_LISTA_COLABORADORES_DETALLE_ACTA_ENTREGRA_Result>();

            }

        }

        public List<sp_listar_epp_colaborador_Result> ListarEPPActaEntrega(int id_entrega, int idUsuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var datosEppColaborador = ctx.sp_listar_epp_colaborador(id_entrega, idUsuario).ToList();
                if (datosEppColaborador == null || datosEppColaborador.Count() <= 0)
                {
                    return new List<sp_listar_epp_colaborador_Result>();
                }
                else
                {

                    return datosEppColaborador;
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                return new List<sp_listar_epp_colaborador_Result>();

            }




        }

        public CambiarContraseñaResponse ObtenerRespuesta(int idUsuario, int id_aplicacion, string guid)
        {
            var response = new CambiarContraseñaResponse();
            try
            {
                var ctx = new EPPEntities();
                var respuesta = ctx.SP_CAMABIAR_CONTRASEÑA(idUsuario, id_aplicacion).FirstOrDefault();
                if (respuesta == null)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo  la respuesta ";
                }
                else
                {

                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Se obtuvo la respuesta correctamente";
                }
            }
            catch (Exception e)
            {
                log.Error($"UsuarioDO -> ({guid})-> ObtenerRespuesta. Mensaje al cliente:  Error interno al obtener respuesta " +
                   "Detalle error: " + JsonConvert.SerializeObject(e));

                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la respuesta";

            }

            return response;

        }

        public ObtenerDetalleUsuarioResponse ObtenerDetalleUsuario(int id_persona, string cod_aplicacion)
        {
            var response = new ObtenerDetalleUsuarioResponse();

            try
            {
                var ctx = new EPPEntities();
                var dataUsuario = ctx.SP_OBTENER_DETALLE_USUARIO(id_persona, cod_aplicacion).FirstOrDefault();
                if (dataUsuario == null)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo el detalle del usuario";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_DETALLE_USUARIO_Result, DetalleUsuario>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<SP_OBTENER_DETALLE_USUARIO_Result, DetalleUsuario>(dataUsuario);
                    response.data = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "Datos obtenidos correctamente";
                }
            }


            catch (Exception e)
            {
                log.Error($"UsuarioDO -> ObtenerDetalleUsuario. Mensaje al cliente: Error interno al obtener el detalle del usuario. " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener el detalle del usuario";


            }
            return response;
        }


        public ListasMaestroWebResponse ListarClasificaiconEppWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarClasifiacionEPPWeb = ctx.SP_LISTAR_CLASIFICACION_WEB().ToList();
                if (listarClasifiacionEPPWeb == null || listarClasifiacionEPPWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_CLASIFICACION_WEB_Result, DataClasificacionEppWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_CLASIFICACION_WEB_Result>, List<DataClasificacionEppWeb>>(listarClasifiacionEPPWeb);
                    response.listaDataClasificacionEppWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de calsificacion de epp";

            }
            return response;


        }

        public ListasMaestroWebResponse ListarCategoriaEppWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarCategoriaEPPWeb = ctx.SP_LISTAR_CATEGORIA_WEB().ToList();
                if (listarCategoriaEPPWeb == null || listarCategoriaEPPWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de EPP";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_LISTAR_CATEGORIA_WEB_Result, DataCategoriaEppWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_CATEGORIA_WEB_Result>, List<DataCategoriaEppWeb>>(listarCategoriaEPPWeb);
                    response.listaDataCategoriaEppWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarClasificaiconEpp. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de categoria de epp";

            }
            return response;
        }
        public ListasMaestroWebResponse ListarTipoColaboradorWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarTipoColaboradorWeb = ctx.SP_OBTENER_LISTA_TIPO_COLABORADORES_WEB().ToList();
                if (listarTipoColaboradorWeb == null || listarTipoColaboradorWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de tipo colaboradores";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTA_TIPO_COLABORADORES_WEB_Result, DataTipoColaboradorWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTA_TIPO_COLABORADORES_WEB_Result>, List<DataTipoColaboradorWeb>>(listarTipoColaboradorWeb);
                    response.listaDataTipoColaboradorWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de tipo colaboradores";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarTipoColaboradorWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de tipo de colaboradores";

            }
            return response;
        }
        public ListasMaestroWebResponse ListarInstalacionWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarInstalacionWeb = ctx.SP_OBTENER_LISTAS_INSTALACIONES_WEB().ToList();
                if (listarInstalacionWeb == null || listarInstalacionWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de instalaciones ";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTAS_INSTALACIONES_WEB_Result, DataInstalacionesWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTAS_INSTALACIONES_WEB_Result>, List<DataInstalacionesWeb>>(listarInstalacionWeb);
                    response.listaDataInstalacionesWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de instalaciones";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarInstalacionWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de instalaciones";

            }
            return response;
        }
        public ListasMaestroWebResponse ListarEmpresaWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarEmpresaWeb = ctx.SP_OBTENER_LISTAS_EMPRESA_WEB().ToList();
                if (listarEmpresaWeb == null || listarEmpresaWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de instalaciones ";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTAS_EMPRESA_WEB_Result, DataEmpresasWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTAS_EMPRESA_WEB_Result>, List<DataEmpresasWeb>>(listarEmpresaWeb);
                    response.listaDataEmpresaWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de empresa";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarInstalacionWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de empresa";

            }
            return response;
        }
        public ListasMaestroWebResponse ListarRolUsuarioWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarRolesdeUsuarioWeb = ctx.SP_OBTENER_LISTAS_ROL_USUARIO_WEB().ToList();
                if (listarRolesdeUsuarioWeb == null || listarRolesdeUsuarioWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de los roles de usuario ";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTAS_ROL_USUARIO_WEB_Result, DataRolUsuarioWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTAS_ROL_USUARIO_WEB_Result>, List<DataRolUsuarioWeb>>(listarRolesdeUsuarioWeb);
                    response.listaDataRolUsuarioWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de los roles de usuario";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarRolUsuarioWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de  los roles de usuario";

            }
            return response;
        }
        public ListasMaestroWebResponse ListarAreaWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarAreaWeb = ctx.SP_OBTENER_LISTAS_AREA_WEB().ToList();
                if (listarAreaWeb == null || listarAreaWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de los roles de usuario ";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTAS_AREA_WEB_Result, DataAreaWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTAS_AREA_WEB_Result>, List<DataAreaWeb>>(listarAreaWeb);
                    response.listaDataAreasWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de las areas";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarAreaWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener las areas";

            }
            return response;
        }

        public CreacionProcesoCargaUsuariosResponse CrearProcesoCargaUsuarios(string UUIDusuario, string urldocumento, string nombreArchivo, string nombreArchivoApi)
        {
            try
            {
                var ctx = new EPPEntities();
                var response = ctx.SP_PROCESOCARGA_CREACION_PROCESO_CARGA_USUARIOS(
                     nombreArchivo, nombreArchivoApi, Convert.ToInt32(UUIDusuario), urldocumento).Select(x => new CreacionProcesoCargaUsuariosResponse()
                     {
                         codigo = x.codigo.GetValueOrDefault(),
                         descripcion = x.descripcion,
                         idProcesoCargaUsuarios = x.idProcesoUsuariosModificaciones.GetValueOrDefault(),
                         codProcesoCargaUsuarios = x.codProcesoUsuariosModificaciones
                     }).FirstOrDefault();
                if (response == null || response.idProcesoCargaUsuarios <= 0)
                {
                    return new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = 0,
                        descripcion = "No se obtuvo respuesta del servicio de creacion proceso carga de usuarios."
                    };
                }
                return response;
            }
            catch (Exception)
            {
                return new CreacionProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno, no se pudo crear el proceso de carga de usuarios."
                };
            }
        }

        public CreacionProcesoCargaUsuariosResponse CambiarEstadoProcesoCargaUsuariosAEnProceso(string UUIDusuario, int idProcesoCarga, int totalGlobal)
        {
            try
            {
                log.Warn($"UsuarioDO ({idProcesoCarga})->  CambiarEstadoProcesoCargaUsuariosAEnProceso. Actualizar estado Proceso.");
                var ctx = new EPPEntities();
                var response = ctx.SP_PROCESOCARGA_USUARIOS_CAMBIAR_ESTADO_A_EN_PROCESO(UUIDusuario, idProcesoCarga,
                    totalGlobal).Select(x => new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = x.codigo.GetValueOrDefault(),
                        descripcion = x.descripcion
                    }).FirstOrDefault();

                if (response == null)
                {
                    return new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = 0,
                        descripcion = "No se obtuvo respuesta al cambiar el estado del proceso de carga."
                    };
                }
                return response;
            }
            catch (Exception ex)
            {
                log.Warn($"UsuarioDO ({idProcesoCarga})->  CambiarEstadoProcesoCargaUsuariosAEnProceso. Error al actualizar estado. " +
                    "Error: " + JsonConvert.SerializeObject(ex));
                return new CreacionProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno al cambiar el estado del proceso de carga."
                };
            }
        }

        public RegistroDatosProcesoCargaUsuariosResponse RegistrarDatosUsuarios(DataTable dtCargaUsuarios, int idProcesoCarga, string UUIDusuario)
        {
            try
            {
                log.Warn($"UsuarioDO ({idProcesoCarga})->  RegistrarDatosUsuarios. Registro datos temporales.");
                var ctx = new EPPEntities();
                var parameterIdProcesoCargaUsuarios = new SqlParameter { SqlDbType = SqlDbType.Int, ParameterName = "@idProcesoCargaUsuarios", Value = idProcesoCarga };

                var parameterUUIDusuario = new SqlParameter { SqlDbType = SqlDbType.VarChar, ParameterName = "@idUsuario", Value = UUIDusuario };

                var parameterTblDataUsuarios = new SqlParameter
                {
                    SqlDbType = SqlDbType.Structured,
                    ParameterName = "@tblDataUsuarios",
                    Value = dtCargaUsuarios,
                    TypeName = "type_datos_usuarios"
                };

                ctx.Database.CommandTimeout = 3600;
                var respuesta = ctx.Database.SqlQuery<RegistroDatosProcesoCargaUsuariosResponse>(
                    "SP_PROCESO_USUARIOS_REGISTRO_DATOS " +
                    "@idProcesoCargaUsuarios," +
                    "@idUsuario," +
                    "@tblDataUsuarios",
                    parameterIdProcesoCargaUsuarios,
                    parameterUUIDusuario,
                    parameterTblDataUsuarios)
                    .FirstOrDefault();
                if (respuesta == null)
                {
                    return new RegistroDatosProcesoCargaUsuariosResponse()
                    {
                        codigo = 0,
                        descripcion = "No se obtuvo respuesta al registrar los datos."
                    };
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioDO ({idProcesoCarga})->  RegistrarDatosUsuarios. Error Registro datos temporales. " +
                    "Error: " + JsonConvert.SerializeObject(ex));
                return new RegistroDatosProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno al registrar los datos."
                };
            }
        }
        public ListasMaestroWebResponse ListarCargoWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarCargoWeb = ctx.SP_OBTENER_LISTAS_CARGO_WEB().ToList();
                if (listarCargoWeb == null || listarCargoWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de tipo cargos";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTAS_CARGO_WEB_Result, DataCargoWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTAS_CARGO_WEB_Result>, List<DataCargoWeb>>(listarCargoWeb);
                    response.listaDataCargoWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de tipo de cargos";

                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarEstadoEntregaWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de estados del acta";

            }
            return response;


        }
        public ListasMaestroWebResponse ListarEstadoEntregaWEB()
        {

            var response = new ListasMaestroWebResponse();
            try
            {
                var ctx = new EPPEntities();
                var listarEstadoWebEPPWeb = ctx.SP_OBTENER_LISTA_ESTADO_ENTREGA().ToList();
                if (listarEstadoWebEPPWeb == null || listarEstadoWebEPPWeb.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista los tipo de estado del acta";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_OBTENER_LISTA_ESTADO_ENTREGA_Result, DataEstadoEntregaWeb>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_OBTENER_LISTA_ESTADO_ENTREGA_Result>, List<DataEstadoEntregaWeb>>(listarEstadoWebEPPWeb);
                    response.listaDataEstadoEntregaWeb = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de clasifiacacion de epp";

                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListarEstadoEntregaWEB. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de estados del acta";

            }
            return response;


        }


        public CreacionProcesoCargaUsuariosResponse ActualizarProcesoCargaUsuariosFinalizado(int idProcesoCargaUsuarios, string UUIDusuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var response = ctx.SP_PROCESOCARGA_USUARIOS_UPDATE_ESTADO_FINALIZADO(idProcesoCargaUsuarios,
                    Convert.ToInt32(UUIDusuario)).Select(x => new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = x.codigo.GetValueOrDefault(),
                        descripcion = x.descripcion
                    }).FirstOrDefault();

                if (response == null)
                {
                    return new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = 0,
                        descripcion = "No se obtuvo respuesta al actualizar el estado del proceso de carga de usuarios a finalizado."
                    };
                }
                return response;
            }
            catch (Exception)
            {
                return new CreacionProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno, no se pudo completar la actualización del estado del proceso de carga de usuarios a finalizado."
                };
            }
        }

        public CreacionProcesoCargaUsuariosResponse TraspasoDatosProcesoCargaUsuarios(string UUIDusuario, int idProcesoCarga)
        {
            try
            {
                log.Warn($"UsuarioDO ({idProcesoCarga})->  TraspasoDatosProcesoCargaUsuarios. Registro datos finales.");
                var ctx = new EPPEntities();
                ctx.Database.CommandTimeout = 3600;
                var response = ctx.SP_PROCESOCARGA_USUARIOS_TRASPASO_DATOS_TEMPORALES_A_PERMANENTES(UUIDusuario,
                    idProcesoCarga).Select(x => new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = x.codigo.GetValueOrDefault(),
                        descripcion = x.descripcion
                    }).FirstOrDefault();
                if (response == null)
                {
                    return new CreacionProcesoCargaUsuariosResponse()
                    {
                        codigo = 0,
                        descripcion = "No se puedo obtener respuesta en el traspaso de datos de la carga."
                    };
                }
                return response;
            }
            catch (Exception ex)
            {
                log.Error($"UsuarioDO ({idProcesoCarga})->  TraspasoDatosProcesoCargaUsuarios. Error registro datos finales. " +
                    "Error: " + JsonConvert.SerializeObject(ex));
                return new CreacionProcesoCargaUsuariosResponse()
                {
                    codigo = -1,
                    descripcion = "Error interno en el traspaso de de datos de la carga."
                };
            }
        }

        public ProcesoCargalistadoResponse listarProcesoCargo(int num_pagina, int cant_registro, string criterioBusqueda, int id_usuario)
        {

            try
            {
                var ctx = new EPPEntities();

                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var criterioBusquedaParameter = new SqlParameter("criterioBusqueda", (object)criterioBusqueda ?? DBNull.Value);
                var id_usuarioParameter = new SqlParameter("id_usuario", (object)id_usuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                var datosBusqueda = ctx.Database.SqlQuery<SP_PROCESO_CARGA_LISTADO_PROCESO_Result>("EXEC SP_PROCESO_CARGA_LISTADO_PROCESO " +
               "@num_pagina, @cant_registro, @criterioBusqueda, @id_usuario, @total_registros OUT",
               num_paginaParameter, cant_registroParameter, criterioBusquedaParameter, id_usuarioParameter, total_registrosParameter).ToList();

                int total_registros = Convert.ToInt32(total_registrosParameter.Value);
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_PROCESO_CARGA_LISTADO_PROCESO_Result, ListarProcesoCargaResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_PROCESO_CARGA_LISTADO_PROCESO_Result>, List<ListarProcesoCargaResponse_Data>>(datosBusqueda);

                    return new ProcesoCargalistadoResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ProcesoCargalistadoResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarProcesoCargaResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarProcesoCargo. Mensaje al cliente: Error interno al listar la carga de proceso " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ProcesoCargalistadoResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar la carga de proceso"
                };
            }




        }

        public DetalleCargaUsuarioResponse DetalleProcesoCarga(int id_proceso_Carga, int id_usuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var response = new DetalleCargaUsuarioResponse();
                var detalleCarga = ctx.SP_PROCESO_CARGA_DETALLE(id_proceso_Carga, id_usuario).Select(x => new DetalleCarga()
                {
                    id_usuarios_proceso = x.id_usuarios_proceso,
                    cod_usuarios_proceso_modificacion = x.cod_usuarios_proceso_modificacion,
                    cod_usuarios_proceso_carga_estado = x.cod_usuarios_proceso_carga_estado,
                    sigla = x.sigla,
                    descripcionProCarEstado = x.descripcionProcesoCargaEstado,
                    nombre_archivo = x.nombre_archivo,
                    fecha_inicio = x.fecha_inicio,
                    fecha_fin = x.fecha_fin,
                    total_global = x.total_global.GetValueOrDefault(),
                    total_completos = x.total_completos.GetValueOrDefault(),
                    cantidadErrores = x.cantidadErrores,
                    //porcentaje_avance = Convert.ToDouble(x.porcentaje_avance.GetValueOrDefault())
                    porcentaje_avance = (x.porcentaje_avance == null) ? 0 : Convert.ToDouble(x.porcentaje_avance)
                }).FirstOrDefault();


                if (detalleCarga == null || detalleCarga.id_usuarios_proceso <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = "No se obtuvo respuesta del servicio detalle de carga de usuarios.";
                    return response;
                }

                response.codigoRes = HttpStatusCode.OK;
                response.mensajeRes = "Se obtuvo el detalle de la carga de usuarios correctamente.";
                response.data = detalleCarga;
                return response;

            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarProcesoCargo. Mensaje al cliente: Error interno al listar los datos de detalle de la carga." +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new DetalleCargaUsuarioResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar los detalles de la carga"
                };
            }



        }

        public ListaTiposErroresCargaResponse ObtenerListaTipoError()
        {
            var response = new ListaTiposErroresCargaResponse();
            try
            {
                var ctx = new EPPEntities();
                var listaTipoErrores = ctx.SP_PROCESOCARGA_LISTA_TIPO_ERRORES_CARGA().ToList();
                if (listaTipoErrores == null || listaTipoErrores.Count() <= 0)
                {
                    response.codigoRes = HttpStatusCode.NoContent;
                    response.mensajeRes = " No se obtuvo la lista de tipos de errores carga ";
                }
                else
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<SP_PROCESOCARGA_LISTA_TIPO_ERRORES_CARGA_Result, DataErrores>(); });
                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_PROCESOCARGA_LISTA_TIPO_ERRORES_CARGA_Result>, List<DataErrores>>(listaTipoErrores);
                    response.ListaErroesCarga = datosMapeados;
                    response.codigoRes = HttpStatusCode.OK;
                    response.mensajeRes = "se obtuvo la lista de tipos de errores carga";



                }
            }

            catch (Exception e)
            {
                log.Error("UsuarioDO ->  ListaTiposErroresCargaResponsecs. Mensaje al cliente: Error interno . " +
                     "Detalle error: " + JsonConvert.SerializeObject(e));
                response.codigoRes = HttpStatusCode.InternalServerError;
                response.mensajeRes = "Error interno al obtener la lista de tipos de errores carga";

            }
            return response;

        }

        public ListarProcesoCargaErroresResponse listarProcesoCargoErrores(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int num_pagina, int cant_registro, int id_usuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var idProcesoCargaParameter = new SqlParameter("idProcesoCarga", (object)idProcesoCarga ?? DBNull.Value);
                var idsCargaErrorEstadosParameter = new SqlParameter("idsCargaErrorEstados", (object)idsCargaErrorEstados ?? DBNull.Value);
                var criterioBusquedaParameter = new SqlParameter("criterioBusqueda", (object)criterioBusqueda ?? DBNull.Value);
                var num_paginaParameter = new SqlParameter("num_pagina", (object)num_pagina ?? DBNull.Value);
                var cant_registroParameter = new SqlParameter("cant_registro", (object)cant_registro ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)id_usuario ?? DBNull.Value);
                var total_registrosParameter = new SqlParameter
                {
                    ParameterName = "@total_registros",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                var datosBusqueda = ctx.Database.SqlQuery<SP_LISTAR_PROCESO_CARGA_ERRORES_Result>("EXEC SP_LISTAR_PROCESO_CARGA_ERRORES " +
               "@idProcesoCarga , @idsCargaErrorEstados, @criterioBusqueda, @num_pagina,@cant_registro , @idUsuario, @total_registros OUT",
               idProcesoCargaParameter, idsCargaErrorEstadosParameter, criterioBusquedaParameter, num_paginaParameter, cant_registroParameter, idUsuarioParameter, total_registrosParameter).ToList();

                int total_registros = Convert.ToInt32(total_registrosParameter.Value);
                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_PROCESO_CARGA_ERRORES_Result, ListarProcesoCargaErroresResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_PROCESO_CARGA_ERRORES_Result>, List<ListarProcesoCargaErroresResponse_Data>>(datosBusqueda);

                    return new ListarProcesoCargaErroresResponse()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),
                        total_registros = total_registros
                    };
                }
                else
                {
                    return new ListarProcesoCargaErroresResponse()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarProcesoCargaErroresResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarProcesoCargo. Mensaje al cliente: Error interno al listar la carga de proceso " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarProcesoCargaErroresResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar la carga de proceso"
                };
            }

        }
        public ListarProcesoCargaErroresResponseExcel listarProcesoCargoErroresExcel(int idProcesoCarga, string idsCargaErrorEstados, string criterioBusqueda, int id_usuario)
        {

            try
            {
                var ctx = new EPPEntities();
                var idProcesoCargaParameter = new SqlParameter("idProcesoCarga", (object)idProcesoCarga ?? DBNull.Value);
                var idsCargaErrorEstadosParameter = new SqlParameter("idsCargaErrorEstados", (object)idsCargaErrorEstados ?? DBNull.Value);
                var criterioBusquedaParameter = new SqlParameter("criterioBusqueda", (object)criterioBusqueda ?? DBNull.Value);
                var idUsuarioParameter = new SqlParameter("idUsuario", (object)id_usuario ?? DBNull.Value);

                var datosBusqueda = ctx.Database.SqlQuery<SP_LISTAR_PROCESO_CARGA_ERRORES_EXCEL_Result>("EXEC SP_LISTAR_PROCESO_CARGA_ERRORES_EXCEL " +
                "@idProcesoCarga , @idsCargaErrorEstados, @criterioBusqueda , @idUsuario ",
                idProcesoCargaParameter, idsCargaErrorEstadosParameter, criterioBusquedaParameter, idUsuarioParameter).ToList();

                if (datosBusqueda != null && datosBusqueda.Count > 0)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<SP_LISTAR_PROCESO_CARGA_ERRORES_EXCEL_Result, ListarProcesoCargaErroresExcelResponse_Data>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var datosMapeados = mapper.Map<List<SP_LISTAR_PROCESO_CARGA_ERRORES_EXCEL_Result>, List<ListarProcesoCargaErroresExcelResponse_Data>>(datosBusqueda);

                    return new ListarProcesoCargaErroresResponseExcel()
                    {
                        codigoRes = HttpStatusCode.OK,
                        mensajeRes = "Datos obtenidos correctamente.",
                        data = datosMapeados.ToList(),

                    };
                }
                else
                {
                    return new ListarProcesoCargaErroresResponseExcel()
                    {
                        codigoRes = HttpStatusCode.NoContent,
                        mensajeRes = "No se obtuvieron datos.",
                        data = new List<ListarProcesoCargaErroresExcelResponse_Data>()
                    };
                }
            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  listarProcesoCargo. Mensaje al cliente: Error interno al listar la carga de proceso " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ListarProcesoCargaErroresResponseExcel()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno al listar la carga de proceso"
                };
            }

        }
        public ValidacionCantidadIntentosResponse  ValidacionCantidadIntentos(string usuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var usuarioParameter = new SqlParameter("usuario", (object)usuario ?? DBNull.Value);
                var respuesta = ctx.Database.SqlQuery<SP_VALIDACION_CANTIDAD_INTENTOS_Result>("EXEC SP_VALIDACION_CANTIDAD_INTENTOS " + " @usuario ",
              usuarioParameter).FirstOrDefault();

                return new ValidacionCantidadIntentosResponse()
                {
                    codigoRes = (HttpStatusCode)Convert.ToInt32(respuesta.codigo),
                    mensajeRes = respuesta.descripcion


                };
            }
            catch (Exception e)
            {

                log.Error("UsuarioDO ->  ValidacionCantidadIntentos. Mensaje al cliente: Error interno al listar la carga de proceso " +
                    "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ValidacionCantidadIntentosResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno a la validacion de cantidad de intentos"
                };
            }

        }

        public void UpdateUsuarioExitoso(string usuario)
        {
            var ctx = new EPPEntities();
            ctx.SP_REGISTRO_USUARIO_EXITOSO(usuario);

        }


        public ActivarCuentaResponse activarCuenta(string usuario)
        {
            try
            {
                var ctx = new EPPEntities();
                var response = ctx.SP_ACTIVAR_CUENTA(usuario).FirstOrDefault();
                if (response != null)
                {
                    if (response.codigo == 200)
                    {
                        return new ActivarCuentaResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = response.descripcion
                        };
                    }
                   
                    else
                    {
                        return new ActivarCuentaResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = response.descripcion
                        };
                    }
                }
                else
                {

                    return new ActivarCuentaResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }

            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  activarCuenta. Mensaje al cliente: Error interno  " +
                                   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ActivarCuentaResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno en el servicio activarCuenta"
                };
            }

          

        }
        public ValidarRutResponse validarrut(string rut)
        {

            var response = new ValidarRutResponse();
            try
            {
                var ctx = new EPPEntities();
                var rutParameter = new SqlParameter("rut", (object)rut ?? DBNull.Value);
                var datosBusqueda = ctx.Database.SqlQuery<SP_VALIDAR_RUT_Result>("SP_VALIDAR_RUT " +
                    "@rut", rutParameter).FirstOrDefault();
             
                if (datosBusqueda != null)
                {
                    if (datosBusqueda.codigo == 200)
                    {
                        return new ValidarRutResponse()
                        {
                            codigoRes = HttpStatusCode.OK,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }

                    else
                    {
                        return new ValidarRutResponse()
                        {
                            codigoRes = HttpStatusCode.BadRequest,
                            mensajeRes = datosBusqueda.descripcion
                        };
                    }
                }
                else
                {

                    return new ValidarRutResponse()
                    {
                        codigoRes = HttpStatusCode.NotFound,
                        mensajeRes = "Los campos se encuentran vacios"
                    };
                }

            }
            catch (Exception e)
            {
                log.Error("UsuarioDO ->  validarrut. Mensaje al cliente: Error interno  " +
                                   "Detalle error: " + JsonConvert.SerializeObject(e));
                return new ValidarRutResponse()
                {
                    codigoRes = HttpStatusCode.InternalServerError,
                    mensajeRes = "Error interno en el servicio activarCuenta"
                };
            }
        }

    }
}





